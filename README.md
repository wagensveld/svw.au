# svw.au

[![Netlify Status](https://api.netlify.com/api/v1/badges/362ffbbe-6cdb-4c09-a391-c8b3ff5fb2a9/deploy-status)](https://app.netlify.com/sites/svwau/deploys)

A repository for my personal website <a href="https://svw.au" target="_blank">svw.au</a>

## References

- <a href="https://github.com/lxndrblz/anatole" target="_blank">Anatole Hugo Theme</a>
- <a href="https://icons8.com/line-awesome" target="_blank">Line Awesome Icons</a>
- <a href="https://emojipedia.org/docomo/" target="_blank">NTT DoCoMo Emoji Font</a>
- <a href="https://blog.ypertex.com/articles/useful-hugo-templating/">Link Previews</a>
