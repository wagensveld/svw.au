---
title: "Honey Sriracha Chicken"
description: "Sweet spicy chicken to have with rice."
summary: "Sweet spicy chicken to have with rice."
date: "2022-02-03T20:15:56"
category: "recipes"
---

> Preheat oven to **_250°C_**.

1. In cast iron pan brown chicken on both sides. Crack pepper and add salt to chicken. Remove and add to plate.
   - **Chicken Thighs**, 1kg
2. Slice onion, and mince garlic. Add to pan, saute for about 1 minute.
   - **Onion**, 1
   - **Garlic**, 3 Cloves
3. Deglaze with sake, add chicken stock, soy sauce, sriracha, and honey. Stir well.
   - **Sake**, 1/2 Cup
   - **Chicken Stock**, 1 Cup
   - **Soy Sauce**, 1 Tbsp
   - **Sriracha**, 1/4 Cup
   - **Honey**, 1/2 Cup
4. Cut Broccoli florets into bite sized pieces. Add to pan.
   - **Broccoli**, 1 Head
5. Add Chicken to pan. Move Pan into oven for 18 minutes.
