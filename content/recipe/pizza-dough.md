---
title: "Pizza Dough"
description: "Basic pizza dough recipe."
summary: "Basic pizza dough recipe."
date: "2021-07-21T19:34:10"
category: "recipes"
---

> Creates 1 large pizza

1. Mix yeast and water, let rest for 5 minutes.
   - **Yeast**, 1 tsp
   - **Water**, 125ml -- Room temperature
2. Combine all ingredients in a mixer, until smooth.
   - **Water Yeast mixture**
   - **Flour**, 200g
   - **Salt**, 1 tsp
   - **Olive Oil**, 1 Tbsp
3. Place dough in lightly oiled bowl and cover for 30 minutes to an hour. Use this time to preheat your oven to **_250°C_**.
4. Move dough to a lightly floured surface, knead gently.
5. Roll the dough out to your desired size and thickness.
6. Add desired toppings.
7. Bake for 10 minutes, or until the crust and cheese are golden brown.
