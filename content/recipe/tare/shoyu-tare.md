---
title: "Shoyu Tare"
description: "General all-purpose shoyu tare."
summary: "General all-purpose shoyu tare."
date: "2022-09-25T22:39:55"
category: "recipes"
series:
  - "ramen"
---

> Add 30ml of tare per 350ml of soup

1. Combine soy sauce, mirin, konbu, sake, and niboshi in a container. Place in the fridge for 6 hours up to 2 days.
   - **Soy Sauce**, 400ml
   - **Mirin**, 45ml
   - **Sake**, 50ml
   - **Konbu**, 15g
   - **Niboshi**, 15g
2. Bring contents to a simmer for about 10 minutes.
3. Remove konbu.
4. Add the bonito flakes continue simmering for another 5 minutes.
   - **Bonito Flakes**, 15g
5. Add sugar, whisking to dissolve.
   - **Sugar**, 30g
6. Strain and reserve tare.
