---
title: "Shio Tare"
description: "Fairly standard shio tare."
summary: "Fairly standard shio tare."
date: "2022-10-04T21:24:14"
category: "recipes"
series:
  - "ramen"
---

1. Add water, konbu and mirin to a container, keep in fridge for 6 hours to 2 days.
   - **Water**, 400ml
   - **Mirin**, 45ml
   - **Konbu**, 15g
2. Bring mixture to a simmer for 10 minutes.
3. Remove and discard konbu.
4. Add sake, bring to boil for 10 minutes, or until the smell of alcohol subsides.
   - **Sake**, 100ml
5. Add remaining ingredients, whisking to dissolve.
   - **Salt**, 50g
   - **MSG**, 10g
6. Store in a container.
