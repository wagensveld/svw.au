---
title: "Minimal Shio Tare"
description: "Super basic shio tare. TBH I don't use it for ramen, but more if I want to give a taste test for stock."
summary: "Super basic shio tare. TBH I don't use it for ramen, but more if I want to give a taste test for stock."
date: "2022-10-04T18:46:11"
category: "recipes"
series:
  - "ramen"
---

1. Combine all ingredients.
   - **Water**, 500ml
   - **Salt**, 50g
   - **MSG**, 33g
