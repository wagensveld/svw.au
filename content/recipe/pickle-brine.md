---
title: "Pickle Brine"
description: "The basic pickle brine is 1:1:1, 1 cup water to 1 cup vinegar to 1 tablespoon salt."
summary: "The basic pickle brine is 1:1:1, 1 cup water to 1 cup vinegar to 1 tablespoon salt."
date: "2021-10-10T10:10:10"
category: "recipes"
---

> Usually creates enough for a 900ml jar.

Before starting remember to [sterilised your jars](/recipe/not-food/sterilise-jars).

1. Combine all ingredients in a pot, bring to a boil.
   - **Water**, 1 Cup
   - **Vinegar**, 1 Cup
   - **Salt**, 1 Tbsp
2. Add brine to vegetables in a jar until filled. Let cool and refrigerate.
