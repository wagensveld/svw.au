---
title: "Ferment Almost Anything"
description: "Vegetable weight (g) + Water weight (g) * 0.02 = salt (g)."
summary: "Vegetable weight (g) + Water weight (g) * 0.02 = salt (g)."
date: "2021-10-10"
category: "recipes"
---

> Usually creates enough for a 900ml jar.

> Remember to [sterilise the jar](/recipe/not-food/sterilise-jars).

1. Add a jar to a scale and zero out, add vegetables to the jar and fill up with water. Record the weight in grams.
  - **Vegetable**
  - **Water**
2. Multiply the weight by 0.02 (or 0.03 if the vegetable is prone to developing mould), this is the amount of salt you need (in grams).
3. Remove the water into a bowl, combine with salt, and add back to the jar.
  - **Salt**
4. Leave at room temperature for 3 days to 2 weeks depending on taste.
