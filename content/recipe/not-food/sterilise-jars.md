---
title: "Sterilise Jars"
description: "Sterilising jars is an important and often overlooked aspect of food preparation. Here's how to do it."
summary: "Sterilising jars is an important and often overlooked aspect of food preparation. Here's how to do it."
date: "2022-09-23T16:12:18"
category: "recipes"
---

> Preheat oven to **_160°C_**.

1. Wash jar and jar lid using water and dish soap, rinse.
   - **Jar**
   - **Jar lid**
2. Add jar to oven set to **_160°C_** for about 10 minutes.
3. Boil jar lid in pot for about 10 minutes as well.
4. Remove jar from the oven, and lids from the pot, let cool.
5. Use jars.
