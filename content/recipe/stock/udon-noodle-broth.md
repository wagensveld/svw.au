---
title: "Udon Noodle Broth"
description: "Straightforward Udon soup."
summary: "Straightforward Udon soup."
date: "2022-09-30T10:03:34"
category: "recipes"
images:
  - "images/udon-broth.webp"
series:
  - "udon"
---

> Make or buy [dashi](/series/dashi/) in advance.

1. Combine all ingredients in a pot, bring to a simmer.
   - **[Dashi](/series/dashi/)**, 1L -- If making your own dashi, use Ichiban Dashi
   - **Salt**, 5g
   - **Light Soy Sauce**, 20ml
   - **Dark Soy Sauce**, 20ml
   - **Mirin**, 15ml
   - **Sugar**, 12g

## Serving

Serve with [udon noodles](/recipe/noodles/udon-noodles), some finely sliced spring onion, kamaboko, and some shichimi spice mix.
