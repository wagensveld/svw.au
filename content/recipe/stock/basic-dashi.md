---
title: "Basic Dashi"
description: "A simple dashi which isn't too overpowering. Can be used in most applications."
summary: "A simple dashi which isn't too overpowering. Can be used in most applications."
date: "2022-09-25T22:11:44"
category: "recipes"
series:
  - "ramen"
  - "dashi"
  - "udon"
---

> Makes 1L of Ichiban Dashi and 1L of Niban Dashi

Quick context about ichiban and niban dashi. Ichiban (Primary) dashi is made first it's a lot more fragrant and has subtle flavours. It's used in things such as sauces and clear soups. Niban (Secondary) dashi uses the leftovers from the ichiban dashi, it isn't as fragrant or delicate as ichiban dashi, but works well for soup stocks.

## Ichiban Dashi

1. Add water and konbu to a pot. Heat uncovered. Remove konbu before the water starts to boil. (Konbu releases a really foul odour if it's boiled.)
   - **Water**, 1L
   - **Konbu**, 30g
2. Insert your thumbnail into the thickest part of the konbu, if it's soft put aside the konbu, if it's still hard, reduce the heat by adding some more cold water (60ml or so) and continue for another 1 to 2 minutes.
3. After the konbu has been removed add about 60ml of cold water to the pot to reduce the temperature. Add the bonito flakes. Bring back to a boil. Then immediately remove from heat. (Bonito flakes can be overpowering if left to boil for too long.)
   - **Bonito Flakes**, 30g
4. As the dashi is cooling skim the scum.
5. Strain and reserve dashi.
6. Reserve the bonito flakes and konbu for the niban dashi.

## Niban Dashi

1. Add reserved konbu and bonito flakes, and water to a pot. Heat uncovered at just a simmer. Until stock is reduced by about 1/3 or 1/2. (About 20 minutes). Taste to confirm desired ratio.
   - **Reserved konbu and bonito flakes**
   - **Water**, 1.5L
2. Add new bonito flakes to pot, immediately remove heat, let it settle (30 seconds to 1 minute). Remove scum.
   - **Bonito Flakes**, 10g
3. Strain and reserve dashi.
