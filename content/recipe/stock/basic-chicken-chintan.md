---
title: "Basic Chicken Chintan Stock"
description: "While originally for my ramen post, this basic chicken stock can be used for all sorts of dishes."
summary: "While originally for my ramen post, this basic chicken stock can be used for all sorts of dishes."
date: "2022-09-25T22:11:03"
category: "recipes"
series:
  - "ramen"
---

> Makes about 6 bowls of ramen.

1. Add chicken necks and thigh bones to a pot and add enough water to cover. Bring to a boil. After about 5 to 10 minutes scum will rise to the surface, skim it.
   - **Chicken Necks**, 2kg
   - **Chicken Thigh Bones**, 500g
   - **Water**, 4L -- 3L if you want to use dashi later
2. After scum has been removed lower heat to about 80C - just a simmer (this is to prevent emulsification - to keep the soup clear). Let it cook for 5 hours. If you're using dashi, this is a good time to add it. Replace water if too much evaporates or if the broth tastes too strong.
  - **Dashi**, 1L -- Optional
3. Add your onion, garlic, ginger. Cook for another hour, again replacing water to taste.
   - **Onion**, 1 -- Quartered
   - **Garlic**, 10 Cloves -- Crushed
   - **Ginger**, 2cm -- Peeled
