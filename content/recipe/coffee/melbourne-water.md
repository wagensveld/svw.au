---
title: "Melbourne Water"
description: "1L Distilled Water : 0.122g Epsom Salt : 0.034 Baking Soda"
summary: "1L Distilled Water : 0.122g Epsom Salt : 0.034 Baking Soda"
date: "2023-02-28T13:52:53"
category: "recipes"
---

Recently I've been reading the [Espresso Aficionados guides](https://espressoaf.com/) and saw [Melbourne Water](https://espressoaf.com/guides/water.html#explanations-of-waters-in-the-water-recipe-table) listed.

It's a lot cheaper for me to get Melbourne water from the tap than to make it. So I'm posting this in case I ever get homesick.

1. Mix all ingredients together.
   - **Distilled water**, 1L
   - **Epsom salt**, 0.122g
   - **Baking soda**, 0.034g
