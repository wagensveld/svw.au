---
title: "Japanese Canned Coffee"
description: "This isn't a post on canning coffee (though I may do that some time). It's about the canned coffee you can get at asian groceries and the like. Brands such as BOSS and UCC."
summary: "This isn't a post on canning coffee (though I may do that some time). It's about the canned coffee you can get at asian groceries and the like. Brands such as BOSS and UCC."
date: "2022-10-13T22:50:04"
draft: true
category: "recipes"
---

## Black Coffee

Canned black coffee is usually a [straight forward americano](/espresso#americano) which I find a little bit weaker than a pour over.

### Ingredients

| Ingredient | Amount | Comment        |
| :--------- | :----- | :------------- |
| Espresso   | 32g    | 16g in 32g out |
| Warm Water | 240ml  |                |

### Preparation

1. [Make an americano as you normally would.](/espresso#americano)

## Milk Coffee

This is usually what I'm after when buying canned coffee, I've found [pour over](/not-espresso/#pour-over-v60) provides the best results, but a slightly stronger americano, or french press would work well.

As an alternative for milk I have milk powder listed. The first reason is I have a lot left over which I need to get rid of. But the the other reason is it makes doing something like an [ice pour over](/not-espresso/#ice-pour-over) a lot easier by adding the extra 50g of water as ice cubes.

#### Ingredients

| Ingredient    | Amount | Comments                       |
| :------------ | :----- | :----------------------------- |
| Ground Coffee | 16g    |                                |
| Boiled Water  | 240ml  |                                |
| Roasted Sugar | 15g    | Or regular sugar               |
| Milk          | 50ml   | Or 5g Milk Powder + 50ml Water |

#### Preparation

1. [Make a pour over](/not-espresso/#pour-over-v60)
2. Add sugar and milk.
   - 15g Sugar
   - 50ml Milk
