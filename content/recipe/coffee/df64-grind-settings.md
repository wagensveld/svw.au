---
title: "DF64 Grind Settings"
description: "So many variables go into dialling in for coffee: bean type, roast level, age, dosage burrs, personal preference. Because of that I can't give any definitive values for a great cup, but what I can do is give a decent starting point."
summary: "So many variables go into dialling in for coffee: bean type, roast level, age, dosage burrs, personal preference. Because of that I can't give any definitive values for a great cup, but what I can do is give a decent starting point."
date: "2022-10-12T17:35:28"
aliases:
  - "/df64"
toc: true
category: "food"
---

## Aligning Springs and Align Zero

Because each DF64 is slightly different, setting it up gets us close enough to make the numbers relevant later on.

The first thing to do is set up your DF64, to do that follow [Caffe Martella Singapore's video on youtube](https://www.youtube.com/watch?v=GJuzz9NXKJY).

Essentially what you want to do is the following:

> Make sure the grinder is not plugged in

Align Springs:

1. Unscrew the grind adjustment wheel.
2. Remove the dosing funnel.
3. Remove the springs.
4. Make sure the silicone jacket around the spring is flush with the springs bottom.
5. Add the springs back. making sure the silicone jacket remains flush with the bottom of the spring.
6. Add the dosing funnel, add the grind adjustment wheel.

Align Zero:

1. Tighten the grind adjustment wheel until you cannot spin the burrs by hand.
2. Loosen until you can spin the burrs and you hear a faint scratching noise. This is zero.
3. Align your grind marker with zero on your grind adjustment wheel.

## Grind Settings

Personally I drink a lot of medium light roasts. If you prefer darker roasts most of these numbers will be slightly off.

### Turkish

> #2

Starting off strong I don't often make turkish coffee at home, but around #5 seems to be fine enough. Given it's so fine I'm not sure about maintenance long term. Whether this requires frequent cleaning to prevent clogging.

### Espresso

Most often I do either a ristretto or a normale. Lungo is largely there for completions sake.

#### Ristretto

> #7

While I really enjoy a magic, normale is still the style of espresso I make most often. Because of that my ristrettos are not particularity consistent, I usually take the normale grind value and decrease it by a few clicks.

#### Normale

> #12 -> #10

This is the one I make most often, because of that there's a range of grind settings. I find that at the rate I drink coffee and how they age, at the start of a bag 15 would be perfect, but towards the end a 12 is required to get the same extraction time.

#### Lungo

> #5

This is the opposite case of the ristretto, I take the normale value and increase it by 1 or 2.

### Moka / Nanopresso

> #30

Since getting my espresso machine I haven't made either type of coffee frequently, in the tests I've done 30 is a good starting point.

### Syphon / Pour Over (V60)

> #55

I like to use the same grind setting for syphon and pour over, however it's not uncommon to have pour over slightly finer than syphon.

### French Press

> #70

#70 is right about where I get a delicious and not a lot of silt. I do find I have to let my coffee brew for longer than other recipes (a lot say about 4 minutes, but I find myself waiting 10 minutes).

### Cold Brew

> #90

I think this comes down to your style of cold brew, I tend to do a 1:8 ratio. For wider ratios doing something like a french press grind works well as well.
