---
title: "Not Espresso Drinks"
description: "The sequel to the espresso drinks post, this time featuring coffee drinks which are not espresso based. this follows the same kind of format and uses similar language, so if partway through this post doesn't make sense it give the first couple of sentences a read to get up to speed."
summary: "The sequel to the espresso drinks post, this time featuring coffee drinks which are not espresso based. this follows the same kind of format and uses similar language, so if partway through this post doesn't make sense it give the first couple of sentences a read to get up to speed."
date: "2022-10-13T22:50:04"
aliases:
  - "not-espresso"
toc: true
category: "food"
---

I'll order the drinks in coarsest grind to finest - which happens to roughly correspond with how often I drink that style of coffee.

## Cold Brew

> 1:8 Ratio - 100g Coffee 800g Water for about 24 hours.

I know I said the order roughly corresponds with how frequent I drink the coffee, well this is why it's rough. I don't usually make cold brew this way, I usually do the [ice pour over](#ice-pour-over) method below.

That being said, the method is as follows:

- Use a 1:8 ratio - 100g coffee 800g water and leave in a large container: a large french press works perfectly because it has a filter.
- Leave it in the fridge for 24 hours.

As for coarseness you can use a french press grind, or something a little bit coarser. I would say if you can pick up an individual ground it's too coarse, but something under that is good.

Either drink it as is or with a dash of milk.

## French Press

> 1:14 Ratio - 15g Coffee 210g Water for about 10 minutes.

French press is near and dear to my heart, it's the style my parents would have for breakfast every Saturday. When I moved out they gave me one of their french presses, which I used every day for 2 years until I left the window open and a gust of wind knocked it over - shattering it.

Making it:

- Using a 1:14 ratio add 210g water to 15g ground coffee in a french press for about 10 minutes.
- Halfway through give it a gentle mix. The beans should be somewhat coarse but not too much. Think a little bit coarser than sand.

Usually you drink french press coffee on its own, however growing up we added a splash of milk.

## Syphon

> 1:15 Ratio - 16g Coffee 240g Water for 2 minutes.

Syphon coffee is wonderful, such a fun spectacle. Of the filter coffees it's my favourite, but it can be a hassle to set up.

Instructions:

- In a kettle boil some water.
- While boiling assemble the syphon.
- Pour the water into the bottom chamber, add the top chamber on top (ensuring the filter has been securely fastened).
- Light your bunsen burner.
- When the water has boiled to the top add your coffee to the top chamber, give it a mix, and then wait two minutes.
- Kill the heat, the coffee should be sucked down. Enjoy.

## Pour Over (V60)

> 1:15 Ratio - 16g Coffee 240g Water.

There are plenty of pour over brewers, V60 is the one I happen to have. One of the unique things about it is it has a relatively large opening at the bottom; this means liquid passes through quickly, so I tend to grind finer to compensate.

Instructions:

- Assemble filter, V60 and cup.
- Give the filter paper a rinse with hot water.
- Discard the water which has gone through the filter.
- Place V60/cup on a scale.
- Add coffee to the centre of the V60.
- Create a little well in the centre. You want roughly even distance between coffee and filter paper throughout the V60.
- Boil water.
- Add water at a 1:2 ratio - or 2 grams of water per 1 gram of bean. While pouring try to saturate as much of the grounds as possible.
- Quickly give it a swirl. Let rest for about 30 seconds.
- Over the next 1 and a half minutes pour in hot water in a circular motion, you want to pour at a rate which disturbs the grounds slightly. By now you should have used around 70% of your water, and have more less filled the V60.
- Continue pouring the rest of the water over 30 seconds, be gentle this time.
- Let coffee drip, giving it a slight swirl to get things going.

### Ice Pour Over

> 1:10:5 Ratio - 16g Coffee 160g Water 80g Ice.

This is how I usually make my ice coffee, the method is the same as a regular pour over, except you replace some of the water with ice. I'm pretty rough with the ratio, I usually weigh out my ice then subtract that from my usual amount of water used for a pour over.

The instructions are pretty much the same as the pour over, except you put the ice cubes in the cup below the V60.

## Moka

> If you can a 1:10 ratio but don't worry too much.

I'm not sure how I feel about moka pots, for one they are capable of producing a really tasty cup of coffee, and they look cool as the coffee is being extracted. On the other they're a bit finicky, in general you want to fill both the chamber and the basket. Your only real room for variance is grind size.

Instructions:

- Put boiling water in the base chamber to just under the valve.
- Fill filter basket with ground coffee, don't tamp - but if you have a WDT tool or a tooth pick mix it around to break up clumps.
- Assemble moka pot.
- Heat moka pot on a stove, just as it begins to sputter kill the heat and run the bottom chamber under cold water.

## Turkish

> 1:8 Ratio - 8g Coffee 64g Water

This is another coffee I don't often make, but that's just me being scared of damaging my grinder. It's easy and tastes quite nice.

- Heat up water in a cezve.
- Just as bubbles are about to form add coffee.
- Mix well!
- Huge bubbles will form, it'll look like it's boiling over. Kill the heat.
