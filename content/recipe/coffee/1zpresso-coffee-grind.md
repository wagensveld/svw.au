---
title: "Coffee Grind Adjustment on a 1Zpresso JX/JX Pro"
description: "Turn the dial or hand clockwise for finer grounds."
summary: "Turn the dial or hand clockwise for finer grounds."
date: "2021-10-22T21:00:43"
category: "food"
---

I keep on forgetting which direction to adjust the 1zpresso, so here's a reference page.

Assuming you are looking directly at the grind adjuster:

## JX

### Finer

Turn the hand (where the reference marker is) clockwise (the numbers will get larger).

### Coarser

Turn the hand (where the reference marker is) anti-clockwise (the numbers will get smaller).

## JX Pro

### Finer

Turn the dial (where the numbers are) clockwise (the numbers will get smaller).

### Coarser

Turn the dial (where the numbers are) anti-clockwise (the numbers will get larger).
