---
title: "Pad Krapow"
description: "Basic Thai basil chicken recipe."
summary: "Basic Thai basil chicken recipe."
category: "recipes"
images:
  - "images/pad-krapow.webp"
date: "2022-09-13T16:13:53"
---

> Creates 3 servings

## Sauce

1. Combine all ingredients in a bowl.
   - **Chicken Stock**, 80ml
   - **Oyster Sauce**, 1 Tbsp
   - **Fish Sauce**, 2 tsp
   - **Sugar**, 1 tsp

## Vegetables

1. Dice and combine all ingredients in a bowl.
   - **Shallot**, 1 --  Large
   - **Garlic**, 4 Cloves -- Minced
   - **Thai Chillies**, 4 -- chopped

## Basil

> The amount of basil you use depends on how much you like basil.

1. Pluck basil leaves and put in a bowl.
   - **Basil**, a lot

## Putting it all together

1. Cut chicken thighs into bite sized pieces, cook on oiled pan until it looks brown.
   - **Chicken Thighs**, 500g -- Cut into bite sized pieces
2. Add prepared vegetables to the pan.
   - **Prepared Vegetables**
3. When the vegetables start to caramelise, add prepared sauce.
   - **Prepared Sauce**
4. When sauce starts to caramelise, kill the heat and add prepared basil. Once the leaves are wilted you're ready to serve.
   - **Prepared Basil**

## Serving

Serve with jasmine rice, and a fried egg on top.
