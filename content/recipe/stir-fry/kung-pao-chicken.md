---
title: "Kung Pao Chicken"
description: "Quick and easy stir fried chicken."
summary: "Quick and easy stir fried chicken."
date: "2022-11-07T17:46:53"
category: "recipes"
---

## Chicken

1. Combine all ingredients in a bowl.
   - **Chicken**, 700g -- Cut into bit sized pieces
   - **Soy Sauce**, 5ml
   - **Shaoxing Wine**, 5ml
   - **Sesame Oil**, 5ml
   - **Corn Starch**, 1/2 tsp
   - **Salt**, 1/2 tsp
   - **White Pepper**, 1/2 tsp
   - **Sugar**, 1/4 tsp

## Sauce

1. Combine all ingredients in a bowl.
   - **Chicken Stock**, 30ml
   - **Soy Sauce**, 15ml
   - **Shaoxing Wine**, 15ml
   - **White Vinegar**, 15ml
   - **Sesame Oil**, 5ml
   - **Sugar**, 15g
   - **Corn Starch**, 5g

## Vegetables

1. Cut all ingredients into bite sized pieces and combine in a bowl.
   - **Red Capsicum**, 1
   - **Green Capsicum**, 1
   - **Stalks Celery**, 2 Stalks

## Spring Onion

1. Chop spring onion into small slices.
   - **Spring Onion**, 1 Stalk

## Putting it all together

1. Add prepared chicken to hot oiled wok until slightly undercooked. Remove.
   - **Prepared chicken**
2. Add prepared vegetables to the wok. Replace oil if needed. Until you see slight caramelisation.
   - **Prepared Vegetables**
3. Add peanuts. Wait 30 seconds.
   - **Peanuts**, 100g
4. Move vegetables aside. Add garlic and ginger. Fry for a few seconds.
   - **Garlic**, 2 Cloves -- Crushed
   - **Ginger**, 2 tsp -- Minced
5. Add prepared sauce.
   - **Prepared Sauce**
6. When sauce starts to thicken, kill the heat and add prepared spring onion and dried chillies.
   - **Prepared Spring Onion**
   - **Dried Chillies**, 8
