---
title: "Mongolian Beef"
description: "Serve it with noodles or rice."
summary: "Serve it with noodles or rice."
date: "2022-10-07T13:46:16"
category: "recipes"
---

## Beef

1. Cut steak into bite sized pieces, add to bowl with enough corn starch to cover sides.
   - **Steak**, 500g
   - **Corn Starch**, ~1/4 Cup

## Sauce

1. Combine ingredients in a bowl.
   - **Water**, 80ml
   - **Soy Sauce**, 80ml
   - **Sugar**, 100g
   - **Garlic**, 8 Cloves -- Crushed
   -  **Ginger**, 2 tsp -- Minced

## Vegetables

> Add whichever vegetables you like, personally I add chillies and broccoli often.

1. Dice and combine all ingredients in a bowl.
   - **Capsicum**, 1
   - **Spring Onion**, 4

## Putting it all together

1. Brown beef in a pan.
   - **Prepared Beef**
2. Add prepared sauce to the pan.
   - **Prepared Sauce**
3. As sauce is beginning to thicken, add prepared vegetables.
   - **Prepared Vegetables**
4. Simmer until thickened.
