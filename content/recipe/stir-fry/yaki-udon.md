---
title: "Yaki Udon"
description: "Basic yaki udon recipe."
summary: "Basic yaki udon recipe."
date: "2021-10-13T15:32:15"
category: "recipes"
series:
  - "udon"
---

> Creates 3 servings

## Preparation

> It's a good idea to cook the Udon in the background during the stir fry steps.

## Sauce

1. Combine all ingredients in a bowl.
   - **Sugar**, 1 Tbsp
   - **Oyster Sauce**, 1 Tbsp
   - **Soy Sauce**, 1 Tbsp
   - **Dashi**, 1 Tbsp
   - **Mirin**, 1/2 Tbsp
   - **Rice Wine Vinegar**, 1/2 Tbsp

## Stir Fry

1. Cook Udon according to packaging instructions.
   - **[Udon](/recipe/noodles/udon-noodles)**, 250g
2. Chop mushrooms into bite sized pieces, slice onion into slices. Keep in a bowl.
   - **Mushroom**, 1 -- Large
   - **Onion**, 1/2
3. Thinly slice cabbage, cut spring onion into inch long pieces. Keep in a separate bowl.
   - **Cabbage**, 1/4 Head
   - **Spring Onion**, 3
4. Heat a wok up high, add onions and mushrooms, stir until brown.
   - **Bowl of Onions and Mushrooms**
5. Add cabbage and spring onion, stir for a few minutes.
   - **Bowl of Cabbage and Spring Onion**
6. Add udon and sauce, stir until brown.
   - **Cooked Udon**
   - **Bowl of sauce**
7. Add minced garlic, white pepper, msg, and chilli oil to taste.
   - **Minced Garlic**, To Taste
   - **White Pepper**, To Taste
   - **MSG**, To Taste
   - **Chilli Oil**, To Taste
