---
title: "ハニーシラチャチキン"
description: "ご飯のお供に甘辛チキン。"
date: "2022-02-03"
category: "recipes"
---

ご飯のお供に甘辛チキン。

<!--more-->

## 材料

| 材料           | 量         |
| :------------- | :--------- |
| 鶏もも肉       | ~1kg       |
| 塩             | お好みで   |
| 胡椒           | お好みで   |
| オリーブオイル | 大さじ 2   |
| ネギ           | 1 個       |
| ニンニク       | 3 かけ     |
| 料理酒         | 1/2 カップ |
| 鶏がらスープ   | 1 カップ   |
| 醤油           | 大さじ 1   |
| シラチャソース | 1/4 カップ |
| 蜂蜜           | 1/2 カップ |
| ブロッコリー   | 1 束       |

## 作り方

> オーブンを 250℃ に予熱する。

1. 鋳鉄鍋でチキンを両面焼きます。 鶏肉に胡椒をふり、塩をふる。 取り出してプレートに追加します。
   - 鶏もも肉 1kg
2. 玉ねぎはみじん切り、にんにくはみじん切りにする。 鍋に加え、約 1 分間ソテーします。
   - ネギ 1 個
   - ニンニク 3 かけ
3. 酒で味を整え、チキンストック、醤油、シラチャー、蜂蜜を加えます。 よくかき混ぜ。
   - 料理酒 1/2 カップ
   - 鶏がらスープ 1 カップ
   - 醤油大さじ 1
   - シラチャソース 1/4 カップ
   - 蜂蜜 1/2 カップ
4. ブロッコリーの小房を一口大に切る。 鍋に加える。
   - ブロッコリー 1 束
5. 鶏肉をフライパンに加える。 パンをオーブンに 18 分間移します。
