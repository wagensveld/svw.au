---
title: "フレンチオニオンスープ"
description: "冬に向けてシンプルで暖かいもの。"
date: "2021-11-16"
category: "recipes"
---

冬に向けてシンプルで暖かいもの。

<!--more-->

## オニオンスープ

### 材料

| 材料         | 量         | コメント                                                           |
| :----------- | :--------- | :----------------------------------------------------------------- |
| ねぎ         | 650g       | 玉ねぎ 3 個くらいですが、赤 1 個、黄 2 個、エシャロット 3 個です。 |
| バター       | 50g        |                                                                    |
| 赤ワイン     | 1/4 カップ |                                                                    |
| 鶏がらスープ | 900ml      |                                                                    |
| タイム       | 1 小枝     |                                                                    |
| ローリエ     | 1 枚       |                                                                    |
| 魚醤         | 小さじ 1/2 |                                                                    |
| りんご酢     | 小さじ 1/2 |                                                                    |
| ニンニク     | 1 かけ     |                                                                    |
| 塩           | お好みで   |                                                                    |
| 胡椒         | お好みで   |                                                                    |

### 作り方

1. 玉ねぎをさいの目に切る。
   - ねぎ 650g
2. ダッチオーブンでバターを溶かし、玉ねぎを加えて非常に柔らかくなるまで（チョコレート色である必要はありません）。 よくかき混ぜます。
3. 赤ワインを加え、ほとんど蒸発するまで煮る。
   - 赤ワイン 1/4 カップ
4. 鶏がらスープ、タイム、ローリエ、魚醤、りんご酢を加える。 にんにくをみじん切りにして加える。 混ぜる。
   - 鶏がらスープ 900ml
   - タイム 1 小枝
   - ローリエ 1 枚
   - 魚醤小さじ 1/2
   - りんご酢小さじ 1/2
   - ニンニク 1 かけ
5. 塩胡椒を加えて味を調えます。
   - 塩
   - 胡椒

## パン

### 材料

| 材料             | 量                           |
| :--------------- | :--------------------------- |
| 田舎風のパン     | スープ 1 杯につき 1 切れ     |
| にんにく         | パン 1 切れにつき 1 クローブ |
| イタリアンハーブ | お好みで                     |
| 厳選チーズ       | パンを覆うのに十分な         |

### 作り方

> オーブンを 250C に予熱する

1. にんにくをみじん切りにし、パンに広げ、イタリアン ハーブをひとつまみ加え、チーズをふりかけ、パンがトーストされ、チーズが溶けるまでオーブンで焼きます。
   - 田舎風のパン
   - にんにく
   - イタリアンハーブ
   - Cheese
2. オニオンスープと一緒に器に盛る。
