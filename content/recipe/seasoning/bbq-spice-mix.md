---
title: "BBQ Spice Mix"
description: "Spice rub mix for any BBQ."
summary: "Spice rub mix for any BBQ."
date: "2021-12-05T22:00:25"
category: "recipes"
images:
  - "images/spice-mix.webp"
---

1. Combine all ingredients.
   - **Brown Sugar**, 1/2 Cup
   - **Paprika**, 1/2 Cup
   - **Black Pepper**, 1 Tablespoon
   - **Salt**, 1 Tablespoon
   - **Chilli Powder**, 1 Tablespoon
   - **Garlic Powder**, 1 Tablespoon
   - **Onion Powder**, 1 Tablespoon
   - **Cayenne Pepper**, 1 Teaspoon
