---
title: "Teriyaki Sauce"
description: "The ratio is 1:1:1:0.15 Sake(ml):Mirin(ml):Soy Sauce(ml):Sugar(g)."
summary: "The ratio is 1:1:1:0.15 Sake(ml):Mirin(ml):Soy Sauce(ml):Sugar(g)."
date: "2022-09-25T18:26:33"
series: ["sauce"]
category: "recipes"
---

> Creates about 300ml of teriyaki sauce

1. Add all ingredients to a pot with medium heat. Stir until sugar is dissolved.
   - **Sake**, 100ml
   - **Mirin**, 100ml
   - **Soy Sauce**, 100ml
   - **Sugar**, 15g
