---
title: "Air Fried Sweet Potato Chips"
description: "195°C for 12 minutes"
summary: "195°C for 12 minutes"
date: "2023-02-21T21:19:47"
category: "recipes"
series:
  - snacks
---

1. Peel and cut sweet potatoes to chip size.
   - **Sweet potato**, 1
2. Combine all ingredients in a bowl, make sure chips are evenly coated.
   - **Salt**
   - **Olive Oil**
   - **Other seasoning** -- e.g. garlic powder, onion powder, paprika, oregano, etc
3. Air fry at 195°C for 12 minutes.
