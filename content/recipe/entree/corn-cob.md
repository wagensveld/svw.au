---
title: "Corn Cob"
description: "It's corn"
summary: "It's corn"
date: "2023-02-28T16:56:37"
category: "recipes"
series:
  - "snacks"
---

Corn is pretty simple to make. The fun comes with the butter you spread on it. In reality just try a bunch of herbs and spices. I've left a few recipes below.

## Corn

### Air Fried

1. Spray corn cob with olive oil and add salt.
   - **Corn Cob**
   - **Olive Oil**, Sprayed
   - **Salt**, A pinch
2. Air fry corn at **180°C_** for 14 minutes.
3. Spread butter on corn. (See below)

### Pan Seared

1. Spray corn cob with olive oil and add salt.
   - **Corn Cob**
   - **Olive Oil**, Sprayed
   - **Salt**, A pinch
2. On a dry medium hot pan sear corn for 4 minutes at a time, or until slightly charred.
3. Spread butter on corn. (See below)

## Butter

### Indonesian Sweet Chilli

1. Combine
   - **Butter**, 40g -- Softened
   - **Red Chilli**, 1
   - **Sugar**, 1 tsp
   - **Salt**, 1/2 tsp
   - **Kecap Manis**, 1 tsp

### Garlic

1. Combine
   - **Butter**, 40g -- Softened
   - **Garlic**, 1 Clove, Minced
   - **Salt**, 1/2 tsp
