---
title: "Ajitsuke Tamago"
description: "Ramen Egg. The basic brine is 1:1:1, 50ml water to 50ml mirin to 50ml soy sauce."
summary: "Ramen Egg. The basic brine is 1:1:1, 50ml water to 50ml mirin to 50ml soy sauce."
date: "2022-09-23T16:34:01"
series:
  - "ramen"
category: "recipes"
---

The exact amount of brine needed depends on how many eggs you make, I find 150ml total liquid works well for 3 eggs in resealable bag.

1. Add all ingredients to a resealable bag. Let rest for 6 hours.
   - **[Soft-boiled Eggs](/recipe/toppings/soft-boiled-eggs/)**, 3
   - **Water**, 50ml
   - **Mirin**, 50ml
   - **Soy Sauce**, 50ml
