---
title: "Teriyaki Chicken"
description: "Quick way to prepare chicken for rice or a sandwich."
summary: "Quick way to prepare chicken for rice or a sandwich."
date: "2022-09-25T18:43:38"
series:
  - "ramen"
category: "recipes"
---

> Creates 3 teriyaki chicken thighs

1. Add chicken to a medium hot pan cook until slightly undercooked (~10 minutes). Remove chicken.
   - **Chicken Thighs**, 3 -- ~250g
2. Add teriyaki sauce to pan deglaze pan. The sauce will begin to bubble and thicken. Add the chicken, ensure an even coating.
   - **[Teriyaki Sauce](/recipe/teriyaki-sauce)**, 100ml
3. Serve.
