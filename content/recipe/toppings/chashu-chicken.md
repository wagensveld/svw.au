---
title: "Chashu Chicken"
description: "Primarily for ramen, but it could work on a sandwich."
summary: "Primarily for ramen, but it could work on a sandwich."
date: "2022-09-30T09:49:15"
category: "recipes"
series:
  - "ramen"
---

> Creates 3 chicken chashu

1. Roll up chicken thighs, season with a bit of salt and pepper, tie with twine.
   - **Chicken Thighs**, 3 -- ~250g
   - **Salt and Pepper**, To taste
2. Add chicken to small pot brown on all sides, then remove.
3. In a bowl mix water, soy sauce, mirin, sugar, garlic, add to pot.
   - **Water**, 250ml
   - **Soy Sauce**, 125ml
   - **Mirin**, 125ml
   - **Sugar**, 12g
   - **Garlic**, 3 Cloves -- Crushed
4. Deglaze with sake.
   - **Sake**, 60ml
5. Add chicken and simmer for about 25 minutes, rotating every few minutes to evenly distribute sauce.
6. Kill the heat and let cool to room temperature - the flavours will continue to soak.
7. Slice chicken then serve.
