---
title: "Soft Boiled Eggs"
description: "Usually I have no backstory or context for any of these recipes, I like to get straight to the point, but this time I have a multi sentence explanation for why this recipe is significant to me, and  in any case it's an egg recipe, there's no way I've SEO optimised this page well enough to have people stumble upon this by chance."
summary: "Usually I have no backstory or context for any of these recipes, I like to get straight to the point, but this time I have a multi sentence explanation for why this recipe is significant to me, and  in any case it's an egg recipe, there's no way I've SEO optimised this page well enough to have people stumble upon this by chance."
date: "2022-09-22T21:25:01"
category: "recipes"
images:
  - "images/soft-boiled-eggs-recipe.webp"
---

The story sort of starts with this screenshot I've had on my desktop for over 5 years now:

![Screenshot of Discord: "Sebastiaan @ 06/01/2017: how to make perfect soft boiled eggs: 1. get your water boiling in a pot (double ltp: use a kettle before hand to heat up quicker) 2. once water is boiling lower the heat to a simmer 3. put eggs in water 4. wait 7 minutes 5 remove eggs from simmering water 6. place eggs in cold water"](soft-boiled-eggs-recipe.webp)

I don't remember exactly where I came across the recipe, but it would have been early during my uni days in 2016 - when not having money meant two boiled eggs counted as a meal.

One of my great investments during university were some sticky notes I got for free during OWeek, where I kept the egg recipe on full display above my stove in my first and second apartment.

In 2017 a lot of my friends moved out for university. In our Discord server I wrote up the egg recipe - the only useful university advice I have.

Having written and screenshotted the recipe, I promptly threw away that sticky note.

Yet I haven't done the same to the screenshot?

Before retiring the laptop I used at the time I set up syncthing, the screenshot continues to live on all my devices, and now here online.

Looking at the recipe makes me really nostalgic, a lot has changed over the past 6 years, personally and globally. But one thing hasn't changed, these eggs are fantastic.

> Creates Eggs

1. Bring water to a boil (use a kettle if you live on student res and don't need to pay for electricity, use the stove if you live in an apartment where you don't need to pay for gas because it's covered by the strata levy).
   - **Water**
2. Use a needle to create a small hole in the egg shells (this is to prevent the shell from cracking).
   - **Egg**
3. Reduce heat to a simmer, but keep your eye on the heat if the temperature lowers too much. The aim is just shy of a rolling boil.
4. Add eggs for 7 minutes. (Do 6 and a half minutes if you want a really runny yolk.)
5. Kill heat and add eggs to ice water.
   - **Ice water**
6. While underwater tap the ends of the eggs with a spoon. The water will get underneath the shell making it easier to remove.
