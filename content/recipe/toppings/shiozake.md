---
title: "Japanese Salted Salmon (Shiozake)"
description: "Really simple salted salmon recipe."
summary: "Really simple salted salmon recipe."
date: "2022-04-18T12:16:25"
category: "recipes"
---

1. Splash a dash of sake on the salmon (not much, maybe 1/2 tsp per fillet).
   - **Salmon Fillet**
   - **Sake**
2. Pat down dry with paper towels.
3. Press in a generous amount of salt on all sides.
   - **Salt Flakes**
4. Move to sealed container lined with paper towels.
5. Let rest for two days.
6. Fry in pan.
