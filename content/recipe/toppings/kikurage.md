---
title: "Kikurage"
description: "Simple mushroom topping for ramen."
summary: "Simple mushroom topping for ramen."
date: "2022-10-14T19:37:01"
category: "recipes"
series:
  - "ramen"
---

1. Add dried black fungus to a bowl with water, let soak overnight.
   - **Dried black fungus**, 15g
   - **Water**, About a bowls worth
2. When the mushrooms have been rehydrated, cut into strips.
3. In a pan saute with remaining ingredients.
   - **Soy Sauce**, 7g
   - **Mirin**, 5g
   - **Sesame Oil**, 7g
