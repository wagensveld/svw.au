---
title: "Chicken Fajitas"
description: "Chicken Fajitas, what more is there to say."
summary: "Chicken Fajitas, what more is there to say."
date: "2021-10-18"
category: "recipes"
---

> Preheat oven to **_200°C_**.

1. Cut Chicken Breast into strips.
   - **Chicken Breast**, 500g
2. Combine Olive Oil, Chilli Powder, Ground Cumin, Garlic Powder, and Salt in a bowl.
   - **Olive Oil**, 2 Tbsp
   - **Chilli Powder**, 1 Tbsp
   - **Ground Cumin**, 1 tsp
   - **Garlic Powder**, 1 tsp
   - **Salt**, 1/2 tsp
3. Add Chicken to bowl and coat evenly.
4. Cut Capsicums and Onion into slices.
   - **Red Capsicum**, 1
   - **Yellow Capsicum**, 1
   - **Red Onion**, 1
5. Add Capsicums and Onion to bowl and mix.
6. Add Chicken, Capsicums, and Onion to baking sheet and roast for 15 minutes.
7. Turn oven heat up to **_200°C_** and roast for an additional 5 minutes.
8. Juice Lime and stir in Chicken, Capsicums, and Onion.
   - **Lime**, 1/2
