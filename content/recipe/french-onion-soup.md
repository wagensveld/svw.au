---
title: "French Onion Soup"
description: "Something simple, warm for winter."
summary: "Something simple, warm for winter."
date: "2021-11-16"
category: "recipes"
---

## Onion Soup

1. Dice onions.
   - **Onions**, 650g -- Usually 4 onions, I usually do 1 Red, 2 Yellow, and 3 Shallot
2. In a dutch oven, melt butter and add onion, until very soft (it doesn't need to be chocolatey brown). Stir often.
3. Add red wine, simmer until most has evaporated.
   - **Red Wine**, 1/4 Cup
4. Add chicken stock, thyme, bay leaf, fish sauce, cider vinegar. Mince garlic and add. Stir.
   - **Chicken Stock**, 900ml
   - **Sprig Thyme**, 1
   - **Bay Leaf**, 1
   - **Fish Sauce**, 1/2 tsp
   - **Cider Vinegar**, 1/2 tsp
   - **Garlic**, 1 Clove
5. Add salt and pepper to taste.
   - **Salt**, To taste
   - **Pepper**, To taste

## Bread

> Preheat oven to **_250°C_**.

1. Mince garlic, spread on bread, give a pinch of Italian herbs, sprinkle on cheese and bake in the oven until bread is toasted and cheese is melted.
   - **Rustic Bread**, 1 slice per bowl of soup
   - **Garlic** - 1 Clove per  slice of bread
   - **Italian Herbs**, To taste
   - **Cheese**, Enough to cover bread
2. Serve in bowl with onion soup.
