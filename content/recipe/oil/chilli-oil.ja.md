---
title: "ラー油"
description: "何にでも追加できます。"
summary: "何にでも追加できます。"
date: "2021-10-14T22:50:17"
images:
  - "images/chilli-oil.webp"
category: "recipes"
---

> 1 リットル作る

## 材料

| 材料               | 量           |
| :----------------- | :----------- |
| 植物油             | 750ml        |
| 大茴香             | 5 個         |
| シナモンスティック | 1 本         |
| ローリエ           | 2 枚         |
| ペッパーコーン     | 大さじ 3     |
| カルダモンのさや   | 2 個         |
| ジンジャーパウダー | 大さじ 1     |
| クローブ           | 小さじ 2 tsp |
| にんにく           | 3 かけ       |
| エシャロット       | 2 個         |
| チリフレーク       | 110g         |
| 塩                 | 大さじ 1     |

## 作り方

1. 油、大茴香、シナモン、ローリエ、ペッパーコーン、カルダモンのさや、ジンジャーパウダー、クローブを冷たい鍋に入れます。
   - 植物油 750ml
   - 大茴香 5 個
   - シナモンスティック 1 本
   - ローリエ 2 枚
   - ペッパーコーン大さじ 3
   - カルダモンのさや 2 個
   - ジンジャーパウダー大さじ 1
   - クローブ小さじ 2
2. にんにく半分、鍋に入れる。
   - にんにく 3 かけ
3. エシャロット 1 個を 4 等分に切り、鍋に加える。
   - エシャロット 1 個
4. 耐熱容器を用意し、チリフレーク、塩を入れます。
   - チリフレーク 110g
   - 塩小さじ 1
5. 残りのエシャロットをさいの目に切って容器に入れる。
   - エシャロット 1 個
6. 鍋を 110 ～ 120C に温め、30 ～ 60 分煮込みます。
7. 油を容器にこし、均一に分布するように混ぜます。
