---
title: "Chilli Oil"
description: "Add it to anything."
summary: "Add it to anything."
date: "2021-10-14T22:50:17"
images:
  - "images/chilli-oil.webp"
category: "recipes"
---

> Creates 1 litre

1. Add oil, star anise, cinnamon, bay leaves, peppercorns, cardamom pods, ginger powder, cloves to a cold pot.
   - **Vegetable Oil**, 750ml
   - **Star Anise**, 5
   - **Cinnamon Stick**, 1
   - **Bay Leaves**, 3
   - **Peppercorns**. 3 Tbsp
   - **Cardamom Pods**, 2
   - **Ginger Powder**, 1 Tbsp
   - **Ground Cloves**, 2 tsp
2. Half garlic, add to pot.
   - **Garlic**, 3 Cloves
3. Quarter 1 shallot, add to pot.
   - **Shallot**, 1
4. Prepare a heat proof container, add chilli flakes, and salt.
   - **Chilli Flakes**, 110g
   - **Salt**, 1 tsp
5. Dice the remaining shallot and add to container.
   - **Shallot**, 1
6. Bring pot to **_110°C_** ~ **_120°C_**, let simmer for 30 ~ 60 minutes.
7. Strain oil into container, mix to ensure even distribution.
