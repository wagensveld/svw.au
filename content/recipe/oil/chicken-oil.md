---
title: "Chicken Oil"
description: "I thought I wrote this up weeks ago but Google Search Console told me I had 404 errors on my website."
summary: "I thought I wrote this up weeks ago but Google Search Console told me I had 404 errors on my website."
date: "2022-10-19T18:56:08"
series:
  - "ramen"
category: "recipes"
---

1. Add chicken skin to fry pan fry until oil is released and skin is crispy.
   - **Chicken skin**, 130g

You can use the skin as a snack or a topping.
