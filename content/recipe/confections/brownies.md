---
title: "Brownies"
description: "I had a couple of eggs which were about to expire. They went to good use. As an aside: are brownies biscuits or cakes? Though biscuits and cookies should be the same thing, it seems a lot more natural to call brownies cookies than biscuits."
summary: "I had a couple of eggs which were about to expire. They went to good use. As an aside: are brownies biscuits or cakes? Though biscuits and cookies should be the same thing, it seems a lot more natural to call brownies cookies than biscuits."
date: "2022-03-13T15:39:22"
images:
  - "images/brownies.webp"
series: ["biscuits", "cakes"]
category: "recipes"
---

> Preheat your oven to **_160°C_**.

1. In a bowl mix:
   - **Sugar**, 250g
   - **Cocoa Powder**, 65g
   - **Butter**, 140g -- Softened
2. Wait until the bowl has cooled (if butter is hot).
3. Add the following ingredients to the bowl, mixing until everything is combined.
   - **Eggs**, 2
   - **Flour**, 65g
   - **Salt**, 1/4 tsp
   - **Vanilla Extract**, 1/2 tsp
   - _(Optional)_** Desiccated Coconut**, 65g
4. Pour batter onto lined baking tray.
5. Bake for 25 minutes, then let rest.
