---
title: "Fluffy Pancakes"
description: "Harajuku style pancakes."
summary: "Harajuku style pancakes."
date: "2021-08-19T19:25:28"
category: "recipes"
---

1. Sift flour, baking powder, and sugar in bowl, then combine.
   - **Self Raising Flour**, 1 Cup -- Sifted
   - **Baking Powder**, 1/2 tsp
   - **Sugar**, 1/4 Cup
2. In another bowl combine egg, buttermilk, and vanilla, whisk until just combined.
   - **Egg**, 1
   - **Buttermilk**, 3/4 Cup
   - **Vanilla Extract**, 1/4 tsp
3. Make a well in the centre of the dry ingredients, and pour the liquid mixture in.
4. Mix together and add the mayonnaise, mix well to combine.
   - **Japanese Mayonnaise**, 1 Tbsp
5. Add butter / oil to frying pan on low heat, pour pancake mixture into mould (fill to about 70%).
6. Place lid on the frying pan, leave it for 15 minutes.
7. Once little bubbles appear, and the sides are cooked, flip with an egg flipper, cook for another 15 minutes.
8. Serve with favourite topping.
