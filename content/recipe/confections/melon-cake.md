---
title: "Melon Cake"
description: "Here's the cake I made for my girlfriend's birthday. While in Japan we really enjoyed Japanese melons, I couldn't find any for sale here so I ended up using a honeydew melon. Which definitely was not as sweet. All in all it was a fun cake to bake."
summary: "Here's the cake I made for my girlfriend's birthday. While in Japan we really enjoyed Japanese melons, I couldn't find any for sale here so I ended up using a honeydew melon. Which definitely was not as sweet. All in all it was a fun cake to bake."
date: "2022-11-09T18:43:53"
showImagesBottom: true
images:
  - "images/melon-cake.webp"
series: ["cakes"]
category: "recipes"
---

## Cake Base

> Preheat your oven to **_170°C_**. Also begin boiling water in a pot, we'll double boil our first mixture.

1. In a heat safe bowl mix:
   - **Eggs**, 4
   - **Sugar**, 100g
   - **Honey**, 15g
   - **Vanilla Extract**, 5g
2. Double boil while mixing until mixture reaches **_40°C_**. Put aside.
3. In a separate bowl mix butter and milk while double boiling until the mixture reaches **_60°C_**.
   - **Unsalted Butter**, 35g
   - **Milk**, 55g
4. Whisk the egg mixture until fluffy.
5. Add flour to egg mixture and combine.
   - **Flour**, 120g
6. Add a little bit of the egg mixture to the butter mixture. Combine well.
7. Add the butter mixture to the egg mixture. Add a tiny bit of green food dye. Combine well.
   - **Green Food Dye**
8. Line and oil a round baking tin. Add mixture.
9. Bake for about 35~40 minutes at **_170°C_**.
10. Let cool.

## Undyed Cream

1. In a bowl whisk until peaks form.
   - **Cream**, 310g
   - **Sugar**, 31g
2. Store in the fridge until needed.

## Melon

1. Remove rind and seeds. Cut into 1cm slices.
   - **Honeydew Melon**, 1/4
2. If possible pat dry.

## Putting most of it together

### Preparation

1. Slice the cake into 1cm sheets.
   - **Cake**, baked earlier.
2. Line a cake-sized bowl with cling wrap.
   > You want this to be quite a round bowl. This is where the cake gets its shape.
3. Cut a sheet of the cake roughly to the circumference of the bottom of the bowl, place it in, add a layer of cream, add a layer of melon roughly cut to the circumference of the bowl. Repeat until you have filled the bowl, finishing with a layer of cake.
   - **Cake**, sliced earlier.
   - **Undyed cream**, prepared earlier.
   - **Melon**, prepared earlier.
4. Put in the freezer for **_30 minutes_**.
5. With the leftover melon cut a small T shape to emulate the stem of the melon. You can snack on the rest.
6. Keep the leftover undyed cream.

## Dyed Cream

1. Whisk until peaks form.
   - **Cream**, 310g
   - **Sugar**, 31g
   - **Green Food Dye**

## Putting it all together

1. Remove cake from bowl.
   - **Cake**, prepared earlier.
2. Cover cake with dyed cream. Smooth it out.
   - **Dyed cream**, prepared earlier.
3. Use the leftover undyed cream in a piping bag to emulate the look of a melon.
   - **Undyed cream**, prepared earlier.
4. Add the melon stem cutout to the top.
   - **Melon stem**, prepared earlier.
5. Keep in the fridge for a while to firm up.
