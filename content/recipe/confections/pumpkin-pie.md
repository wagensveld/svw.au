---
title: "Pumpkin Pie Recipe"
description: "They don't sell pumpkin pie in Australia, here's my recipe to alleviate the pain that brings me."
summary: "They don't sell pumpkin pie in Australia, here's my recipe to alleviate the pain that brings me."
date: "2021-07-31T17:38:45"
# images:
#  - "images/pumpkin-pie.webp"
series: ["pie"]
category: "recipes"
---

> Creates 1 pie

> You'll first want to make a [sweet pie base](/recipe/confections/sweet-pie-base)

## Pumpkin filling

1. Boil pumpkin in a pot until soft.
   - **Pumpkin**, 750g
2. Drain and let cool.
3. Combine all ingredients in a mixer, until smooth.
   - **Boiled Pumpkin**
   - **Sweetened Condensed Milk**, 1 Can (400g)
   - **Eggs**, 2
   - **Ground Cinnamon**, 1 tsp
   - **Ground Ginger**, 1/2 tsp
   - **Ground Nutmeg**, 1/2 tsp
   - **Salt**, 1/2 tsp

## Pie

> Preheat oven to **_220°C_**.

1. Add Pumpkin filling to Pie Base
   - **Pumpkin filling**
   - **Pie Base**, 1
2. Bake for 15 minutes
3. Reduce heat to **_180°C_**, bake for 35 minutes or until filling is set.
4. Let cool.
