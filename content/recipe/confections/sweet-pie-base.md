---
title: "Sweet Pie Base"
description: "Basic pie base recipe for sweet pies."
summary: "Basic pie base recipe for sweet pies."
date: "2021-08-06T15:20:38"
series: ["pie"]
category: "recipes"
---

> Creates 1 pie

> Preheat oven to **_190°C_**

1. Combine all ingredients in a mixer, until smooth.
   - **Water**, 70ml
   - **Flour**, 125g
   - **Salt**, 3.5g
   - **Butter**, 75g
2. Roll on flat surface, place in mould or dish.
3. Bake in oven until golden brown. (15 mins)
