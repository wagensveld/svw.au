---
title: "Rondos"
description: "Dutch almond cakes."
summary: "Dutch almond cakes."
# images:
#  - "images/rondos.webp"
date: "2021-10-30T14:51:52"
series: ["biscuits", "cakes"]
category: "recipes"
---

> Creates 10

## Dough

> Preheat oven to **_175°C_**.

1. In a bowl combine.
   - **Flour**, 200g -- Sifted
   - **Baking Powder**, 5g
   - **Butter**, 150g
   - **Sugar**, 100g
   - **Salt**, a pinch
2. Use your hands to press mixture together until it forms a dough, set aside.

## Almond Paste

1. Grind blanched almonds in a food processor until a fine powder is formed.
   - **Blanched Almond**, 75g
2. In a bowl combine almond powder, sugar, lemon zest, egg, and water.
   - **Almond Powder**, 75g
   - **Sugar**, 75g
   - **Zest of 1/2 Lemon**
   - **Egg**, 1
   - **Water**, 15ml

## Baking

### Ingredients

1. Split and roll dough into 25g balls. There should be a total of 20. Flatten 10 and press into greased cupcake tin.
2. Spoon in almond paste on top.
3. Flatten the remaining 10 balls and cover almond paste.
4. Combine egg and water to create an egg wash.
   - **Egg**, 1
   - **Water**, 15ml
5. Press almond onto each rondo.
   - **Almonds**, 10
6. Bake at **_175°C_** for 20 minutes.
