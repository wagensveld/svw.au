---
title: "Caramel Slices"
description: "Just like with brownies, I don't actually know how to categorise caramel slices, they're really not a biscuit, but I think it's overkill to have a slice series on this website."
summary: "Just like with brownies, I don't actually know how to categorise caramel slices, they're really not a biscuit, but I think it's overkill to have a slice series on this website."
date: "2021-10-14T11:31:53"
# images:
#  - "images/caramel-slices.webp"
series: ["biscuits"]
category: "recipes"
---

## Base

> Preheat oven to **_180°C_**.

1. Mix all ingredients, press into pan to desired thickness.
   - **Flour**, 1 Cup
   - **Brown Sugar**, 1/2 Cup
   - **Desiccated Coconut**, 1/2 Cup
   - **Butter**, 125g
2. Bake for 15 minutes. Let cool afterwards.

## Caramel Sauce

> Alternatively use Biscoff spread to make a Biscoff slice.

1. Place butter, sugar, vanilla extract in a saucepan over low heat, mix.
   - **Butter**, 125 g
   - **Brown Sugar**, 1/2 Cup
   - **Vanilla Extract**, 1 tsp
2. When bubbles start to appear, add condensed milk, whisk for 5 minutes.
   - **Sweetened Condensed Milk**, 400 g
3. Pour onto base.

## Chocolate

1. Combine all ingredients.
   - **Chocolate**, 200 g (Softened)
   - **Vegetable Oil**, 1 Tbsp
2. Pour onto caramel, refrigerate for at least an hour.
