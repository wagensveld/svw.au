---
title: "Ontbijtkoek"
description: "A Dutch spiced breakfast cake."
summary: "A Dutch spiced breakfast cake."
date: "2021-12-03"
series: ["bread", "cake"]
category: "recipes"
---

> Creates 1 loaf

> Preheat oven to **_150°C_**.

1. Combine all ingredients in a mixer, until smooth.
   - **Rye Flour**, 120g
   - **AP Flour**, 120g
   - **Baking Powder**, 3 tsp
   - **Ground Cardamom**, 1 tsp
   - **Ground Cinnamon**, 1 tsp
   - **Ground Ginger**, 1/2 tsp
   - **Coriander Seed**, 1/2 tsp
   - **Ground Clove**, 1/4 tsp
   - **Ground Nutmeg**, 1/4 tsp
   - **Black Pepper**, 1/4 tsp
   - **Ground Aniseed**, 1/4 tsp
   - **Salt**, pinch
   - **Brown Sugar**, 100g
   - **Honey**, 170g
   - **Molasses**, 75g
   - **Vanilla Extract**, 1 tsp
   - **Milk**, 250ml -- Room temperature
2. Line loaf tin with baking paper and add mixture, cover with aluminium foil.
3. Bake at **_150°C_** for 80 minutes.
4. Let cool completely in tin. Remove foil when cool enough to touch.
