---
title: "Chipotle Chicken"
description: "Chipotle chicken for burritos."
summary: "Chipotle chicken for burritos."
date: "2022-03-27"
category: "recipes"
---

1. Mix all ingredients in a bowl.
   - **Chicken Breast**, 700g
   - **Chipotle in Adobo Sauce**, 460g
   - **Water**, 1/2 Cup
   - **Olive Oil**, 2 Tbsp
   - **Cumin**, 1 Tbsp
   - **Oregano**, 2 tsp
   - **Honey**, 1 tsp
   - **Pepper**, 1/2 tsp
   - **Garlic**, 2 Cloves -- Minced
2. Cover and put in fridge for at least 1 hour (overnight is even better).
3. Cook in pan with a little bit of oil. Chop into bite sized pieces, serve in burrito.
