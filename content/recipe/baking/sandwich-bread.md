---
title: "Sandwich Bread"
description: "As of writing this I have made this bread 3 times, not once using it for sandwiches, but rather toast."
summary: "As of writing this I have made this bread 3 times, not once using it for sandwiches, but rather toast."
date: "2021-08-06T18:57:11"
series: ["bread"]
category: "recipes"
---

> Creates 1 loaf of bread

1. Mix oats and water, let rest til lukewarm (20 minutes).
   - **Oats**, 60g
   - **Boiling Water**, 400ml
2. Add yeast and let bloom (5 minutes)
   - **Yeast**, 1 1/4 tsp
3. Add butter, salt, flour and sunflower seeds, mix using a stand mixer.
   - **Butter**, 45g
   - **Salt**, 1 tsp
   - **Flour**, 430g
   - **Sunflower Seeds**, 60g
4. Place dough in lightly oiled bowl and cover until doubled in size (an hour).
5. Punch down dough, roll out into a square, then tightly roll into a loaf.
6. Place dough in lightly oiled bread tray and cover until doubled in size (an hour). Use this time to preheat your oven to **_180°C_**.
7. Bake for 40 minutes.
