---
title: "Italian Herbs and Cheese Bread"
description: "You too can be Subway."
summary: "You too can be Subway."
date: "2021-10-05T18:41:28"
images:
  - "images/italian-herbs.webp"
series: ["bread"]
category: "recipes"
---

> Creates 1 11 inch sandwich

1. Mix yeast and water, and let bloom (5 minutes).
   - **Lukewarm Water**, 60ml
   - **Yeast**, 1 tsp
2. Add olive oil, salt, and flour, mix using a stand mixer.
   - **Olive Oil**, 1 Tbsp
   - **Salt**, 1/4 tsp
   - **Flour**, 100g
3. Place dough in lightly oiled bowl and cover until doubled in size (an hour). Use this time to preheat your oven to 180 c.
4. Punch down dough, roll out into an 11 inch sub, place in tray.
5. Grate Parmesan cheese and sprinkle Italian herbs to taste.
   - **Parmesan Cheese**, to taste
   - **Italian Herbs**, to taste
6. Bake for 20 minutes.
7. Remove from oven and over with towel.
