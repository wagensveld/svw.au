---
title: "Shokupan"
description: "Japanese soft milk bread."
summary: "Japanese soft milk bread."
date: "2021-10-18T04:32:06"
category: "recipes"
---

> Creates 2 small loaves

## Yudane

### Ingredients

| Ingredient     | Amount |
| :------------- | :----- |
| Bread Flour    | 50g    |
| Water (boiled) | 40ml   |

### Preparation

1. Mix flour and water for until even.
   - 50g Bread Flour
   - 50ml Water (boiled)
2. Cling wrap and refrigerate overnight.
   - Yudane

## Shokupan

### Ingredients

| Ingredient        | Amount |
| :---------------- | :----- |
| Milk              | 150ml  |
| Sugar             | 15g    |
| Instant Yeast     | 3g     |
| Butter (unsalted) | 10g    |
| Yudane            | 1      |
| Bread Flour       | 200g   |
| Salt              | 5g     |

### Preparation

1. Add milk, sugar, butter, yeast into mixer bowl.
   - 150ml Milk
   - 15g Sugar
   - 3g Yeast
   - 10g Butter
2. Tear up yudane and add to the mixer bowl.
   - Yudane
3. Add flour and salt.
   - 200g Bread Flour
   - 5g Salt
4. Attach kneading hook to mixer, mix on 1 until combined.
5. Turn up speed to 5 or 6 for 20 minutes.
6. Roll dough and place into a greased bowl, wrap with cling wrap and let rise for 1 hour. If you can poke the dough without it bouncing back, it's ready.
7. Punch dough down and cut into two equal parts, roll them.
8. cover rolled doughs with a wet cloth and let rest for 20 minutes.
9. Roll out each dough with a rolling pin, have it narrow enough to fit into your bread tin.
10. Fold dough tightly (try to avoid letting air in folds), and add to a greased bread tin.
11. Preheat oven to 185C.
12. Let dough rest for 30 minutes.
13. Bake dough for 25 minutes.
14. (Optional) Whisk an egg, and lightly brush over the bread.
