---
title: "Quick Ciabatta Bread"
description: "This is essentially a copy of Jason Molina's recipe."
summary: "This is essentially a copy of Jason Molina's recipe."
date: "2022-11-30T19:40:03"
images:
  - "images/quick-ciabatta.webp"
category: "recipes"
---

> Creates 4 loaves of bread

1. Roughly mix all ingredients in a bowl. Let rest for 10 minutes.
   - **Flour**, 500g
   - **Water**, 475ml
   - **Yeast**, 2 tsp
   - **Salt**, 15g
2. Use hook attachment in stand mixer to combine. It will form a cohesive dough. About 20 to 30 minutes.
3. Add to well oiled container and cover. Wait for it to triple in size. About 3 hours.
4. Place dough onto heavily dusted counter. Cover with a damp cloth for about an hour. Use this time to preheat the oven to **_250°C_**.
5. Flip loaf into oiled bread tin, bake for 15-25 minutes.
