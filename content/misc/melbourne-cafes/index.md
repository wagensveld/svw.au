---
title: "Melbourne Cafes"
description: "I don't actually have the best palette when it comes to coffee. Here are the opinions of someone who knows what single origin and blends are. But isn't in deep enough to identify one while cupping."
summary: "I don't actually have the best palette when it comes to coffee. Here are the opinions of someone who knows what single origin and blends are. But isn't in deep enough to identify one while cupping."
date: "2024-05-29T22:23:15"
category: "misc"
series:
  - "melbourne"
  - "coffee"
---

Melbourne is known for having a great coffee culture. In general you can walk into any cafe and get a pretty nice cup of coffee. Here's a list of some of the places I've been, the type of flavour the coffee had, and whether I would reccomend it. At the moment it's all going to be pretty high level, I'm initially writing this from memory in an afternoon. But later I hope to go back and write more substantial notes.

I tend to like milk based drinks, my go to will be a magic if it's in the CBD, or a flat white or latte if I'm in the suburbs, or if the cafe tends to have a fairly light roast (I'm looking at you Market Lane).

## CBD

### North West - Spencer to Elizabeth, Bourke to La Trobe

- **Patricia Coffee Brewers**

```
Reccomend: Excellent
Address: 493-495 Little Bourke Street, Melbourne
Beans: Patricia Coffee Brewers
Last Went: Prior to writing this post
```

Balanced to dark, and quite strong. I was lucky enough to have a job near Patricia's. Their coffee is fantastic, and the line you have to wait is proof of that. Best time to go is probably 10:30 to 11 AM just after the morning rush, and before lunch begins.

If you want to fulfil the Melbourne dream of drinking great coffee in a laneway, give them a shot.

- **Brother Baba Budan**

```
Reccomend: Excellent
Address: 359 Little Bourke St, Melbourne
Beans: Seven Seeds
Last Went: Prior to writing this post
```

Balanced, maybe slightly dark but all in all a smooth drink. When I moved back to Melbourne this was the café recommended to me.{{% years-since 2015 %}} years later and it's still up there.

It's a popular place in a touristy area, lines can get long, but they're pretty quick with their turnaround.

- **MAKER**

```
Reccomend: High
Address: 387 Little Bourke Street, Melbourne
Beans: Maker Coffee
Last Went: 2024-06-01
```

Probably the smoothest coffee I've had in the CBD, not that acidic, perhaps subtlety bitter. I'm surprised it has taken this long to visit them. I feel like I would enjoy this place most as an afternoon coffee, while Brother Baba Budan half a block is more of a morning coffee.

- **- Manchester Press**
```
Reccomend: High
Address: 8 Rankins Lane, Melbourne
Beans: ONA Coffee
Last Went: 2024-06-09
```

More of a brunch place than a coffee shop. It has nice balanced coffee nonetheless. I have it ranked as high, like MAKER, I prefer MAKER's coffee, but for a date I think Manchester Press would be nicer.

- **Cafe Court**

```
Reccomend: Fine
Address: 536 Lonsdale Street, Melbourne
Beans: Undercover Roasters
Last Went: 2024-06-08
```


### South West - Spencer to Elizabeth, Flinders to Bourke

- **Code Black Coffee**

```
Reccomend: Excellent
Address: 630 Little Collins Street, Melbourne
Beans: Code Black Coffee Roasters
Last Went: Prior to writing this post
```

Balanced to dark, but they have a lot of options you can choose from. I went once before COVID, and then they shut down for the entire pandemic. I loved the aesthetic though, the entire place was black. I'm happy they've reopened, but they've lost a bit of the black motif. It's still physically dark in the room, but now they have beans on display which have various colours. There's often a line but nothing too crazy, the issue is it's a small shop, it can feel a bit cramped. Still fantastic coffee though.

- **Project Zero Coffee**

```
Reccomend: Excellent
Address: 140 King Street, Melbourne
Beans: Project Zero Coffee
Last Went: Prior to writing this post
```

A newcomer to the scene. Their coffee is reasonably balanced, perhaps a little darker than normal. As far as I know this physical café is the sole place where you can get their beans.

- **Cherry and Twigs**

```
Reccomend: High
Address: 247 Flinders Lane, Melbourne
Beans: Symmetry Coffee Roasters
Last Went: Prior to writing this post
```

Darker roast than average for Melbourne, chocolately. I have only been once, and that was to drop a laptop off during covid. Out of all the entries I have written so far this one feels like a hidden gem the most.


### North - Queen Victoria Market, Melbourne Central, Emporium, QV, and Surrounds

#### Queen Victoria Market

- **Market Lane Coffee**

```
Reccomend: High
Address: Dairy Produce Hall, Queen Victoria Market, Melbourne
Beans: Market Lane Coffee
Last Went: Prior to writing this post
```

A bit acidic, a nice light roast.

Market Lane have a few cafes around the city, I think the one at the dairy produce hall at QVM is the one which comes to mind when I think of "Market Lane" in the city. This place gets quite busy; it's a popular tourist spot, a place where people get groceries, and it's fairly confined.

As an aside I really like the Market Lane tote bags, they're good quality and look nice. Throughout the city you'll see people with the bags more often than you'd expect. I think they change the colour every 6 months or so.

```
Reccomend: High
Address: 462 Queen Street, Melbourne
Beans: Market Lane Coffee
Last Went: 2024-06-02
```

Of the Market Lanes near Vic Market this is my favourite, it's a really small nook, and doesn't get too busy.

```
Reccomend: High
Address: 83-85 Victoria Street, Melbourne
Beans: Market Lane Coffee
Last Went: Prior to writing this post
```

This location is also great, it gets a bit busy at times. Determining where the queue is, and who's just waiting for their coffee can be a little confusing at times because they have two entrances on opposite sides.

- **Little League**

```
Reccomend: High
Address: Container 1 String Bean Alley, Queen Victoria Market, 513 Elizabeth Street North Melbourne
Last Went: 2024-06-22
```

Another place I'm surprised it took this long to try out. I really like their coffee, quite balanced.

- **ST. Ali & THE QUEEN**
```
Reccomend: Read below
Address: 1 Dhanga Djeembana Walk, Melbourne
Beans: ST. Ali
Last Went: Prior to writing this post
```

Do you separate the coffee from the café? If you can the coffee is good. It probably even serves as a reasonable baseline for what "good" coffee is in this city. I don't know how they compare to other cafes in the city (perhaps it's a matter of a successful business having the spotlight shinned on it). I have heard unkind things about their management.

During the pandemic, while there was a shortage of antigen tests, [they offered two tests if you spent $160, and later apologised for it](https://www.theguardian.com/australia-news/2022/jan/18/melbourne-coffee-roaster-panned-for-offering-free-rapid-antigen-tests-to-vip-customers). Perhaps I'm being petty, but this has left me feeling conflicted about recommending this place.

I think that if you're able to put that aside it's a serviceable coffee. Market Lane nearby can be a little acidic for some people's preferences. St. Ali tends to be darker.

### Emporium

- **SAINT DREUX**

```
Reccomend: Good
Address: Emporium Shop LG-30, 287 Lonsdale Street, Melbourne
Beans: BENCH COFFEE CO.
Last Went: 2024-05-01
```

I may even bump it up to a highly recommended later. It's a reasonably balanced cup, a little dark. Out of all the options in the Melbourne Central Emporium shopping complex this has been my favourite. Perhaps I haven't been to enough café's, but it seems like it's hard to find places which use BENCH beans, which is a shame I quite like them.

<!-- ### North East Elizabeth to Spring, Bourke to La Trobe -->

### South East Elizabeth to Spring, Flinders to Bourke

- **Dukes Coffee Roasters**

```
Reccomend: Excellent
Address: 247 Flinders Lane, Melbourne
Beans: Dukes Coffee
Last Went: Prior to writing this post
```

Balanced. I used to frequent it, (un)fortunately they have gotten popular in previous years. The lines are too long for me to bother with. But still one of my favourites.

## Docklands

- **Story**

```
Reccomend: Excellent
Address: 700 Bourke Street, Docklands
Beans: Dukes Coffee
Last Went: Prior to writing this post
```

Tends to be a pretty balanced cup. It gets pretty busy between 9 and 10 AM, as well as over lunch.

Story will always have a special place in my heart during my first job my team would go almost every day, it got to the point where they wouldn't even ask my order.

- **Tukk and Co**

```
Reccomend: Excellent
Address: 720 Bourke Street, Docklands
Beans: ONA Coffee
Last Went: Prior to writing this post
```

Lighter roast. It doesn't get as busy as Story, but still gets fairly busy.

We would often to go Tukk and Co if Story was too busy. But looking back I wish I went here more often, perhaps my tastes have changed but I think I may prefer their coffee to Story's now, I really like ONA's beans.

- **Focaccino**

```
Reccomend: Good
Address: 720 Bourke Street, Docklands
Beans: Roasting Warehouse Specialty Coffee
Last Went: Prior to writing this post
```

Balanced cup. I think their coffee is fine, if there weren't other cafes in the area I would happy have it as my go to. Things may have changed since I have been but they were a good deal cheaper and had larger sizes than the other cafes.

- **shall we coffee**

```
Reccomend: Fine
Address: 50-96 Waterfront Way, Docklands
Beans: shall we coffee
Last Went: 2024-05-31
```

A bit bitter, but probably a medium to dark roast? I want to like them. It looks like they roast their own beans. I love that, and I want them to do well. But the coffee misses the mark for me.

- **Daybreak Cafe**

```
Reccomend: Fine
Address: 90 Waterfront Way, Docklands
Beans: shall we coffee
Last Went: Prior to writing this post
```

Daybreak Cafe is actually a part of *shall we coffee*, I've had a slightly better experience there, I think they may set up their machines slightly differently. That or it's just placebo.

- **Penny Speciality Coffee**


```
Reccomend: Fine
Address: The District Docklands SCG01, G02/442-446 Docklands Drive, Docklands
Beans: Constance Coffee
Last Went: 2024-06-04
```

I think in some ways like shall we coffee, I find it a little too bitter for my liking. And like SWC they use what seems like a smaller roaster - which again I love.

### Collingwood

- **Proud Mary Coffee**

```
Reccomend: Highly
Address: 172 Oxford Street, Collingwood
Beans: Proud Mary Coffee
Last Went: 2024-06-29
```

Another place I'm surprised it took this long to go to. This place gets highly recommended. But tbh I thought it was fine, but not miles ahead of the other cafe's like I had heard. Over all balanced coffee, worth checking out if you're in the area.

### Brunswick

- **ONA**

```
Reccomend: Excellent
Address: 22 Ovens Street, Brunswick
Beans: ONA Coffee
Last Went: Prior to writing this post
```

I really love ONA coffee, one of my favourites. They ask which beans you'd like when ordering your coffee, my two favourites are "Raspberry Candy" - lighter and acidic, and "Gateway" - really balanced and smooth.


## To Try
- 1983 Espresso & Panini Bar
- 279
- Abacus Bar & Kitchen
- ACoffee
- All Are Open
- Archie's All Day
- Arepa Days
- AU79 Abbotsford
- Auction Rooms
- Aunty Pegs
- Bawa
- BENCH Coffee Co
- Bonnie Coffee
- Burnside
- Cafe Bourgeois
- Cafenatics
- Chiaki
- Cibi
- Clementine
- Coffee Logic
- Coffee Supreme
- Convoy
- Coracle
- Dame
- Elster
- Everyday Coffee
- Fenton
- First Love Coffee
- Florian
- Grain Store
- Halcyon Days
- Higher Ground
- Ima Asa Yoru
- Juniper
- Lagotto
- Levi
- Liminal
- Little Bang Espresso
- Mister Hoffman
- Monk Bodhi Bharma
- Moon Mart
- Mr. Summit Cafe & Bar
- Napier Quarter
- Niccolo
- Nine Yards
- No. 19
- Ondo Armadale
- Operator San
- Otherside Coffee
- Pillar of Salt
- Plus Coffee & Co
- Prior
- Proud Mary Cafe
- Quists Coffee
- Rudimentary
- Rustica
- Saluministi
- Seven Seeds
- Sloane Ranger
- Small Batch Coffee Roasting Co.
- Square One Rialto
- Stagger Lee's
- Sunhands
- Tall Timber
- Terror Twilight
- The Hardware Societe
- The Kettle Black
- The League of Honest Coffee
- The Penny Drop
- The Terrace
- Traveller Coffee
- Vacation Coffee
- Veneziano
- Vertue Coffee Roasters
- Via Porta
- Wide Open Road


## To Write about

- Cocobei
- Corsia
- Fiord
- Industry Beans
- Little Rogue
- Oli & Levi
- Peter & Co Cafe
