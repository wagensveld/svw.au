---
title: "SSM Diff"
description: "pythonで書かれたSSM Diffツールのフォークのフォークのフォークのフォーク。"
summary: "pythonで書かれたSSM Diffツールのフォークのフォークのフォークのフォーク。"
date: "2021-12-29T21:28:56"
images:
  -  "images/ssm.webp"
category: "projects"
---

[コードはここにあります。](https://gitlab.com/wagensveld/ssm-diff)

SSM Diff は、[GitHub の runtheops](https://github.com/runtheops)によって作成されたツールです。このコードには複数のフォークが既に存在し、それぞれが依存するテクノロジの変更に対応しています。私はたまたま Python3.9 と互換性がありませんでした。

このツールの使い方は非常に簡単です。

1. 端末に AWS CLI をセットアップします。(環境変数ベースの認証プロセスを使用している場合は、`AWS_DEFAULT_REGION` が設定されていることを確認してください。)
2. ssm-diff ディレクトリで、依存関係がインストールされていることを確認します: `pip install -r requirements.txt`。
3. `ssm-diff init`を実行して、デフォルトの`parameters.yml`を初期化します。
4. `ssm-diff pull`を実行して、AWS から parameters.yml ファイルにパラメーターを複製します。
5. `ssm-diff plan`を実行して、SSM パラメータに加えられた変更を確認します。
6. `ssm-diff push`を実行して、SSM パラメータに加えられた変更をプッシュします。

`/Dev/DBServer/MySQL/app1`、`/Dev/DBServer/MySQL/app2`、`/Dev/EC2/Web/app1`、`/Test/DBServer/MySQL/app1`、`/Test/DBServer/MySQL/app2`および`/Test/EC2/Web/app1`は次のように表示されます。

```yaml
Dev:
  DBServer:
    MySQL:
      app1: <value>
      app2: <value>
  Web:
    app1: <value>
Test:
  DBServer:
    MySQL:
      app1: <value>
      app2: <value>
  Web:
    app1: <value>
```

引数も渡すことができます。 特に:

- `-f` 使用する yaml ファイルを指定します(複数のアカウント/リージョンに役立ちます)。
- `--engine` 使用する差分エンジンを指定します(デフォルトは DiffResolver)。
- `--profile` 使用する AWS プロファイルを指定します(デフォルトは profile です)。
- `--force` pull コマンドで使用します。parameters.yml ファイルを上書きし、追加されたキーを保持します。
