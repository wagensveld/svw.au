---
title: "TF Infrastructure"
description: "The first project listed in the new projects section! It's a simple Terraform repo to manage the infrastructure for this site as well as other projects."
summary: "The first project listed in the new projects section! It's a simple Terraform repo to manage the infrastructure for this site as well as other projects."
date: "2023-09-28T16:25:29"
images:
  - "images/terraform-cloud.webp"
category: "projects"
---

[TF Infrastructure](https://gitlab.com/wagensveld/tf-infrastructure) is a basic Terraform repo which sets up most of the infrastructure involved in this website, as well as a few other services.

If it were a more collaborative project I would probably split it up into multiple repositories. But it's just me making very occasional changes, so monorepo it is!

It's fairly specific to the services I use, it probably won't be directly useful to anyone else. You may not use mailbox.org for email, but the DNS entries required to set up email is fairly consistent across providers. Hopefully it can provide some ideas for a problem you're trying to solve.

## Services

This repo lets you manage the following:

- A Cloudflare zone
- DNS for mailbox.org configured on Cloudflare
- AWS billing alerts
- AWS S3 bucket for a static site (undoes [this](/blog/cloudflare-pages))
- AWS S3 bucket for a static site with redirections

## Set Up

At the moment the Terraform state, workspaces, variables, and execution are managed by Terraform Cloud. My over all experience with Terraform Cloud is good, my only gripe is with importing resources. TF Cloud doesn't support it, instead it has to be done locally. It's fine occasionally but gets tedious quickly.

### Workspaces, Variables, and Projects

While this is a monorepo I don't want to redeploy all of my infrastructure at once. That's where workspaces come in handy. Set up a new workspace using version control, and specify the Terraform working directory. On top of that you can create multiple workspaces pointing to the same working directory but with different variables specified to deploy very similar but distinct applications.

It gets a bit messy though, so it helps to split these workspaces into different projects.

### Credentials

To apply Terraform code we need to grant access to wherever we're deploying to.

#### AWS

I followed [this blog post on AWS about setting up OIDC for Terraform Cloud](https://aws.amazon.com/blogs/apn/simplify-and-secure-terraform-workflows-on-aws-with-dynamic-provider-credentials/).

#### Cloudflare

Unfortunately Cloudflare doesn't have such an elegant way of providing access. Creating an API with the appropriate access, and assigning it to the `CLOUDFLARE_API_TOKEN` environment variable within a variable set or individual workspace works well enough.

## Folder Overview

### aws

#### alerts

Creates:

- A Cloudwatch metric alarm for when `AWS/Billing` meets a certain threshold.
- An SNS topic and subscription for that metric.

#### S3

Creates a static site on S3 with corresponding DNS on Cloudflare.

#### S3-redirect

Creates a static site for redirection on S3 with corresponding DNS on Cloudflare.

### Cloudflare

#### Email

Sets up the DNS required for [mailbox.org](https://mailbox.org/en).

#### Zone

Creates a zone in Cloudflare.

### Modules

#### S3-policy

Creates the policies required for publicly accessible S3 static sites.

Originally I wanted to create some sort of S3 site module and use `null` and `count` to switch between resource creation and attributes.

It ended up messy and I didn't want to think about making modifications to it in the future. So instead we have both _s3_ and _s3-redirect_ as separate folders with shared definitions for their policies.
