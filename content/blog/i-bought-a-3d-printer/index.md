---
title: "I bought a 3d Printer"
description: "Day one and I'm feeling pretty stoked."
summary: "Day one and I'm feeling pretty stoked."
date: "2022-04-20T12:23:45"
images:
  - "images/3dprint.webp"
category: "blog"
---

3d printing was always in the category of hobbies, I want to try but couldn't justify the cost and effort on a whim, especially because in reality - there were only two or three things I want to print.

I shelved the idea for years.

## Tipping Point

There isn't exactly a single tipping point, but rather a collection of annoyances and opportunities which would require 3d printing. For the most part it was really minor stuff - my headphones were missing a clip and no one sells replacement parts.

Afterwards came things I wish I had a 3d printer for because I couldn't justify the price of buying parts.

I really wanted to do the [Gaggia Classic Pro PID mod](https://heald.ca/adding-a-diy-pid-to-the-gaggia-classic-pro/) but not having a case was a deal breaker, and ready made kits were way out of my budget.

I was curious about the [Tractyl Manuform](https://github.com/noahprince22/tractyl-manuform-keyboard), but couldn't make one, or justify the cost of commissioning someone to make one for me.

A friend who is very into Open Source Hardware then introduced me to [Ploopy](https://ploopy.co/). Ploopy makes open source pointing devices (mostly trackballs, but they also have a mouse). I found the idea of buying a mouse but then having the official model to create replacement parts incredibly appealing - this was the first time I had really considered buying a 3d printer. (Funny enough, I still haven't gotten the mouse I've ordered, but it should be here in the next couple of days.)

Around the same time I found out [someone made that missing clip for my headphones on thingiverse](https://www.thingiverse.com/thing:2855971).

Thingiverse is a website where creators upload their 3d models, you can download, and print them yourself. Almost everything is free, occasionally you have the option to tip the seller as well.

I was browsing the things on the site, and came across something something oddly personal. A replica of the Dutch lighthouse [Lange Jaap](https://www.thingiverse.com/thing:3910645). Lange Jaap is a lighthouse in Den Helder, Netherlands; it stands at 63.5 metres making it one of the tallest lighthouses in the world. It also reminds me of my grandparents and my dad who lived there. Simply put I thought it would be a nice gift for my dad.

This had me excited about 3d printing, perhaps not enough to purchase a machine myself, but enough to be invested.

## Thingiverse

Lange Jaap convinced me to try 3d printing, Thingiverse convinced me to get a 3d printer.

Thingiverse scratches the same itch Aliexpress does, low cost solutions to everyday problems. I got quite excited about printing my own pots, coin sorters, and headphone stands. And learning how to design them myself.

I started to change the way I think about hobbies. Much like 3d printing itself, there are lots of things I want to try [stenography](https://www.thingiverse.com/thing:3142989) for example, like 3d printing, the initial investment seems like too much for what will likely end up being something I don't commit to. 3d printing changed the way I think about this. If I wanted to try stenography I could make my own, the only cost is some filament, some electrical components, and my time.

All while this is happening, I am fortunate enough to be surrounded by people who are getting excited for me about this.

I was convinced.

## Which printer to get

While convinced, I wasn't committed. I needed something cheap, fortunately I saw an ebay auction for a fairly unused Ender 3 on eBay. I place my bid and lose. It ends up selling for about AU$210. Next thing I see is a brand new Voxelab Aquila - an Ender 3 clone on sale for AU$234, eBay at the time had a blanket 5% off code, and they then decide to send me a gift card worth AU$30. All for a total for AU$192.

## Voxelab Aquila

As of writing this I have the Aquila, assembly was pretty easy, though I did get some screws mixed up. Unfortunately I stripped some threads, they're in non critical parts of the printer integrity wise (the PSU and one of the X axis belt adjustment).

Speaking of stripping, I messed up the height adjustment and scratched the surface of the hotbed.

![Scratch on the hotbed](scratch.webp)

Pretty devastated but it still prints fine.

I was going to print a [tool holder](https://www.thingiverse.com/thing:4723526) for it, but something has gone wrong.

![Tool holder failed print](error.webp)

I'm almost out of demo filament, when my spindles arrive I'll retry it, and finally print Lange Jaap.
