---
title: "Ways to write more"
description: "I find writing difficult."
summary: "I find writing difficult."
date: "2022-04-06T17:43:29"
category: "blog"
---

That is to say I find expressing thought difficult.

Lately I have been in a self reflective mood; and I've identified some of the concerns I have:

- I find expressing myself difficult.
- I find keeping organised and productive a challenge.
- I find I can't disconnect, appreciate a moment for what it is.
- I find my self-reflection often shallow.

## It's Not a Writing Problem, It's a Writing Solution

Simplifying the issues I see two skills I want to improve:

- Ability to formulate thought.
- Ability to express thought.

Neither of which are specific to writing; however I believe I can exercise those skills if I force myself to write more often.

With that I'm devising ways to incorporate writing more in my day to day life, I'll update this page as I try new things.

## Current Solutions

### Bullet Journal / Planner

> [Main page here](/blog/create-a-planner)

Every morning I write the tasks I want to accomplish that day, in the evening I review my progress.

Upsides:

- Starts my day with productivity in mind.
- Forces myself to reflect on my goals.
- Is a doable daily activity.
- Addresses the 'formulate thought' concern.

Downsides:

- Writing is limited to short descriptions.
- Doesn't address 'express thought' concern.

### Write About Your Surroundings

When free take the time to write a paragraph or so about your surrounding environment.

Upsides:

- Forces me to consider my surroundings.
- Shows improvement in writing for similar environments.
- Multiple approaches to description.
- Is a doable daily activity.
- Addresses the 'formulate thought' concern.
- Addresses 'express thought' concern.

Downsides:

- At home writing will get boring quickly.
- Outside writing is inconvenient.
