---
title: "IBM Model F XT Reassembly Notes"
description: "Not a tutorial just notes."
summary: "Not a tutorial just notes."
date: "2022-05-27T16:09:25"
category: "blog"
---

My [model f keyboard](/blog/setting-up-a-model-f) started acting up months back, very occasionally it would repeat characters at random. One day it properly flipped out.

It was time to give it a proper clean and restoration, so I ordered foam online, which got lost in the mail, but eventually became un-lost.

I took it apart gave it a good clean, but then came the part I was dreading, reassembly.

It's tough, tougher than I thought it would be.

Here's some notes/tips:

- Order clamps - I ended up having to use 12.
- Make sure the case pins properly align - I scratched my PCB by mistake (it's fine though).
- If you snap the flippers feet, you can super glue it back together, but make sure the bottom is still smooth - I broke the spacebar flipper twice, as a result my F10 key doesn't work, but the other one I glued together (F9) is fine.
- Make note of how the spacebar stabiliser is connected to the barrel - I forgot to and it took me an embarrassingly long time to figure out it had to click in place.
- The floss trick can get trapped, I had better luck sliding the backplate onto the flippers - but I also broke two flippers so take that with a grain of salt.
