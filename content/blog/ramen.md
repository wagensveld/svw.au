---
title: "Ramen Masterpost"
description: "Quite a lot goes into making ramen, while a lot of the components can be store bought, actually making ramen from scratch requires a lot of time, work and patience. For a lot of the parts it doesn't make sense to have a single recipe post, rather I've broken it down into multiple, hopefully it's easier to decide which toppings or soup base to use while cooking when you have the options presented side by side."
summary: "Quite a lot goes into making ramen, while a lot of the components can be store bought, actually making ramen from scratch requires a lot of time, work and patience. For a lot of the parts it doesn't make sense to have a single recipe post, rather I've broken it down into multiple, hopefully it's easier to decide which toppings or soup base to use while cooking when you have the options presented side by side."
date: "2022-10-08T11:05:26"
series:
  - "ramen"
aliases:
  - "/ramen"
  - "/rmn"
category: "food"
toc: true
---

A lot of the recipes and work is heavily inspired by [The Ramen_Lord Book of Ramen by Michael T. Satinover and Scott J. Satinover](https://docs.google.com/document/d/1qLPoLxek3WLQJDtU6i3300_0nNioqeYXi7vESrtNvjQ/) a fantastic resource on ramen, and far more in depth than I ever plan on going here.

This post is categorised into the various components which make up ramen, in roughly the order in which to make them. I recommend skimming through everything you want to cook so you can see what can be doubled up; as well as any prerequisites you may want to make - such as [dashi](#dashi) or [kansui](/recipe/misc/kansui-powder).

## Alkaline Noodles

> The noodles need to rest at least 24 hours before serving.

One of the unique characteristics of ramen is it uses something called kansui. Usually made up of sodium carbonate or potassium carbonate with a pH of 11. Kansui affects the gluten in the wheat, making it tougher, harder, and slightly yellow.

Creating noodles at home with a pasta maker can be a fun experience, but it's also a lot of work with a long lead time. I often find myself buying pre-made noodles from the store.

- [Standard Ramen Noodles](/recipe/noodles/standard-ramen-noodles)
  <!-- - TBD [Hakata Style Noodles](/recipe/noodles/hakata-style-noodles) TBD TN JA -->
  <!-- - TBD [Kitakata Style Noodles](/recipe/noodles/kitakata-style-noodles) TBD TN JA -->
  <!-- - TBD [Tsukemen Noodles](/recipe/noodles/tsukemen-noodles) TBD TN JA -->

## Dashi

> Dashi is actually quite quick to make, only about 30 minutes or so for both _Ichiban_ and _Niban_ dashi; but it is a prerequisite for the stock and tare later on.

Dashi isn't actually one of the components of ramen, rather it is often used as an ingredient for two of ramen's components: stock and tare.

On that, dashi is a fundamental ingredient in Japanese cooking, it's a type of soup stock often made of konbu (a type of seaweed), and bonito flakes; however other variations using shiitake mushrooms, sardines, and other ingredients. Quite a lot of Japanese food has an umami, seafood flavour; this comes from dashi.

Provided you can find the ingredients I think it's worth making your own dashi - it's quick, and simple. However there options available at your local asian grocery work fine as well.

- [Basic Dashi](/recipe/stock/basic-dashi)
<!-- - TBD [Niboshi Dashi](/recipe/stock/niboshi-dashi) TBD TN JA
- TBD [Clam Dashi](/recipe/stock/clam-dashi) TBD TN JA -->

## Stock

> How far in advance to make the broth depends on the type. You can safely do a chintan on the same day as serving. While a paitan probably requires you to start one or two days in advance.

Stock is the body, it holds everything together. While flavour is definitely an important part of the stock. It could be argued texture is a more important part of stock.

The good news is if you already have a stock you really like, then its probably a great option for ramen. The bad news is barring premium stock, the options at the groceries are pretty ordinary. I recommend making your own stock.

Most of these recipes have (niban) [dashi](#dashi) as an optional ingredient. In general I prefer adding it to all of my stocks when it comes to making ramen.

### Chintan

> Most chintan stocks take about 5 or so hours to make.

Chintan (清湯) is a loan word from Chinese, literally meaning clear soup. Like the name suggests it's fairly light and quite versatile even outside of ramen.

- [Basic Chicken Chintan](/recipe/stock/basic-chicken-chintan)
<!-- - TBD [Chicken and Pork Chintan](/recipe/soup/chicken-pork-chintan) TBD TN JA : Look at “Doubutsu Kei” Style Chintan in ramen book, seems to be the style commonly used in hakodate  -->

<!-- ### Paitan
> A lot of paitan stocks can take about 20 hours of simmering to make.

- TBD [Tonkotsu Paitan](/recipe/soup/tonkotsu-paitan) TBD TN JA
- TBD [Chicken Paitan](/recipe/soup/chicken-paitan) TBD TN JA
- TBD [Tsukemen Paitan](/recipe/soup/tsukemen-paitan) TBD TN JA -->

## Tare

> Takes at least 6 hours to make (most is resting time), but can also be done days in advance.

When first learning to make ramen a lot of the time I would make something that tasted fine, but it didn't feel like ramen.

It was tare.

If stock is the body, tare is the soul. Tare is a concentrated stock that goes into the soup, different tares have different flavours and mouth feel, a shio tare will feel quite light, while a miso tare will be a lot heavier.

I know it's fantastic writing to have the tip for what I think is the best way to experiment with ramen right in the middle of the post, in probably not the most relevant section.

But.

Probably the best way to experiment with different ramen flavours is to make a [basic chicken chintan](/recipe/stock/basic-chicken-chintan), store bought noodles, a simple oil like the [chicken oil](/recipe/oil/chicken-oil), and an [ajitsuke tamago](/recipe/toppings/ajitsuke-tamago) (of course). Then using that as a baseline experiment with different tares.

With it you can turn a chicken soup into a fishy soup, or one with a strong emphasis on shiitake. The world is your oyster (I'm sure its possible to make a lovely tare with oysters as well).

The store bought question, I don't think I have ever seen tare for sale. But it's easy and the most fun about making ramen (imo).

- [Minimal Shio Tare](/recipe/tare/minimal-shio-tare)
- [Shio Tare](/recipe/tare/shio-tare)
- [Shoyu Tare](/recipe/tare/shoyu-tare)
<!-- - TBD [Kitakata Shoyu Tare](/recipe/tare/kitakata-shoyu-tare) TBD TN JA
- TBD [Toasted Shoyu Tare](/recipe/tare/toasted-shoyu-tare) TBD TN JA
- TBD [Miso Tare](/recipe/tare/miso-tare) TBD TN JA
- TBD [Kara Miso (Spicy Miso)](/recipe/tare/kara-miso) TBD TN JA -->

## Aroma Oil

> Takes maybe 15 minutes to make. I'm including it up here so it doesn't get lost in the sea of toppings.

Needlessly continuing with my body analogy: it's the perfume. Aroma oil is fairly subtle, it creates an inviting smell, improves mouthfeel, helps retain heat, and adds subtle flavours.

Buying it in store? There are some cases where I would advocate for buying in store. If you end up with all the ingredients for a bowl of ramen left over (aside from the oil). I would opt for something like lard to make a quick meal.

A lot of the time the aroma oil you make depends on the ingredients you have left over.

Making a tonkotsu ramen? Take a little bit of the fat and make lard. Making a chicken ramen with spring onion? Take the fat and the white ends and make an oil that way.

- [Chicken Oil](/recipe/oil/chicken-oil)
<!--
- TBD [Chicken and Spring Onion Oil](/recipe/chicken-spring-onion-oil) TBD TN JA
- TBD [Spring Onion Oil](/recipe/spring-onion-oil) TBD TN JA -->
- [Spring Onion and Niboshi Oil](/recipe/oil/spring-onion-niboshi-oil)
<!-- - TBD [Ginger and Onion Lard](/recipe/ginger-onion-lard) TBD TN JA -->
- [Chilli Oil](/recipe/oil/chilli-oil): Not a very traditional aroma oil, but especially if served fresh can be interesting.
- Lard, Chicken Fat, Duck Fat: Some potential store bought options.

## Toppings

> Pretty much all of these are quick and can be done on the day. Something like the [ajitsuke tamago](/recipe/toppings/ajitsuke-tamago) should be done early on in the day because it requires to be steeped for 6 hours.

- [Ajitsuke Tamago (Ramen Egg)](/recipe/toppings/ajitsuke-tamago): The classic. It's cheap and easy to make. Include it with your ramen. Have it with any ramen.
- Kamaboko (Steamed Fish Cake): It's made from white fish. Generally I would include it in either chintan or ramen with a lot of seafood flavours. <!-- TBD Make recipe -->
- [Chashu Pork](/recipe/toppings/chashu-pork)
<!-- - TBD [Kakuni Pork](/recipe/kakuni-pork) TBD TN JA -->
- [Chashu Chicken](/recipe/toppings/chashu-chicken)
- [Teriyaki Chicken](/recipe/toppings/teriyaki-chicken)
  <!-- - TBD [Karaage Chicken](/recipe/karaage-chicken) TBD TN JA -->
  <!-- - TBD [Wontons](/recipe/wontons) TBD TN JA: If you're making your own noodles use some of the dough from that to make the skins. -->
- Nori (Dried Seaweed): Cut up the seaweed you would use in a sushi roll and add it to the corner of your soup. Works with any ramen.
- Menma (Fermented Bamboo Shoots): Fairly subtle sour flavour, with a nice crunch. Have it with any ramen.
- Bamboo Shoots: I found it quite difficult to find menma, as a replacement I took some frozen bamboo shoots and seasoned it in the same mixture as [ajitsuke tamago](/recipe/toppings/ajitsuke-tamago) for about 4 hours. It's definitely not a replacement but nice nonetheless.
- Spring Onion: Adds a little bit of freshness. Works with any ramen.
  - Sliced at a slight angle.
  - Sliced across into rings.
- Bok Choy.
  <!-- - TBD [Pickled Ginger](/recipe/pickled-ginger) TBD TN JA -->
  <!-- - TBD [Spicy Miso Ball](/recipe/spicy-miso-ball) TBD TN JA -->
- Cooked Corn: Adds a little bit of sweetness. Generally I would add it to something a bit more savoury such as miso or tonkotsu ramen.
- Butter: Adds a little bit of creaminess. I usually add it to a miso ramen.
- [Kikurage](/recipe/toppings/kikurage): Also known as black fungus or wood ear mushroom. Fairly standard addition to any ramen. <!-- TBD Add seasoning  -->
- Bean Sprouts: Adds freshness and crunch. Generally add it to paitan soups.
- Shiitake Mushrooms: Not a traditional topping, but can work especially in a chintan soup. <!-- TBD Add seasoning  -->

### Condiments

With the exception of gyofun which should go with tsukemen. These are some things you can add to ramen after serving it to spice it up.

<!-- - TBD [Gyofun](/recipe/toppings/gyofun) TBD TN JA: A fish based umami powder https://www.reddit.com/r/ramen/comments/qtjv4a/gyofun_recipe/ -->

- Black Pepper
- Minced/Dried Garlic
- Chilli Flakes
- [Chilli Oil](/recipe/oil/chilli-oil)
- More [Tare](#tare)
- Vinegar

## Serving

For the classic ramen look and flavour, add to the bowl in the following order:

1. Tare
2. Aroma Oil
3. Stock
4. Noodles
5. Toppings

### Storing

- Noodles: Will last in the fridge for a few weeks, and a few months in the freezer.
- Dashi: I wouldn't recommend making too much dashi in advance, it will last about 2 weeks in the fridge before it goes sour. As it ages a lot of its delicate flavours break down. I haven't tried freezing it. But I would imagine it's similar to stock, about 4 months or so.
- Stock: In the fridge 4 days, in the freezer 4 months. But again a lot of the delicate flavours start to break down. One way yo cool it down for freezer or fridge storage is to move the pot into an ice bath to rapidly chill.
- Tare: Due to tares high salt content, the tare will last a long time. 6 months in the fridge.
- Aroma Oils: Suffers from the same issues dashi and stock does, where a lot of its fragrance won't store well. However flavour wise aroma oils can keep in the fridge for about 6 months.

### Recipes

Congratulations for making it to the end of this post, the world is truly your oyster; but in case you're in need of some pearls: Here a few recipes which I think are representative of the different types of ramen.

#### Working Man's Ramen

This is a super basic ramen recipe, it'll make a serviceable ramen. The point isn't to be prescriptive, but more if you have an ingredient you want to use up but don't want to go through the process of cooking everything else, try this. Likewise these are just ingredients that I happen to have in the kitchen usually, they can be subbed out for many others.

- Noodles: Store bought noodles.
- Stock: Store bought stock with a little bit of ginger and garlic to taste.
- Tare: Soy sauce.
- Aroma Oil: Sesame oil.
- Toppings: Cooked meat.

#### Chintan

<!-- ##### Hakodate Shio
This is my take on my fondest ramen. Like most things you consider the best, this is completely dependent on your emotional state at the time. In this case around Christmas 2019 I got lost in Hakodate, just as the snow and wind started to pick up, and was also pretty hungry. It was very dark but I could tell I was pretty close to the sea cause the waves were hitting the seawall pretty hard. Then I saw it. A brightly lit ramen restaurant. It was also my first shio ramen - I'm sure that plays into it as well.

- Noodles: [Standard Ramen Noodles](/recipe/noodles/standard-ramen-noodles)
- Stock: [Chicken and Pork Chintan](/recipe/soup/chicken-pork-chintan) using a [basic dashi](/recipe/stock/basic-dashi) TBD
- Tare: [Shio Tare](/recipe/shio-tare)
- Aroma Oil: [Chicken Oil](/recipe/oil/chicken-oil) TBD
- Toppings: [Ajitsuke Tamago (Ramen Egg)](/recipe/toppings/ajitsuke-tamago), [Chashu Pork](/recipe/chashu-pork), Nori, Menma, Spring Onion (both cut into strips and rings) -->

##### Tokyo Shoyu

- Noodles: [Standard Ramen Noodles](/recipe/noodles/standard-ramen-noodles)
- Stock: [Basic Chicken Chintan](/recipe/stock/basic-chicken-chintan) using a [basic dashi](/recipe/stock/basic-dashi)
- Tare: [Shoyu Tare](/recipe/tare/shoyu-tare)
- Aroma Oil: [Spring Onion and Niboshi Oil](/recipe/oil/spring-onion-niboshi-oil)
- Toppings: [Ajitsuke Tamago (Ramen Egg)](/recipe/toppings/ajitsuke-tamago), [Chashu Pork](/recipe/toppings/chashu-pork), Nori, Menma, Kamaboko

<!-- ##### Kitakata Shoyu
- Noodles: [Kitakata Style Noodles](/recipe/noodles/kitakata-style-noodles)
- Stock: [Chicken and Pork Chintan](/recipe/soup/chicken-pork-chintan) using a [niboshi dashi](/recipe/stock/niboshi-dashi)
- Tare: [Kitakata Shoyu Tare](/recipe/kitakata-shoyu-tare)
- Aroma Oil: [Spring Onion and Niboshi Oil](/recipe/spring-onion-niboshi-oil)
- Toppings: [Ajitsuke Tamago (Ramen Egg)](/recipe/toppings/ajitsuke-tamago), [Kakuni Pork](/recipe/kakuni-pork), Menma, Spring Onion (both cut into strips and rings) -->

<!-- ##### Asahikawa Shoyu
- Noodles: [Standard Ramen Noodles](/recipe/noodles/standard-ramen-noodles)
- Stock: [Chicken and Pork Chintan](/recipe/soup/chicken-pork-chintan) using a [clam dashi](/recipe/stock/clam-dashi)
- Tare: [Shoyu Tare](/recipe/shoyu-tare)
- Aroma Oil: [Spring Onion and Niboshi Oil](/recipe/spring-onion-niboshi-oil)
- Toppings: [Chashu Pork](/recipe/chashu-pork), Menma, Nori, Spring Onion (both cut into strips and rings) -->

<!-- ##### Sapporo Miso
- Noodles: [Standard Ramen Noodles](/recipe/noodles/standard-ramen-noodles)
- Stock: [Chicken and Pork Chintan](/recipe/soup/chicken-pork-chintan) using a [niboshi dashi](/recipe/stock/niboshi-dashi)
- Tare: [Miso Tare](/recipe/miso-tare)
- Aroma Oil: [Ginger and Onion Lard](/recipe/ginger-onion-lard)
- Toppings: [Ajitsuke Tamago (Ramen Egg)](/recipe/toppings/ajitsuke-tamago), [Chashu Pork](/recipe/chashu-pork), Bean Sprouts, Nori, Corn, Butter, Spring Onion (both cut into strips and rings) -->

<!-- ##### Curry
Look at: https://www.reddit.com/r/ramen/comments/2n6bd8/next_up_on_my_tour_of_ramen_styles_homemade_soup/
- Noodles: [Kitakata Style Noodles](/recipe/noodles/kitakata-style-noodles)
- Stock: [Chicken Feet Chintan](/recipe/chicken-feet-chintan) using a [basic dashi](/recipe/stock/basic-dashi)
- Tare: [Japanese Curry Paste](/recipe/japanese-curry-paste)
- Toppings: [Ajitsuke Tamago (Ramen Egg)](/recipe/toppings/ajitsuke-tamago), [Chashu Pork](/recipe/chashu-pork), Spring Onion (both cut into strips and rings) -->

<!-- #### Paitan -->

<!-- ##### Kurume Tonkotsu
- Noodles: [Hakata Style Noodles](/recipe/noodles/hakata-style-noodles)
- Stock: [Tonkotsu Paitan](/recipe/soup/tonkotsu-paitan) using a [basic dashi](/recipe/stock/basic-dashi)
- Tare: [Shio Tare](/recipe/shio-tare)
- Aroma Oil: [Spring Onion Oil](/recipe/spring-onion-oil)
- Toppings: [Ajitsuke Tamago (Ramen Egg)](/recipe/toppings/ajitsuke-tamago), [Chashu Pork](/recipe/chashu-pork), Kikurage, Spring Onion (cut into rings), Nori -->

<!-- ##### Osaka Tori Paitan
- Noodles: [Hakata Style Noodles](/recipe/noodles/hakata-style-noodles)
- Stock: [Chicken Paitan](/recipe/soup/chicken-paitan) using a [basic dashi](/recipe/stock/basic-dashi)
- Tare: [Shoyu Tare](/recipe/shoyu-tare)
- Aroma Oil: [Chicken and Spring Onion Oil](/recipe/chicken-spring-onion-oil)
- Toppings: [Ajitsuke Tamago (Ramen Egg)](/recipe/toppings/ajitsuke-tamago), [Chashu Chicken](/recipe/chashu-chicken), Kikurage, Spring Onion (cut into rings), bean sprouts -->

<!-- ##### Tonkotsu Gyokai Tsukemen
- Noodles: [Tsukemen Noodles](/recipe/noodles/tsukemen-noodles)
- Stock: [Tsukemen Paitan](/recipe/soup/tsukemen-paitan) using a [niboshi dashi](/recipe/stock/niboshi-dashi)
- Tare: [Toasted Shoyu Tare](/recipe/toasted-shoyu-tare)
- Toppings: [Ajitsuke Tamago (Ramen Egg)](/recipe/toppings/ajitsuke-tamago), [Chashu Pork](/recipe/chashu-pork), Spring Onion (cut into rings), [Gyofun](/recipe/gyofun), Black Pepper -->
