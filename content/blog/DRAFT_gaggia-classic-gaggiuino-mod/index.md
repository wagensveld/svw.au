---
title: "Gaggia Classic Gaggiuino Mod with Water Tank LEDs"
description: "Part 3 in the Gaggia Classic trilogy which undoes everything I did in the previous two parts; but this time I think it's the last mod."
summary: "Part 3 in the Gaggia Classic trilogy which undoes everything I did in the previous two parts; but this time I think it's the last mod."
date: "2023-02-22T11:44:33"
# images:
#  - "images/gaggiuino.webp"
draft: true
category: "drafts"
---

Sort of.

[Gaggiuino](https://gaggiuino.github.io) is a really cool mod for the Gaggia Classic, Gaggia Classic Pro, and some community members have gotten it to work on other coffee machines.

The two mods which are considered essential are adding a [pid controller](/blog/gaggia-classic-pid-mod) and replacing the 14 bar OPV with 9 bar.

The Gaggiuino does that a lot more. Some of its features include:

- Auto shot timer
- Graphing
- Pre-infusion
- Integrated scales

There's quite a bit planned in the future. Making this probably the last mod I'll do for this machine, but it'll be an ongoing process.

The documentation and the discord server are really good, but like any project there were hurdles I bumped into, so hopefully this post can help out.

## Before ordering the parts

First thing's first, decide if you want to do it. Like I said the documentation and the discord server is really good, but this still requires a degree of problem solving.

For the most part this mod is reversible with the exception of the pressure sensor mod which requires you to splice a hose.

It's worth going through the website and the server, see what you want to integrate into your build. The server has a channel dedicated to mods where I found out about [this pogo pin enclosure](https://www.printables.com/model/337966-pogo-pin-enclosure-for-gaggiuino) just as my final parts were arriving.

I'll install it another time.

## Ordering parts

Most of the parts required are listed on the website, but some for extended functionality may be missing. It's worth going to the Printables page and checking the parts list there.

A couple of examples are the link for the snubber in the lego housing mod, and a few components required for the scales mod.

## Other parts

Other parts which aren't listed but are useful are:

- Heatshrink tubes
- Misc crimp terminals
- Cable ties
- Thermal paste
- Personal Lubricant (for the pressure sensor functionality)

## Pre install

The main thing I had to do prior to installing was replace my 9 bar spring with the stock 14.

As I did the installation I slowly removed the modifications from the PID kit.

This doesn't really count as a pre-step, but I think it makes sense to put it here.

Flashing the STM32 and display: If you're planning on installing the scales as well it makes sense to set up the scales first. The scales require you to flash both the display and the board for calibration, then again for the actual Gaggiuino software.

## Installing

I hope to align these steps with the website, but as the Gaggiuino updates, I'm sure these will drift.

### 2.2.2 Pressure Transducer

This is where that lubricant comes in handy.

1. Place the metal barbed connectors in the freezer until nice and cold.
2. Cut the Saeco hose to size about 15cm in my case.
3. Run boiling water on the hose.
4. Apply lubricant to the connectors and the hose. The combination of the temperature and the lubricant will make it a lot easier to push the hose flush.

### Water Tank LED Lights

If you've reached this point you probably know how to install the LED lights.

Buy some [5V LEDs](https://www.aliexpress.com/item/4000681373431.html) (it's helpful if they're waterproof).

Attach them to the 5V stripboard in the Gaggiuino casing.

You ran run the wires through the same hole as the water funnel. There's another hole near the centre which may work, however I imagine it being quite difficult to remove the LEDs if you need to replace them using that hole.

Speaking of which it's a good idea to use some sort of connector between the LED strip and the Gaggiuino stripboard. so you can manoeuvre the Gaggiuino freely for upgrades without having to remove the LEDs.

Lastly its likely the heat of the machine will melt the LED's glue. Try to find some nontoxic heat resistant adhesive to keep it in place.
