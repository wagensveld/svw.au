---
title: "Moving to Cloudflare Pages"
description: "By the time this post is up the migration will be done."
summary: "By the time this post is up the migration will be done."
date: "2022-04-09T23:05:47"
images:
  -  "images/w3m-cf-svanw.webp"
category: "blog"
---

However at the time of writing this, this blog is hosted on [Vercel](https://vercel.com/). And my over all impression is: it's pretty great. I would highly recommend it to anyone who wants to host their own website.

It provided all the features I want:

- git integration
- preview builds
- redirects
- Brotli compression compatibility
- it being free

There was just one incredibly small _slight_ annoyance that no one else would be notice but me.

It's the way [w3m](http://w3m.sourceforge.net/) handles connecting to the site.

## W3M

> Fun fact: w3m stands for WWW を見る (WWW wo miru), which means "see the world wide web" in Japanese

w3m is an open source text based web browser for Unix like operating systems. It doesn't support css or javascript. A lot of websites don't work properly due to these limitations. But often simple blogs and the likes of hacker news work well enough to surf cyberspace distraction free.

![science.org failing to load in w3m due to lack of javascript support.](w3m-science.webp)

This is my website via `w3m https://svanw.com`

![https://svanw.com successfully displaying in w3m.](w3m-svanw.webp)

Protocols are a hassle to write down, what if I were to do `w3m svanw.com`?

```
Redirecting (308)

https://svanw.com/
```

This is where the problem lies. w3m will not redirect to `https://svanw.com/`.

## It's Always DNS...

...options. Vercel (and some similar alternatives like Netlify) require root level domains to be pointed as an A record to their IP address, routing is done, and you end up on your website. Modern web browsers handle this fine, even other text based browsers such as links handle this well. w3m is the exception.

## Cloudflare Pages

I was playing around on Cloudflare pages - just to see how alternatives to Vercel stack up. It ticked all the boxes:

- git integration
- preview builds
- redirects
- brotli compression compatibility
- it being free

So then I try w3m:

![w3m successfully loads cloudflare.svanw.com](w3m-cf-svanw.webp)

That's a go ahead!

### Migrating

#### Builds

The first issue I had was before that preview website was even up. It seems to be fairly common - the Docker image defaults to a very old version of Hugo.

```
21:59:55.357	Installing Hugo 0.54.0
```

I had the same problem with my site on Vercel. It's easy enough to override, under `Settings/Environment variables` create `HUGO_VERSION` set to your version of hugo (e.g. `0.96.0`).

#### Redirects

A difference between Vercel and Cloudflare is their solution for redirects and headers. Vercel as a `vercel.json` which it uses outside of just builds (it's where I specified the previously mentioned `HUGO_VERSION` variable). It lives in the root directory of your project.

On the flip side environment variables are defined through the GUI; redirects and headers are each their own respective files (`_redirects` and `_headers`), and need to be in the websites root after build. In Hugo's case this means placing it in the `static` folder.

Cloudflare's redirects are not as mature as Vercel's. I previously had the following rule.

```json
{
  "source": "/photos(/?$)",
  "destination": "https://photos.$host"
}
```

It has become:

```
/photos https://photos.svanw.com
```

The second redirect misses out on the following:

- matching with and without a trailing `/`
- the ability to take the current websites domain, and use that to construct a subdomain.

#### Headers

The `_headers` file is pretty straight forward.

```json
{
  "source": "/(.*)",
  "headers": [
    {
      "key": "referrer-policy",
      "value": "no-referrer"
    }
  ]
}
```

Becomes:

```
/*
  Referrer-Policy: no-referrer
```

#### Domain Redirects

One thing I completely took for granted in Vercel are domain redirects. Going to `www.svanw.com` redirects to `svanw.com`.

This is still possible in Cloudflare but may be what causes headaches in the future. It can be done via Page Rules - however you're limited to 3 free page rules per site.

The first step is to create an `AAAA` record in Cloudflare:

- Type: AAAA
- Name: www
- IPv6 Address: 100::
- Proxy Status: Proxied

![Cloudflare DNS Entry](svanw-aaaa.webp)

The second step is to create a page rule with the following settings:

- URL: www.svanw.com/*
- Pick a Setting: Forwarding URL
- Select status code: 301 - Permanent Redirect
- Enter destination URL: https://svanw.com/$1

And the best part is w3m approves.

![w3m successfully loading www.svanw.com](wwwsvanw.webp)

> Edit 2022-09-15

I've noticed a quirk with cloudflare pages. `:git` for date information doesn't seem to work. It marks all pages as last modified with the most recent commit time.
