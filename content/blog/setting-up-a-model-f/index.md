---
title: "Setting up an IBM Model F XT in 2022"
description: "The Soarer's converter does 99% of the work."
summary: "The Soarer's converter does 99% of the work."
date: "2022-02-19T14:43:23"
images:
  - "images/modelfxt.webp"
category: "blog"
---

During university I had an IBM Model M keyboard, but was always curious about its predecessor the Model F.

Recently I won an eBay auction on a Model F XT which was in pretty good condition. These are the things I did to set it up.

## Soarer's Converter

The keyboard uses the ancient XT protocol which no modern computer natively supports, let alone has the 5 pin DIN connector to plug into. [Fortunately in 2011 Soarer on Geekhack shared his converter with the community](https://geekhack.org/index.php?topic=17458.0). It's possible to create one at home, and there are [beginner friendly tutorials](https://deskthority.net/viewtopic.php?t=19035). But I opted to [buy a pre-made one from tinkerBOY](https://www.tinkerboy.xyz/product/tinkerboy-xt-at-usb-converter-with-soarers-converter-firmware/). This ends up doing 99% of what I wanted the board to do.

## Print Screen

The one issue I had was the usage of the `PrtSc` button. The Model F not having a modern layout has the `PrtSc` button to the left of the numpad, and it's combined with the asterisk key.

![Close up of IBM Model F XT PrtSc button. With PrtSc above asterisk](prtsc.webp)

The asterisk was working perfectly, however `PrtSc` wouldn't send. A quick search showed Soarer's converter doesn't handle this use case by default. Fortunately Soarer's converter does allow for rewrite and macros (more on that soon).

The question was how to map it? I didn't want to lose functionality of the asterisk, and by the looks of the legend it appears as if `PrtSc` was under a `Shift` layer.

I referred to the [IBM Personal Computer XT Technical Reference](https://www.reenigne.org/crtc/PC-XT.pdf) for guidance, and lo and behold it is under a shift layer.

![Print Screen: The combination of the Shift and PrtSc (key 55) keys will result in an interrupt invoking the print screen routine. This routine works in the alphanumeric or graphics mode, with unrecognisable characters printing as blanks.](print-screen-definition.webp)

## Other Keys

This made me curious about some of the other key combinations. I took a further look into the manual.

![First page with table of character codes, notable lines to be discussed below](character-codes-1.webp)

Almost everything seems standard except for the Upper Case value of `1` defined as `½` instead of `!`. I can only assume that's how this flavour of IBM PC DOS interpreted that key? In any case I'm not so worried. There are also a few special cases under the `Ctr` layer which I also don't want to touch, as they would interfere with modern shortcuts. One jumped out `Backspace Ctrl = Del`. By default delete is on the dot key on the numpad.

![Extended functions, notable lines to be discuss below](extended-functions.webp)

Under extended functions I see `F11` to `F20` are under `Shift`, `F21-30` under `Ctr` and `F31-40` under `Alt`. Soarer's and modern operating systems seem to only support up to `F24`. This seems like a good place to have some macros later.

## Remapping

Soarer's software can be found [here](https://deskthority.net/viewtopic.php?t=2510), it includes an offline website in the `docs` folder which covers everything you need to know about the software. But I'll have a super high level explanation for the purposes of this post.

The `tools` folder contains the binaries required. The two main ones are `scas` to convert config text files to a config binary file like so: `scas <text_config> [<text_config> ...] <binary_config>` and `scwr` which writes the binary config to the converter `scwr <binary_config>`.

In the `configs` folder are example configurations, you don't need to define the entire keyboard layout rather anything you write is added on top. There's a file called `empty.sc` use this as a test run of sorts to get familiar with the software.

```
scas empty.sc empty_binary.scb
scwr empty_binary.scb
```

You'll get an output along the lines of:

```
$ scas empty.sc empty_binary.scb

scas v1.10
No errors. Wrote: empty_binary.scb


scwr empty_binary.scb

scrw v1.10
scwr: looking for Soarer's Converter: found
scwr: reading file: 6 bytes: ok
scwr: sending info request: ok
protocol version check: converter=1.00, scwr=1.00: ok
settings version check: converter=1.01, file=1.01: ok
settings length check: max=1018, file=117 bytes: ok
scwr: sending write request for 117 bytes: ok
device: ok
device: ready
scwr: sending 60 bytes: ok
device: ok
device: ready
scwr: sending 57 bytes: ok
device: ok
device: complete
```

`scas` is pretty solid software, it'll catch you out if you make a syntax error.

### Config File

First thing is to create an `sc` file, I made `marcos.sc` Everything I'm going to do is a macro, but look at the `config` folder, there are some pretty cool examples, such as toggling between Colemak and qwerty.

To start off we'll get the `PrtSc` working again:

```
macroblock
macro PAD_ASTERIX shift
  PUSH_META CLEAR_META shift
  PRESS PRINTSCREEN
  POP_META
endmacro
endblock
```

Saving that and running `scas` and `scrw` will do the trick.

Getting `F11` working was similar:

```

macro F1 SHIFT
  PUSH_META CLEAR_META SHIFT
  PRESS F11
  POP_META
endmacro
```

With the rest of the Function keys repeated, after `F24` I mapped `Ctr F3-10` to media keys and forward backward keys. I had trouble thinking of `Alt` layer macros, but it turned out not to an issue:

```
scas v1.10
error at line 134: block too large
```

## Wrapping Up

At the end of the day my `macros.sc` looks like this:

```
macroblock

# Enable asterisk shift to be printscreen
macro PAD_ASTERIX SHIFT
  PUSH_META CLEAR_META SHIFT
  PRESS PRINTSCREEN
  POP_META
endmacro

# Enable backspace ctr to be delete
macro BACKSPACE CTRL
  PUSH_META CLEAR_META CTRL
  PRESS DELETE
  POP_META
endmacro

# F keys shift for f11-20
macro F1 SHIFT
  PUSH_META CLEAR_META SHIFT
  PRESS F11
  POP_META
endmacro
macro F2 SHIFT
  PUSH_META CLEAR_META SHIFT
  PRESS F12
  POP_META
endmacro
macro F3 SHIFT
  PUSH_META CLEAR_META SHIFT
  PRESS F13
  POP_META
endmacro
macro F4 SHIFT
  PUSH_META CLEAR_META SHIFT
  PRESS F14
  POP_META
endmacro
macro F5 SHIFT
  PUSH_META CLEAR_META SHIFT
  PRESS F15
  POP_META
endmacro
macro F6 SHIFT
  PUSH_META CLEAR_META SHIFT
  PRESS F16
  POP_META
endmacro
macro F7 SHIFT
  PUSH_META CLEAR_META SHIFT
  PRESS F17
  POP_META
endmacro
macro F8 SHIFT
  PUSH_META CLEAR_META SHIFT
  PRESS F18
  POP_META
endmacro
macro F9 SHIFT
  PUSH_META CLEAR_META SHIFT
  PRESS F19
  POP_META
endmacro
macro F10 SHIFT
  PUSH_META CLEAR_META SHIFT
  PRESS F20
  POP_META
endmacro

# F keys ctrl for F21-24
macro F1 CTRL
  PUSH_META CLEAR_META CTRL
  PRESS F21
  POP_META
endmacro
macro F2 CTRL
  PUSH_META CLEAR_META CTRL
  PRESS F22
  POP_META
endmacro

# F keys ctrl for media keys
macro F3 CTRL
  PUSH_META CLEAR_META CTRL
  PRESS F23
  POP_META
endmacro
macro F4 CTRL
  PUSH_META CLEAR_META CTRL
  PRESS F24
  POP_META
endmacro
macro F5 CTRL
  PUSH_META CLEAR_META CTRL
  PRESS MEDIA_WWW_BACK
  POP_META
endmacro
macro F6 CTRL
  PUSH_META CLEAR_META CTRL
  PRESS MEDIA_WWW_FORWARD
  POP_META
endmacro
macro F7 CTRL
  PUSH_META CLEAR_META CTRL
  PRESS MEDIA_PLAY_PAUSE
  POP_META
endmacro
macro F8 CTRL
  PUSH_META CLEAR_META CTRL
  PRESS MEDIA_MUTE
  POP_META
endmacro
macro F9 CTRL
  PUSH_META CLEAR_META CTRL
  PRESS MEDIA_VOLUME_DOWN
  POP_META
endmacro
macro F10 CTRL
  PUSH_META CLEAR_META CTRL
  PRESS MEDIA_VOLUME_UP
  POP_META
endmacro
endblock
```

The question remains whether it's possible to refactor this to make use of an `Alt` layer, but for now I'm happy. Next stages are creating operating system specific macros.

[Source code for my keyboard configuration files can be found here.](https://gitlab.com/wagensveld/keyboard-config/)
