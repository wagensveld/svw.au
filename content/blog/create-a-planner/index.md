---
title: "Creating a Planner"
description: "I'll update this as I broaden my perspective on Bullet Journaling."
date: "2022-04-05T20:59:15"
category: "blog"
---

> Update: I didn't update this.

I have always struggled with staying productive. Every now and then I'll get the urge to be super productive; create a list of all the books I want to read, courses I want to complete, projects I want to develop. I'll end up sticking to this plan for a couple of days before losing motivation, and becoming intimidated by the huge todo list I've committed to.

## Bullet Journals

Bullet journal is a system of personal organisation. It can cover topics such as todo lists, schedules, goals, brainstorms, etc. It's incredibly flexible; What you keep track of is completely up to you (e.g. [amount of water drunk](https://www.bulletjournaladdict.com/collections/trackers/6-creative-water-trackers-for-your-bullet-journal/)).

Bullet journals are often broken down into the following components:

- `Index`: Which page each collection is on.
- `Key`: What each symbol means in logging.
  - `Bullet`: Way to mark a log item. E.g. `•` Could be a task, `🞨` a complete task, `O` a time specific event, and `⬤` a time specific event that's completed.
  - `Signifier`: Adds extra information to a bullet. E.g. `!` marks it important.
  - `Migration`: Often expressed as a bullet, denotes the log item has moved somewhere else.
- `Collection`: A category of logs, it can be daily consisting of tasks, an event consisting of preparation required, genre of movie with your favourite films, etc.
- `Logging`: An item within a collection, done in bullet form using the symbols defined in the Key.

The idea being you customise this system to your needs adding and removing collections from the index, and doing the same to bullets from the key.

Like I said bullet journals are incredibly flexible.

Which is exactly why I know it probably won't work for me (unless I really slowly ease into it).

## So Much To Do So Much To See

The main problem I'm trying to tackle is keeping a productive mindset. I know my brain well enough to know if I went deep into bullet journaling I would create a complex `bullet` system, and then be too afraid to properly use it out of fear I would break one of my own rules.

There are other considerations of the traditional bullet journal:

### Dedicated Productivity Object

I really love [single purpose tools](https://www.reddit.com/r/specializedtools/). The idea of having a notebook dedicated to productivity really appeals to me. For this to work for me the scope of a bullet journal must be very narrow.

### The Practice of Productivity

I really struggled with keeping productive during the Covid-19 pandemic. Working from home, I found it really difficult to 'start working'. I anticipate routinely writing goals and tasks will kickstart my work day.

### Information Permanence

Even with migrations and the index within a bullet journal, I can foresee finding it very difficult to find old ideas. I have opted to use [norg](https://github.com/nvim-neorg/neorg) for 'permanent' ideas.

On the other hand there is plenty of information which I don't care to keep. I'm not going to be looking for a record of all the times I've cleaned the stove. These go into the journal.

A note are day independent tasks - groceries. While ephemeral, migrating daily tasks seems a bit excessive. The todo app on my phone suffices.

## The Current System

For the journal I'm using a 2022 planner, one where every day has 10 or so lines for you to write events.

> too afraid to properly use it out of fear I would break one of my own rules

I wasn't kidding about this, it's April and I haven't written a thing in it up until recently. But that changes now.

### Day To Day

Before I start my day I write three things I want to achieve, anything from `do X chore` to `complete Y modules of Z course`.

At the end of the day I review tasks. Incomplete either gets migrated or marked invalid. Ideas get recorded.

### Structure

Being a daily journal with ephemeral information I didn't find the need to create an index. Collections are largely daily tasks.

Keys:

| Key         | Definition                     |
| :---------- | :----------------------------- |
| ·           | Task                           |
| X           | Task done                      |
| >           | Task migrated (within journal) |
| <           | Task migrated (out of journal) |
| -           | Ideas                          |
| c̶r̶o̶s̶s̶e̶d̶ ̶o̶u̶t̶ | Invalid task                   |

Signifiers:

| Signifier | Definition       |
| :-------- | :--------------- |
| !         | Important        |
| ⧖         | Time specific    |
| @         | Requires someone |

That marks the end and the beginning of my bullet journal journey.
