---
title: "National Australia Bank: Analyst DevOps Engineer"
description: "My job at NAB during my final year of university"
summary: "After my internship at NAB I was hired as permanent staff, while finishing my final four university units. I ended up becoming the sole DevOps resource on an AEM team, helping developers who's primary focus was front end code learn some of the tooling and practices involved in DevOps."
resumeItem: true
company: "National Australia Bank"
location: "Docklands, VIC, Australia"
titlePlatform: "Analyst DevOps Engineer - Digital Content"
date: "2019-01-24"
endDate: "2021-01-11"
timeSpentYears: "2"
skills:
  - "AWS"
  - "Akamai"
  - "Ansible"
  - "Jenkins"
  - "Splunk"
  - "Service Now"
  - "xMatters"
  - "Linux"
  - "macOS"
  - "Docker"
  - "AEM"
  - "Apache HTTP Server"
  - "Python"
  - "Groovy (Jenkinsfile)"
  - "Git"
  - "Atlassian"
responsibilities:
  - "Training frontend development teams in DevOps tooling and practices; and for production deployments."
  - "Out of hours on call production support for nab.com.au and NAB group sites."
  - "Stabilised build pipelines increasing AEM build success from 30% to 95%"
  - "Creation of Splunk logging, dashboards and alarms for production support."
  - "Dockerised Jenkins builds for quicker and more consistent builds."
category: "resume"
---

## Key Points

- Position: Analyst DevOps Engineer
- Department: Digital Content
- Dates: January 2019 - January 2021 (2 Years)
- Location: Docklands, Australia
- Tools
  - AWS (EC2, S3, Route 53, DynamoDB, Lambda, CloudWatch, CloudFormation)
  - IBM IOD
  - Akamai
  - DigiCert
  - Jenkins
  - Groovy (Jenkinsfile)
  - Ansible
  - Maven
  - Python
  - AEM
  - Apache HTTP Server
  - Docker
  - macOS
  - Amazon Linux
  - Jira
  - Confluence
  - Service Now
  - Splunk
- Responsibilities
  - Training frontend development teams in DevOps tooling and practices
  - Ensuring production and non-production environment stability
  - Out of hours on call production support for nab.com.au and NAB group sites
  - Creation of Splunk logging, dashboards and alarms for production support
  - Dockerised Jenkins builds for speed and consistency
  - DevOps support for SCRUM development teams
  - Jenkins pipeline development

## Day to Day

My normal day was slightly different to [my internship, also in Digital Content](/resume/nab-internship); at the time NAB was very much focused on enhancing the skills and responsibilities of their developers. I joined one of the a (formally) frontend teams (~15 people), to teach the developers how our infrastructure is set up, how to develop for it. And in turn I ended up learning a bit about front end development.

This arrangement played a large part in my professional development, it gave me a much greater perspective of what our frontend developers wanted out of DevOps, but also forced me to understand DevOps on our platform in depth as I was now providing guidance about it.

Another change occurred - everyone was put on an on call rotation. This was a good thing, it meant the health of the sites and monitoring became a priority everyone was focused on. I ended up creating a lot of Splunk logs, alerts, and dashboards; Jenkins jobs to automate routine tasks; and design changes to our AWS stack to address infrastructure related issues.

On the more positive end of production, I was the primary DevOps resource for out of hours deployments, documenting the steps involved in a release, training another developer to be able to run releases in the future.

## Projects

### 95% Success Room

A part of NAB's push to have all developers handle DevOps tasks meant we had to improve the reliability and usability of our environment building pipelines.

Myself with a couple of people from the old DevOps team, along with a couple of frontend developers were tasked with improving the environment creation process to where the average build success was 95%, including user error.

The first stage of the task was dissecting the build process, finding all of the points of failure, along with an effort versus reward analysis.

It it was broadly broken down into three categories: improving AWS infrastructure, better automation via Jenkins, and knowledge sharing.

We ended up reaching out 95% success rate goal, but most of importantly we documented everything.

### AEM 6.4 Upgrade

We had a fairly major upgrade; a new version of AEM requiring a new AWS account, Jenkins pipelines, and monitoring.

I mostly worked on migrating old maintenance pipelines, discarding and rewriting where necessary to match the new architecture.

### Production Support

While at NAB I acted as both primary and secondary support for nab.com.au and the group sites. The scope for this was quite wide - anything from an unhealthy load balancer to an element not rendering on the website. It ended up giving me an appreciation for the wider work of the greater platform.

### Training Teammates

I acted as a mentor within my team for DevOps tasks, and I did this in a few ways.

Presentations on topics my coworkers may find useful, inviting people to either shadow or do tasks with my guidance, and to document my day to day work so someone else can replicate it later.

## Good food near the office

Because [my internship also took place at this office](/resume/nab-internship), the recommendations remain the same.

### Cafes

- Story: Has my favourite coffee near the office, and a nice flat bread. It also has really nice baked sweets.
- Tuk and Co: Close second or equal best coffee nearby. They have a fantastic pork katsu sandwich.
- Focaccino: Their coffee is pretty good, their large is REALLY large. They stay open later than the other places and have a wide variety of tasty food.

### Restaurants

- Souperman: Even though they're a Soup place, I really like their burgers, surprisingly cheap and really crunchy chips.
- Hawkers Corner: Nice and quick Malaysian food right outside the office.
- Katsu House: Quick and tasty Japanese bentō.
