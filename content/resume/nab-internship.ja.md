---
title: "ナショナル・オーストラリア銀行: インターンDevOpsエンジニア"
description: "大学2年生のNABでのインターンシップ"
summary: "私は大学2年生の時に、2018年にデジタルコンテンツのDevOpsインターンとしてNABでのキャリアを開始しました。 部門はnab.com.auとグループサイトを担当します。私の配置が終了した後、私は正社員として留まりました。"
resumeItem: true
company: "National Australia Bank"
location: "Docklands, VIC, Australia"
titlePlatform: "IBL DevOps Engineer - Digital Content"
date: "2018-01-29"
endDate: "2019-01-24"
timeSpentYears: "1"
skills:
  - "AWS"
  - "Akamai"
  - "GitLab CI/CD"
  - "Jenkins"
  - "Remedy"
  - "SonarQube"
  - "Linux"
  - "macOS"
  - "Windows"
  - "AEM"
  - "Apache HTTP Server"
  - "Groovy (Jenkinsfile)"
  - "Atlassian"
responsibilities:
  - "Orchestrated nab.com.au and nab group releases."
  - "Performed disaster recovery exercises."
  - "Implemented SonarQube across the platform’s repositories."
  - "Provided DevOps support across two AEM development teams."
category: "resume"
---

## キーポイント

- 肩書: IBL DevOps Engineer
- 部門: Digital Content
- 日付: 2018 年 1 月から 2019 年 1 月 (1 年)
- 場所: オーストラリア、ドックランズ
- ツール
  - AWS (EC2, S3)
  - Akamai
  - Jenkins
  - Chef
  - SonarQube
  - AEM
  - Apache
  - macOS
  - Windows
  - Amazon Linux
  - Jira
  - Confluence
  - Remedy
- 職責
  - SCRUM 開発チームの DevOps サポート
  - Jenkins パイプライン開発
  - AWS/AEM 環境のプロビジョニング
  - 災害復旧演習

## 普通の日

私の日々の仕事は、Digital Content DevOps チーム(~5 から 10 人)と Digital Content フロントエンド開発チーム(~10 から 15 人)つという 2 つのチームに分かれていました。 DevOps チームはかんばんで、開発チームは SCRUM で、どちらも Jira、Confluence、Github を使用していました。

DevOps チームでの作業では、AEM/AWS スタックのメンテナンス用の Jenkins パイプラインの作成と、Akamai でのページルールの作成について説明しました。

開発チームは、AEM コード(Java)用の Jenkins パイプラインの作成、AEM サポート(ユーザーアクセス、ページのロック解除など)、基盤となる Apache HTTP Server の構成など、開発者サポートに重点を置きました。

## プロジェクト

### リリースを行う

nab.com.au および NAB Group の Web サイトリリースに対して日中の DevOps タスクを実行しました。 ランシートや変更要求の作成など、プレリリースタスクを含みます。

### 災害からの回復

AWS データセンターがオフラインになることをシミュレートするディザスタリカバリの演習に参加しました。これには、実装計画の作成、インシデントの提起、回復に必要な変更の実行、その後の通常の運用の復旧が含まれます。

### SonarQube の実装

NAB には、コード品質チェックを実行するイニシアチブがありました。これを実現するために、SonarQube サーバーをセットアップし、Jenkins ビルドパイプラインにコードチェックを実装する責任がありました。

## 事務所の近くのおいしい食べ物

### カフェ

- Story: 私のオフィスの近くに好きなコーヒー、そして素敵な Flat Bread。 それはまた本当に素敵な焼き菓子を持っています。
- Tuk and Co: 近くの最善のコーヒーを閉じる。素晴らしい豚カツスサンドイッチ があります。
- Focaccino: コーヒーはかなり良いです、Large は本当に大きいです。 これは他の場所よりも後で開いていて、美味しい食べ物を多数持っています。

### レストラン

- Souperman: スープの場所であっても、私は本当に彼らのハンバーガー、驚くほど安くそして本当にカリカリチップが好きです。
- Hawkers Corner: 事務所の外の素敵で素敵なマレーシア料理。
- Katsu House: 素早くおいしい弁当があります。
