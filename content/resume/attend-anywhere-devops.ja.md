---
title: "アテンド・エニウェア: DevOpsエンジニア"
description: "コロナショック時に遠隔医療会社で働く"
summary: "COVID-19のパンデミックの間、私は、英国の国民保健サービスとオーストラリアのいくつかの医療クリニックで使用されるビデオ会議ソフトウェアを開発およびホストする遠隔医療会社であるAttend Anywhereで働き始めました。 現在、クラウドチームのDevOpsエンジニアとして働いています。"
#timeSpentYears: "2"
#timeSpentMonths: "3"
resumeItem: true
company: "Induction Healthcare (formally Attend Anywhere)"
location: "Melbourne, VIC, Australia"
titlePlatform: "DevOps Engineer - Cloud and Operations"
date: "2021-01-12"
#endDate: "2021-01-12"
skills:
  - "AWS"
  - "Chef"
  - "Jenkins"
  - "Terraform"
  - "Linux"
  - "Windows"
  - "Docker"
  - "Groovy (Jenkinsfile)"
  - "Atlassian"
responsibilities:
  - "Migrated from a single AWS account to a multi account implementation."
  - "Provision AWS accounts / environments via Terraform, Jenkins, and Chef."
  - "Implemented security monitoring via AWS Config, Inspector, and tfsec."
  - "Advocated for an Infrastructure as Code approach - replacing manual processes and configuration with repeatable pipelines."
  - "Provided production support for the United Kingdom’s National Health Service’s telehealth software across four websites."
  - "DevOps support for teams across Australia, India, Pakistan, United Kingdom."
category: "resume"
---

## キーポイント

- 肩書: DevOps Engineer
- 部門: Cloud and Operations
- 日付: 2021 年 1 月から現在
- 場所: オーストラリア、メルボルン
- ツール
  - AWS (EC2, S3, ECS, Route 53, RDS, Elastisearch, Lambda, CloudWatch, SSM)
  - Jenkins
  - Chef
  - Docker
  - Terraform (Terragrunt)
  - Amazon Linux
  - Windows Server
  - Kibana
  - OpenVPN
  - Jira (Zephyr)
  - Confluence
- 職責
  - Terraform を介して AWS アカウント/環境をプロビジョニングする
  - 単一の AWS アカウントインフラストラクチャからマルチアカウント設計に移行
  - 安定した分岐と開発の実践
  - 標準的な展開中に手動で介入する必要をなくすためにパイプラインを実装しました
  - 英国の国民保健サービスの遠隔医療ソフトウェアの生産サポートを提供
  - Jenkins パイプライン開発
  - インド、英国、オーストラリア、パキスタンのチームに対する DevOps のサポート
  - カスタマーサポートチケットへの対応

## 普通の日

一日は主に、Attend Anywhere の開発環境と本番環境を安定させてさらに自動化するためのテラフォームモジュールの作成で構成されていました。 別のコンポーネントは、開発者にサポートを提供することでした。

サポートの性質は、いくつかの点で[NAB での以前の役割](/ja/resume/nab-devops)とは異なりました:

AA のチームは、オーストラリア、インド、パキスタン、イギリスのいくつかの国に分かれていました。 これは、タイムゾーンと調整する方法を見つけることを意味しました。

NAB の従業員数は 30,000 人を超え、AA の従業員数は 50 人未満です。これは、VPN やリモートデスクトップ接続などのサポートの種類だけでなく、Jenkins ビルドや環境などの従来の DevOps トピックもカバーすることを意味します。 安定。 それは私に会社の働きへのより大きな露出を内部的に与えました-私はより大きな組織で私が会ったことのない人々と話をするようになりました。 外部的に-私は、より大きな組織ではおそらく会わなかったであろうクライアントに直接より多くの露出を持っていました。

## プロジェクト

### AWS Migration

AWS のメジャーアップグレード中に Attend Anywhere に参加しました。 主に手動のプロセス環境から、大部分が自動化された環境まで、AWS アカウントのプロビジョニングに至るまで。

インフラストラクチャのアップグレードであるため、機能を同じに保ちながら、アプリケーションを実行するハードウェアのみを変更するように細心の注意が払われました。

私は会社に不慣れで、非破壊的な Terraform モジュール(CloudWatch Alarm など)から始めて、データベースなどのより多くのサイト統合サービスに移行しました。

その後、Zephyr（Jira）で記録されたテストを作成して実行することにより、アプリケーションがどのように機能するかについて詳しく学びました。

## 事務所の近くのおいしい食べ物

### カフェ

- Axil: 素晴らしいコーヒー。
- Fiord: 素晴らしいコーヒー。
- Schmucks: 本当に美味しいベーグル。
- Dough Bros: 本当に美味しいドーナツ。
- Spin: 素晴らしいコーヒー、オフィスのすぐ外。
- LB2: 素晴らしいコーヒー、素晴らしいホットチョコレート。
- Oli & Levi: 大きなサイズの素晴らしいコーヒー。

### レストラン

- Cinnamon's: スリランカのカレー、なすを試してみてください、それは素晴らしいです。
- Royal Stacks:　おそらく、市内で 2 番目に好きなハンバーガー店です。
- Roti Bar: 少し値段は高いですが、ロティを使った素晴らしいマレーシア風カレーです。
- Kedai Satay: チキンサテ串は素晴らしいです、サンバルでそれを手に入れてください-それはスパイシーな慰めのタイプです。
- Spicy Korea: *スパイシーコリア*と呼ばれていますが、それほどスパイシーではありませんが、私が持っていたものはすべて一流です。
- Dari Cafe: メニューには 7 つほどのアイテムしかなく、すべてサンドイッチで、すべて素晴らしいです。
- The Kings Hotel: 私はパルマとパイントのために場所に行きます。
