---
title: "National Australia Bank: IBL DevOps Engineer"
description: "My internship at NAB during my second year of university"
summary: "I started off my career at NAB as a DevOps intern in 2018 for Digital Content during my second year of university. The platform is responsible for nab.com.au and group sites, after my placement ended I stayed on as a permanent employee."
resumeItem: true
company: "National Australia Bank"
location: "Docklands, VIC, Australia"
titlePlatform: "IBL DevOps Engineer - Digital Content"
date: "2018-01-29"
endDate: "2019-01-24"
timeSpentYears: "1"
skills:
  - "AWS"
  - "Akamai"
  - "GitLab CI/CD"
  - "Jenkins"
  - "Remedy"
  - "SonarQube"
  - "Linux"
  - "macOS"
  - "Windows"
  - "AEM"
  - "Apache HTTP Server"
  - "Groovy (Jenkinsfile)"
  - "Git"
  - "Atlassian"
responsibilities:
  - "Orchestrated nab.com.au and nab group releases."
  - "Performed disaster recovery exercises."
  - "Implemented SonarQube across the platform’s repositories."
  - "Provided DevOps support across two AEM development teams."
category: "resume"
---

## Key Points

- Position: IBL DevOps Engineer
- Department: Digital Content
- Dates: January 2018 - January 2019 (1 Year)
- Location: Docklands, Australia
- Tools
  - AWS (EC2, S3)
  - Akamai
  - Jenkins
  - Groovy (Jenkinsfile)
  - SonarQube
  - AEM
  - Apache HTTP Server
  - macOS
  - Windows
  - Amazon Linux
  - Jira
  - Confluence
  - Remedy
- Responsibilities
  - DevOps support for SCRUM development teams
  - Jenkins pipeline development
  - AWS/AEM environment provisioning
  - Disaster recovery exercises

## Day to Day

My day to day work was split between two teams, the Digital Content DevOps team (~5-10 people) and one of the Digital Content frontend development teams (~10 to 15 people). The DevOps team was Kanban based, while the development team was SCRUM, both using Jira, Confluence, and Github.

Work in the DevOps team covered the following: writing Jenkins pipelines for the maintenance of an AEM/AWS stack and creating page rules in Akamai.

The development team focused more on developer support, things such as writing Jenkins pipelines for AEM code (Java), AEM support (user access, unlocking pages, etc), and configuring the underlying Apache HTTP Server.

## Projects

### Running Releases

Starting off with shadowing, and working up to independent work; I ran the daytime DevOps tasks for nab.com.au and NAB group website releases. Including the pre release tasks, such as creating runsheets and change requests.

### Disaster Recovery

I took part in a disaster recovery exercise, which simulated an AWS data centre going offline. This involved developing the implementation plan, raising the incidents, implementing permanent fixes after incidents, and afterwards restoring business as usual operation.

### SonarQube Implementation

NAB had an initiative to enforce code quality checks, in order to fulfil this I was responsible for setting up a SonarQube server, and implement code checks into our Jenkins build pipelines.

## Good food near the office

### Cafes

- Story: Has my favourite coffee near the office, and a nice flat bread. It also has really nice baked sweets.
- Tuk and Co: Close second or equal best coffee nearby. They have a fantastic pork katsu sandwich.
- Focaccino: Their coffee is pretty good, their large is REALLY large. They stay open later than the other places and have a wide variety of tasty food.

### Restaurants

- Souperman: Even though they're a Soup place, I really like their burgers, surprisingly cheap and really crunchy chips.
- Hawkers Corner: Nice and quick Malaysian food right outside the office.
- Katsu House: Quick and tasty Japanese bentō.
