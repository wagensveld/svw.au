---
title: "Induction Healthcare (formally Attend Anywhere): Cloud Engineer"
description: "Working at a telehealth company during a pandemic"
date: "2022-07-11"
endDate: "2022-07-15"
timeSpentYears: "1"
timeSpentMonths: "6"
resumeItem: true
company: "Induction Healthcare (formally Attend Anywhere)"
location: "Melbourne, VIC, Australia / St Kilda, VIC, Australia"
titlePlatform: "Cloud Engineer - Cloud and Operations"
skills:
  - "AWS"
  - "Chef"
  - "Jenkins"
  - "Terraform"
  - "Linux"
  - "Windows"
  - "Docker"
  - "Groovy (Jenkinsfile)"
  - "Git"
  - "Atlassian"
responsibilities:
  - "Migrated from a single AWS account to a multi account implementation."
  - "Provision AWS accounts / environments via Terraform, Jenkins, and Chef."
  - "Implemented security monitoring via AWS Config, and Inspector"
  - "Advocated for an Infrastructure as Code approach - replacing manual processes and configuration with repeatable pipelines."
  - "Provided production support for the United Kingdom’s National Health Service’s telehealth software across four websites."
  - "DevOps support for teams across Australia, India, Pakistan, United Kingdom."
  - "Creation of Attend Anywhere site for the DWP."
category: "resume"
draft: true
---

## Key Points

- Position: Cloud Engineer
- Department: Cloud and Operations
- Dates: January 2021 - July 2022
- Location: Melbourne, Australia / St Kilda Australia
- Tools
  - AWS (EC2, S3, ECS, Route 53, RDS, Elastisearch, Lambda, CloudWatch, SSM)
  - Jenkins
  - Chef
  - Docker
  - Terraform (Terragrunt)
  - Amazon Linux
  - Windows Server
  - Kibana
  - OpenVPN
  - Jira (Zephyr)
  - Confluence
- Responsibilities
  - Provision AWS accounts / environments via Terraform
  - Migrated from a single AWS account infrastructure to a multi account design
  - Stabilised branching and development practices
  - Implemented pipelines to remove the need for manual intervention during standard deployments
  - Provided production support for the United Kingdom's National Health Service's telehealth software
  - Jenkins pipeline development
  - DevOps support for teams across Australia, India, Pakistan, United Kingdom
  - Addressing customer support tickets
  - Creation of Attend Anywhere site for the DWP

## Day to Day

My day largely consisted of writing Terraform modules to stabilise & further automate the Attend Anywhere development and production environments. Another component was providing support for the developers.

The nature of the support was different from [my previous role at NAB](/resume/nab-devops) in a few ways:

The team at AA was split between a few countries: Australia, India, Pakistan, United Kingdom. This meant finding ways to coordinate with time zones.

NAB has over 30,000 employees, AA has under 50. On one hand this meant the type of support I gave was a lot broader, covering things like VPN and Remote Desktop connectivity, as well as the more traditional DevOps topics such as Jenkins builds and environment stability. It gave me greater exposure to the workings of a company, internally - I got to talk to people who in a larger organisation I would have never met; externally - I had more exposure to clients directly whom I would have likely not met in a larger organisation.

Later on Attend Anywhere got acquired by Induction Healthcare, this presented a new opportunity to grow, to see how work and culture changes when going from a small self-contained organisation, to a larger one with differing products.

## Projects

### AWS Migration

I joined Attend Anywhere during a major AWS upgrade; from a largely manual process environment, to one largely automated - down to AWS account provisioning.

It being an infrastructure upgrade, great care was taken to only change the hardware the application runs on, while keeping the functionality the same.

Me being new to the company I started off with non destructive Terraform modules (e.g. CloudWatch alarms), and then moved onto more site integral services such as databases.

Afterwards I learnt more about how the application works by means of writing and executing tests recorded in Zephyr (Jira).

### DWP Project

The contract with the British Government's Department for Work and Pensions (DWP) was the first one since our AWS migration

## Good food near the office (Melbourne)

### Cafes

- Patricia's: Fantastic coffee
- Axil: Great coffee.
- Fiord: Great coffee.
- Schmucks: Really good bagels.
- Dough Bros: Really good doughnuts.
- Spin: Great coffee, right outside the office.
- LB2: Great coffee, and fantastic hot chocolate.
- Oli & Levi: Great coffee, with large sizes.

### Restaurants

- Cinnamon's: They do Sri Lankan curry, try their eggplant, it's fantastic.
- Royal Stacks: Probably my second favourite burger place in the city.
- Roti Bar: They're a little bit pricey, but they do wonderful Malaysian style curries with roti.
- Kedai Satay: Their chicken satay skewers are fantastic, get it with sambal - it's a comforting type of spicy.
- Spicy Korea: Despite being called _Spicy Korea_ they're not that spicy, everything I have had there has been top tier.
- Dari Cafe: They only have 7 or so items on the menu, all sandwiches, all fantastic.
- The Kings Hotel: My go to place for a parma and a pint.

## Good food near the office (St Kilda)

### Cafes

- 580 Bench: Great coffee.
- Coffee Society: Great coffee.

### Restaurants

- Cafe Batavia: Cheap and really fantastic indonesian curry. Occasionally they have fritters which are phenomenal.
- Trang Bakery & Cafe: Cheap and great salad rolls.
- Cafe Safi: Cheap, tasty food, wide variety.
- Tapenade: Lovely French food.
- Rice @ Chevron: Tasty and cheap Chinese takeaway.
