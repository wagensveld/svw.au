---
title: "ナショナル・オーストラリア銀行: アナリストDevOpsエンジニア"
description: "大学最終学年のNABでの仕事"
summary: "NABでのインターンシップの後、私は常勤従業員として雇われ、最後の4つの大学講座を修了しました。最終的にAEMチームの唯一のDevOpsリソースになり、フロントエンドコードが主な目的である開発者がDevOpsに関連するツールとプラクティスのいくつかを学ぶのを支援しました。"
resumeItem: true
company: "National Australia Bank"
location: "Docklands, VIC, Australia"
titlePlatform: "Analyst DevOps Engineer - Digital Content"
date: "2019-01-24"
endDate: "2021-01-11"
timeSpentYears: "2"
skills:
  - "AWS"
  - "Akamai"
  - "Ansible"
  - "Jenkins"
  - "Splunk"
  - "Service Now"
  - "xMatters"
  - "Linux"
  - "macOS"
  - "Docker"
  - "AEM"
  - "Apache HTTP Server"
  - "Python"
  - "Groovy (Jenkinsfile)"
  - "Atlassian"
responsibilities:
  - "Training frontend development teams in DevOps tooling and practices; and for production deployments."
  - "Out of hours on call production support for nab.com.au and NAB group sites."
  - "Stabilised build pipelines increasing AEM build success from 30% to 95%"
  - "Creation of Splunk logging, dashboards and alarms for production support."
  - "Dockerised Jenkins builds for quicker and more consistent builds."
category: "resume"
---

## キーポイント

- 肩書: Analyst DevOps Engineer
- 部門: Digital Content
- 日付: 2019 年 1 月から 2021 年 1 月 (2 年)
- 場所: オーストラリア、ドックランズ
- ツール
  - AWS (EC2, S3, Route 53, DynamoDB, Lambda, CloudWatch, CloudFormation)
  - IBM IOD
  - Akamai
  - DigiCert
  - Jenkins
  - Ansible
  - Maven
  - Python
  - AEM
  - Apache
  - Docker
  - MacOS
  - Amazon Linux
  - Jira
  - Confluence
  - Service Now
  - Splunk
- 職責
  - DevOps ツールと実践でフロントエンド開発チームをトレーニングする
  - 本番環境と開発環境の安定性の確保
  - nab.com.au および NAB グループサイトの営業時間外の本番サポート
  - 本番サポート用の Splunk ロギング、ダッシュボード、アラームの作成
  - 速度と一貫性のために Docker された Jenkins ビルド
  - SCRUM 開発チームの DevOps サポート
  - Jenkins パイプライン開発

## 普通の日

私の日々の仕事は、Digital Content DevOps チーム(~5 から 10 人)と Digital Content フロントエンド開発チーム(~10 から 15 人)つという 2 つのチームに分かれていました。 DevOps チームはかんばんで、開発チームは SCRUM で、どちらも Jira、Confluence、Github を使用していました。

DevOps チームでの作業では、AEM/AWS スタックのメンテナンス用の Jenkins パイプラインの作成と、Akamai でのページルールの作成について説明しました。

開発チームは、AEM コード(Java)用の Jenkins パイプラインの作成、AEM サポート(ユーザーアクセス、ページのロック解除など)、基盤となる Apache HTTP Server の構成など、開発者サポートに重点を置きました。

## プロジェクト

### 95%サクセスルーム

すべての開発者に DevOps タスクを処理させるという NAB の推進の一環として、環境構築パイプラインの信頼性と使いやすさを向上させる必要がありました。

私自身、古い DevOps チームの数人と、フロントエンド開発者の数人が、ユーザーエラーを含め、ビルドの平均成功率が 95％になるまで環境作成プロセスを改善するという任務を負いました。

タスクの最初の段階は、ビルドプロセスを分析し、すべての障害点を見つけ、努力と報酬の分析を行うことでした。

これは、AWS インフラストラクチャの改善、Jenkins による自動化の改善、知識の共有という 3 つのカテゴリに大きく分類されました。

最終的に 95％の成功率の目標を達成しましたが、最も重要なことは、すべてを文書化したことです。

### AEM 6.4 のアップグレード

かなり大きなアップグレードがありました。 新しい AWS アカウント、Jenkins パイプライン、モニタリングを必要とする AEM の新しいバージョン。

私は主に、古いメンテナンスパイプラインの移行、新しいアーキテクチャに合わせて必要な場所での破棄と書き換えに取り組みました。

### 本番サポート

NAB にいる間、nab.com.au とグループサイトのプライマリサポートとセカンダリサポートの両方を務めました。 これの範囲は非常に広範でした-不健康な Load Balancers からウェブサイトに表示されない要素まで。 それは私に、より大きなプラットフォームのより広い仕事に対する感謝を与えることになった。

### チームメイトトレーニング

私はチーム内で DevOps タスクのメンターとして行動しましたが、これはいくつかの方法で行いました。

同僚が役立つと思われるトピックに関するプレゼンテーションは、人々を Shadowing するか、私のガイダンスに従ってタスクを実行するように招待し、他の誰かが後でそれを複製できるように私の日々の仕事を文書化します。

## 事務所の近くのおいしい食べ物

[インターンシップもこの事務所で行われました](/ja/resume/nab-internship)ので、推奨事項は同じままです。

### カフェ

- Story: 私のオフィスの近くに好きなコーヒー、そして素敵な Flat Bread。 それはまた本当に素敵な焼き菓子を持っています。
- Tuk and Co: 近くの最善のコーヒーを閉じる。素晴らしい豚カツスサンドイッチ があります。
- Focaccino: コーヒーはかなり良いです、Large は本当に大きいです。 これは他の場所よりも後で開いていて、美味しい食べ物を多数持っています。

### レストラン

- Souperman: スープの場所であっても、私は本当に彼らのハンバーガー、驚くほど安くそして本当にカリカリチップが好きです。
- Hawkers Corner: 事務所の外の素敵で素敵なマレーシア料理。
- Katsu House: 素早くおいしい弁当があります。
