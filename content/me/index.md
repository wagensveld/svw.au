---
title: "About Me"
description: "Who is Sebastiaan?"
date: "2024-05-29T22:23:15"
aliases:
  - "/about"
category: "me"

images:
  - "images/profile.jpg"

---

![Me at the Fuji Shibazakura Festival in Fujikawaguchiko, Japan 2023](profile.jpg "Me at the Fuji Shibazakura Festival in Fujikawaguchiko, Japan 2023")

Hi there! I'm Sebastiaan -- the protagonist of this website. Thanks for dropping on by {{< custom-emoji "waving-hand" "👋" >}}

I'm from Melbourne, Australia, and living up to the stereotype [I have opinions on the local cafes](/misc/melbourne-cafes). In my free time I enjoy DIY projects, repairing old devices, and reading.

I originally intended this blog to be a place where I write [blog posts](/category/blog/) about DevOps, technology, and other various topics that have caught my interest. While starting out with this blog I found it useful to have a place where I could keep notes I could readily access, that's why the majority of the posts are [recipes](/category/recipes/).

The site has been a great learning experience for both web development and writing. But as you can see there's still quite a lot to do!

Related to that: my occupation. I've been a DevOps Engineer for the past {{% years-since 2018 %}} years. I've primarily worked with tools such as AWS, Terraform, Docker, Jenkins. A lot of that work has focused on streamlining the way how we provision infrastructure for the cloud. At the moment I work for [Secure Code Warrior](https://www.securecodewarrior.com) a gamified cyber-security education company based in Sydney, Australia.

If you're curious [here are some things which help me do what I do](/stuff-i-use).

## Contact Information

If would like to get in contact, the best way is probably via email at [t@svw.au](mailto:t@svw.au).

If PGP means anything to you, you can import my key via `gpg --locate-keys t@svw.au` ([see how that's done here](/guides/gpg-wkd-on-a-static-site)).

[Here is a direct link to my public key](/documents/svw.asc).

<details><summary>PGP Key</summary>

```
-----BEGIN PGP PUBLIC KEY BLOCK-----

mQGNBGCR738BDACBRK7pcZgkyNVAvubjY4LOTqFo1Yvy2oMcU/zGib2FYDMmxbSH
xRKbpg0eDtIWbFVyypoZyFZTP75SM5Xjio6njiu8T8LLrmui339WM2+JJ+Y1fc06
3KX1GQc2eJEdjJr3QLtO4/WhpIwTy9fA578grtLdnppTTPa0p+9XFP5pIqk7waa8
/9luI8m0+grbHwILRmmE6E/sToBXKC5lTeTbUZewWIqdil+58uYNrGnHBRo5/Hy5
+IRQHF7VK0Svbnl6k8aDpxqJljsCzPGcFSZrufialxlVAFfQoS61qZPKNZvXhxPB
WZe+Y2PBL6jJH0Ck6C34dXJ4LDzY96B+UJPSE/6ponZbsgkv7UWgErTwrhbvAtbo
KtdDAVG8YjcUpJr47T+cCe7mAEH8azHpfFBMPoqQrvVYFFCDMOJpS2vfGJYYWJj4
kO9UReIYo/BNl7nZ+U48pdTBVH09MLzufMEYw+Z+qjOEY/+HUnasIyh99C6DY3uX
srbWDJjURfS8FIcAEQEAAbQVU2ViYXN0aWFhbiA8dEBzdncuYXU+iQHRBBMBCAA7
AhsDBQsJCAcCBhUKCQgLAgQWAgMBAh4BAheAFiEEdTyutumNK749YBI62nnjw4g9
/XMFAmNPy8UCGQEACgkQ2nnjw4g9/XN+5Qv6AkF/keTyhVWuI2U+GlB9JuFU/fSv
QXr1BRbZPyYvQsmH2veb18TTr8BoM6hruC+MOTQXbL3WtoofXc2vrleGyo20c9xa
2i3oa87QOI/ig+5LnHsF3ypC+ldGpiT9C31Gf2wRSduyDNy/eJCD8G+D0Eb3DLv9
yiMReqplQ6h7wdxDYXtdLkbvuIgylTV+NgMzcR3sZAd2UGxocgXnjJ6H3z95lVOH
ikCkoQeTCtNmOCnYlwJs2UuHm3ZnPQZpnEIB3r3+zz4qf79o3nCw06acUA0aAauY
RJXrQYsWBGGcUq3xYz9l7ynjzdittqw0qoKYp+BnVssUDm7RqGWliFs4VNhOrdbK
us4ByeR/9aFguy6ulRDDzilZFvgXy0hy83AKQ1/N9qPStEkESDCU3vmDY/SL/Q7J
nRIHYW9rTkdC1PjVeKYqLVtv04sElo+JDYXXRpwinTJWG6Wv9MijorEdJQOVo7MY
ui8xzcXsK0sWEIWFEI15lCV/7XMhBAp899CAiQGwBBMBCgAaBAsJCAcCFQoCFgEC
GQEFgmM8BYMCngECmwMACgkQ2nnjw4g9/XM5TQv8D4aJksg4h/FeJcU4yoDb0ZxH
GbW6lWUp8YZY0XqXBPtULpjPizc0P3UtNVxVmLf34VESF9zekZ1FEeLEaLGLRJuH
i54hJPBOWG8APgtsOxtKdTbfa/w52IYd3jOhdUMNkt4X/3UyKk0TPqXMhfl3tG4o
JOphgEZyrIU2Tx1wneQMO6Kuoxu3g3XaKNgZW86HIe4qlvrYz2K9dpEf4rOsKaJz
hJkI0iKKldRrwJiF+xb3DsSZsG+kIG0u1Y3M5Bgp34E5gntyMQL95yOjDpLPaEQc
qohxQX1UdIhhfI0QWU0EIgihoBuOlPN8uF2NvDMJCCEGKF7ohFEoOXRUtQye6WUI
cwXfkwspq4642dOK3xZ/0ZOYZxVpIrScaTa4qQKdabDCZxMiT8aA+iHFm/RPpsGR
/8WD+1es3a+vuZj7D/c1XlDM10ZsWzQS9AJKYpSJPTTzljK9PoDRifPzkMl/pP1G
bgkiPz8PXhO2Y0dEFnSxkxkH+dWdUMbXmkURLQXwtChTZWJhc3RpYWFuIHZhbiBX
YWdlbnN2ZWxkIDxtZUBzdmFudy5jb20+iQHEBBMBCgAuBAsJCAcCFQoCFgECngEC
mwMWIQR1PK626Y0rvj1gEjraeePDiD39cwUCYkBTmQAKCRDaeePDiD39cyLIC/4s
oy442EpgvuCtXhzTwBLcfiVUBO4zh3ho/7jeooE4TfeWebSZillsrj9aA4IeODRF
6dQ1sp5UjKKkOhikIogav/rrOpHx59qiInTDT69nX/+k0c8VNHanbdOMumlAw+d8
0Q0kvwGy8al4PQNDBq/gVPZ1d1/lVldDvhVhQSI3B79Dl5qe+M7Jfd+2VL2rql1N
4K/4/0UxsfV+/Wnkh0amA1oiyUsJywVhTG5L/XQURgs23FLauKkBkMkWyyzcr4Rq
hcCG7q50iqRUwwAWI+v/Oq0oVZdmIKkadj0I9Q3Jr+E4AJ3WCO/20QafhNWByh2L
ZQSqCt52fo0Nt2ynDHBJNEALyFztpr+w5QgVS+kpL7FXwEovALgpvRqx/vIp1dXd
76eg5ro8gjdT7aKxwYsJcwYMpqtcCyFkk1wN3OoPsRZdjb5UbCuzq+2742V7T1LR
/0slV2cItB5IZo2pCccVzJ06HJQIDqlxpNYMxFoBoEvfyNwQfrQmn9k0e8deBuqJ
AZ8EMAEKAAkCHQAFgmWDsW4ACgkQ2nnjw4g9/XOGVwv/YqILnw4VoEfgIywPUM0U
cS3zuGSbPBV8rLQXpklv7Pp+AtLg0RcNPgmUBWCtbAsDDbkyb9K0NmYAheDVpcLW
T+K4kIoM0H7bdWCb/h4bdT2emRpO6aUiiSEt8cfi4z6Q4lV5X7gqY+DVLZRsKWwK
rjD7ZUJvU/gqvr2byMNrfh0YLPcPgMUJJZAQilwE98MqN0pzTXFmYZoDBLGA1nzW
Bsl2GkSkFJwJsa7eRsjjcDvTSPJgOvOnEOoU9h9PRbl6c+oUCU/5GImf7Wd+mqv5
NfEi5NPWBdinajUAh4mSLnPJu8pKziBhFSecp6+WdwgQzczvk3Q5yDg1n3SCjGdW
uav+0cmuXSSMPPJ5WUQLPxLaUoxRYaYQJdv1CTyuPAy8TUtEWtmG3ZHwG5KrRc4K
3QCv74N9ijxsEyOi2+/U22a5RAghWXDBeZvMl64E8WYIN/DsfH2IswB4E945APMi
kXb3iPFcBe6/N9U+QwTJLoYH6tmMtipp3M+B9Bta3ivxtCZTZWJhc3RpYWFuIHZh
biBXYWdlbnN2ZWxkIDxtZUBzZWJiLmF1PokBsAQTAQoAGgQLCQgHAhUKAhYBAhkA
BYJjPAWDAp4BApsDAAoJENp548OIPf1zgOQL/AoBO9vDAR3Ervkkoap+S9sJT/Gr
bgaEiz3X/Enva7GzeImjV3fWfzwe+cfobtMXPieMszL1/Zzsb2KpQCOMGW3iGd54
f/u3D7EKhkuoXXq/balBMQ1ZuI5OvAHP/vfPEqKnmS2rRaIX7RxGR6fgCsAjozP4
mE2Deqnq13NFW3qVsuactWkrouwIyW58XBgUKUdCRxum7vwVx+QZadLfWsPbmSu8
OwopTz/Z3nCZr5Tn4wSJOaqn4j6ofb8oNDCSdZm9aSt96xOoK6RekLtQMP+JWALU
PDd/RN3JREjDv6ltDiY5TFB+yzY7ILMdLQootJXkmZPjI6ak4lwF+KOhpXY3O9JZ
fHw5ZdE3rDRVBXDDVzmelBnnPWtYHcwCeUWoOjOiXTRsiDbK3CBBkKgFJEBqPBVs
IX3Wu/6wDuGtCx7i2j/YbATJ4FmqpYOn/e+4AZz4V8ZY7m0XApgHxa9uy/5wVifQ
7eXwOVsRk/Wd5ZBzTjTFeHeOlMSRDssHnfUL4YkBnwQwAQoACQIdAAWCZR0MfAAK
CRDaeePDiD39c8QYC/425ZKoQfNTlgFZaerj8ofk8IWOGeePJ08qBy10I2r8wvLd
I86IZuIvjoJ9lSAw1eTanml6rADp+G/Uu/gD+1SBzPfMfU8zIHvvoo1aqbS2fcRE
pPQNoLpIBtNOgmy68ifhQjjycPH1JOIzfuj2omRf8zmLUnddEUYUGb+kziGvKTt+
qwIMcyc4ynat2R08emvwU2D2EJZZYgJosWJ0SWRt4jWqjdVZCTTViCDUlsDpXh+W
hV3SSXNs1N733WMm2IuKPoWTUBkKXnN0I0T6w/6cwuaxRhDyTKK7YPGgDEpQ2VoF
8c6Zc5Y2wdbMcDEeq2VleJFHiHu0dls3h5F4HjC3RoyFBPp1Rzc0dClavWNgK2cJ
/DOW8Pl5I7vKH0sBk/RKWC+cgm1b1A0yBSB5L7fvYBJR6lmvnYbj84M8wCJTsAtO
vDcAOTZb9FMNPfA7qgnUe2WZvAGNuajEeUHjgZ1ZsHoqUVpPfd4oJeTPkhG+zKEq
EcoDeuU9Px8sm/0qcrS0GVNlYmFzdGlhYW4gPG1lQHN2YW53LmNvbT6JAbAEEwEI
ABoECwkIBwIVCAIWAQIZAAWCZgteugKeAQKbAwAKCRDaeePDiD39c8gZC/9kRpk0
XSLRcoimLKArr2gSudIJR/x/ZXZaeBdyu0+3fBAP5eXdC7ktVMkUxxEXyDtpMj8E
daWKpY6oPePeHuBMZp81HNKZ8q7TBetkzco+5d0wAZEIah+qDStT/ZosceMBNlC8
VcJnaqRlnljNm6AM9wSIzBYePWF55ll6hlGZN22EeSLO7VyFKK7cPzg54Wn4ZoIl
GxGhR21RP1+arOI+AyIDZtteggGCJMBqqz3k7z6fwf3CAfwh+9VJ8TTQax3eDS2n
DRFDPJWbo+gRld+o8LOvo3EJ66B1wd/E76hNu+E7DUYNAXJ1+EnCfKunx+ihQoq/
aHsYCofyWeO6JJ0bP1Da3DYnbttUcSm2u3XFKhNu4eKW1Z6MJawmHV2IDGgxy5SF
Ne/rldcJxVwRME31dSL79BTNmkyG6uT5SLX3jewJzWrmHoV6yEVhoBn9YQNUfk0g
sqTX/k3dViIYm/iJ5Ox4YiMIgxYdpk4zunzeJOy4h6o5ehOGvqFS7i5jVFW5AY0E
YJHvfwEMAMTcD79lpCfhmMqZ6PX79CCneYbHBYqX8+dD76K6a5VW+H4oEhZpm9Wf
A23QrK0QCP4lvu+9PXhprouLsayavEGs33syyFlCng+a+8LwBvAym8gg/6JxuIQ6
u0eCWk+IPr3Q6QM0pD/hEx0Q56sdOkkP4KQqV36sWIYMAkXh3GPmkAt7Nq1aj8yu
QX9Fu1OtKEXtAbXN98mkzVNPTmOFzcZTlGymX/XkAU/u4VzvxNqyp3Y9KQ93fgs9
MF2cnC869ulfG1IkjpK+ZUlLxIqgsXg0goyNntWqsC8hNyFuXqmpdOslYf23MDHW
KvRG3bl4OYUL48m3+pFO+dE7aoa2Vc3NPuleNRVayvI/pS/zfM6XO5Tv4HDW5+Un
oQ0nFOBVuTDSmtWSt+XxCxjVlC0jxvjmKSOXCot93QCI7TWacXGNPUek76JKTsmq
4yzY4drYk81it3aJ+5oAOtCWcRFspTgr77T79zFpJUU7pgF9peKUcXf7Ad+FFEA2
e4GNQyd6BQARAQABiQG2BBgBCgAJBYJgke9/ApsMACEJENp548OIPf1zFiEEdTyu
tumNK749YBI62nnjw4g9/XOQwQv/XTVpbbujA1mPdA9IhHw0u9QEwgaIvYTqDBWn
JXBzKh2YvbkrrEDonGPfrrby8nYh7RsTvFsp6Uuqi0dUJla7CqdArxni15ED7pKc
4NuON3OH2QtONaIdM+kj9Wlt4f/M+K9PXZl/Iv8ON/npn2BwDDg2iPTIuS7ii1tN
L7noZT7r30DAOIixPKlrvnRWSfni9F1JRpCxrInqtsWFQw66Mk03JwuT5EOeq5Kn
Mq09jeJECxjOcYKPDGBsXxS5VPlRdbrbvLCfPnadgvhXdYC711uRBQiQYJv1mLMO
WNtbidXRhXg4zx0GjXMzz8NUGwIGspfQlYs+9MP80nYOHu7acthKjsHvWp+aCCpG
COxRTcF8NJf1+7NzScIRaGsdqV58irpWX98ggCh4vfdVN7qW4jru+OUrPRDPyfsO
Q+HWNJwae+/GUKJ8JXIn7S4yuYmRAM/h1LNXOIlGrd6iNIAQj8Df/N+2WG6flGLZ
BNKJjFtMGW0sqaMOGOQe34QRFRZv
=WdjB
-----END PGP PUBLIC KEY BLOCK-----
```

</details>

I have accounts on the following social networks, but seldom log on:

- [GitLab](https://gitlab.com/wagensveld)
- [GitHub](https://github.com/wagensveld)
- [LinkedIn](https://www.linkedin.com/in/wagensveld)
