---
title: "Find AMI ID without launching an EC2 Instance"
description: "How to find an AMI ID on the AWS marketplace without having to launch an EC2 instance beforehand."
date: "2021-07-15T20:58:32"
category: "guides"
---

1. Go to the [AWS Marketplace](https://aws.amazon.com/marketplace/), search for the image you need. Select it.
2. Click "Continue to Subscribe" (You may need to log in if you're not already).
3. Click "Continue to Configuration".
4. Configure "Delivery Method", "Software Version", and "Region" - All of these will change the AMI ID.
5. Your AMI ID will be towards the bottom.
