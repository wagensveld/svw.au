---
title: "Seed Jobs in Jenkins"
description: "Using Job DSL"
summary: "Using Job DSL"
date: "2021-08-06T21:22:18"
series: ["devops"]
category: "guides"
---

Having Jenkinsfiles for our jobs is the goal, however by default you can only create one Job from a git repo, its not very feasible to have a new repository for each one off maintenance job, or parent job.

One way to have a repository of Jenkins jobs is to use the [Job DSL plugin](https://plugins.jenkins.io/job-dsl/).

[Use my repository as a guide.](https://gitlab.com/wagensveld/jenkins-jobs)

(The following is largely taken from that repository's readme file)

## Setup

First thing which you need to do is install the `Job DSL` plugin:
Go to `Manage Jenkins` -> `Manage Plugins` -> `Available`, and install the `Job DSL` plugin without restart.

Afterwards create the DSL job with `New Item`, the name and location is up to you, create it as a `Freestyle project`.

In this newly created job under `Build` click `Add build step` -> `Process Job DSLs`, select `Use the provided DSL script`.

Then you're going to want something like this:

```groovy
pipelineJob('example-job-1') {
  definition {
    cpsScm {
      scm {
        git('https://gitlab.com/wagensveld/jenkins-jobs.git', 'master') { node ->
          node / gitConfigName('DSL User')
          node / gitConfigEmail('me@me.com')
        }
      }
      scriptPath('jobs/example-job-1/Jenkinsfile')
    }
  }
}

pipelineJob('example-job-2') {
  definition {
    cpsScm {
      scm {
        git('https://gitlab.com/wagensveld/jenkins-jobs.git', 'master') { node ->
          node / gitConfigName('DSL User')
          node / gitConfigEmail('me@me.com')
        }
      }
      scriptPath('jobs/example-job-2/Jenkinsfile')
    }
  }
}
```

`example-job-1` being the name of the job you want to make, `git` being the repository, you don't need to specify `master` but you can change the branch if you want, the `gitConfig` values are a workaround I had to do for GitLab, DSL seems to be much friendlier towards Github.

You may want to configure `Action for removed jobs/views/config files` to prevent old pipelines clogging up Jenkins.

If you visit `http://yourjenkinsurl.com/plugin/job-dsl/api-viewer/index.html` you'll be greeted with the API reference, where you can do more complex configurations.

Creating subdirectories for example `pipelineJob('subdirectory/example-job-2')` requires you to first make the folder in Jenkins.

Lastly implementation for this changes depending on the git service, but using a git push as a build trigger is very useful.
