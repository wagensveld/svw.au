---
title: "Print all parameters in a jobs description"
description: "def paramsStringBuilder = new StringBuilder()
params.each { param ->
  paramsStringBuilder.append(\"${param.key}: ${param.value}\n\")
}
currentBuild.description = paramsStringBuilder.toString()
"
summary: "
def paramsStringBuilder = new StringBuilder()
params.each { param ->
  paramsStringBuilder.append(\"${param.key}: ${param.value}\n\")
}
currentBuild.description = paramsStringBuilder.toString()
"
date: "2021-07-21T21:22:00"
series: ["devops"]
category: "guides"
---

When creating pipelines I often add something like the following to the start of my pipeline:

```groovy
pipeline {
  stages {
    stage("Config"){
      steps {
        script {
          def paramsStringBuilder = new StringBuilder()
          params.each { param ->
            paramsStringBuilder.append("${param.key}: ${param.value}\n")
          }
          currentBuild.description = paramsStringBuilder.toString()
        }
      }
    }
  }
}
```

I usually implement it as a function as a part of a Jenkins Library, e.g.:

```groovy
def call() {
  def paramsStringBuilder = new StringBuilder()
  params.each { param ->
    paramsStringBuilder.append("${param.key}: ${param.value}\n")
  }
  currentBuild.description = paramsStringBuilder.toString()
}
```

I'll also set the `currentBuild.displayName` as well, for example in an environment creation job:

```groovy
pipeline {
  parameters {
    string(
      name: "ENVIRONMENT_NAME",
      defaultValue: "myWebServer",
      description: "The name of the environment to create."
    )
    choice(
      name: "ENVIRONMENT_TYPE",
      choices: [
        'DEV',
        'SIT',
        'UAT'
      ],
      description: "The type of environment to create."
    )
  }

  stages {
    stage("Config"){
      steps {
        script {
          currentBuild.displayName = "#${BUILD_NUMBER} ${params.ENVIRONMENT_NAME} (${params.ENVIRONMENT_TYPE})"
        }
      }
    }
    ...
  }
}
```
