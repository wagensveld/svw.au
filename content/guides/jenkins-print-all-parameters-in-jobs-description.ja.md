---
title: "JobのParameterにすべてのPipelineをPrintします"
description: "def paramsStringBuilder: new StringBuilder()
params.each { param ->
  paramsStringBuilder.append(\"${param.key}: ${param.value}\n\")
}
currentBuild.description: paramsStringBuilder.toString()
"
summary: "
def paramsStringBuilder = new StringBuilder()
params.each { param ->
  paramsStringBuilder.append(\"${param.key}: ${param.value}\n\")
}
currentBuild.description = paramsStringBuilder.toString()
"
date: "2021-07-21T21:22:00"
category: "guides"
---

Pipeline を作成するとき、Pipeline の始まりに次のようなものを追加します。

```groovy
pipeline {
  stages {
    stage("Config"){
      steps {
        script {
          def paramsStringBuilder = new StringBuilder()
          params.each { param ->
            paramsStringBuilder.append("${param.key}: ${param.value}\n")
          }
          currentBuild.description = paramsStringBuilder.toString()
        }
      }
    }
  }
}
```

Jenkins Library の一部としての関数として実装しています:

```groovy
def call() {
  def paramsStringBuilder = new StringBuilder()
  params.each { param ->
    paramsStringBuilder.append("${param.key}: ${param.value}\n")
  }
  currentBuild.description = paramsStringBuilder.toString()
}
```

環境作成ジョブでは、`currentbuild.displayname`も設定します:

```groovy
pipeline {
  parameters {
    string(
      name: "ENVIRONMENT_NAME",
      defaultValue: "myWebServer",
      description: "The name of the environment to create."
    )
    choice(
      name: "ENVIRONMENT_TYPE",
      choices: [
        'DEV',
        'SIT',
        'UAT'
      ],
      description: "The type of environment to create."
    )
  }

  stages {
    stage("Config"){
      steps {
        script {
          currentBuild.displayName = "#${BUILD_NUMBER} ${params.ENVIRONMENT_NAME} (${params.ENVIRONMENT_TYPE})"
        }
      }
    }
    ...
  }
}
```
