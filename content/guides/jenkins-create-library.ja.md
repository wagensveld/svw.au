---
title: "Jenkins Libraryを作成します "
description: "GitRepoでGroovyファイルを作成します"
summary: "GitRepoでGroovyファイルを作成します"
date: "2021-08-06T21:06:40"
category: "guides"
---

最初のステップは、JenkinsLibrary のリポジトリを作成することです。

[ここでは参考です。](https://gitlab.com/wagensveld/jenkins-library)

リポジを取り込む前に、それを Jenkins に追加しましょう。
`Manage Jenkins` -> `Configure System`、Global Pipeline ライブラリーの検索、 `Add`をクリックします。

`Name`は何でもすることができます、それはあなたが Jenkins Pipeline で参照するものです、デフォルトバージョンはデフォルトで使用するブランチ/タグです。 他のすべてのものはデフォルトのままにすることができます。

レポの構造は次のとおりです:

```
レポ
|-resources
|-src
|-vars
```

基本的に`vars`はあなたがあなたのパイプライン内で呼び出す関数です、src はあなたがより複雑なライブラリ関数に使用できる Groovy SourceFiles であり、リソースは src ファイルの静的ヘルパーデータです。

このために、私たちは vars に焦点を当てています。

この簡単な機能を取ります:

```groovy
def call() {
  def paramsStringBuilder = new StringBuilder()
  params.each { param ->
    paramsStringBuilder.append("${param.key}: ${param.value}\n")
  }
  currentBuild.description = paramsStringBuilder.toString()
}
```

それを `paramsDescription.groovy`に名前を付けて vars repo に置きましょう。

それがプッシュされると、それをパイプラインで使用するためにこれを JenkinsFile `@Library('my-library') _`、`my-library`の先頭に追加しただけで、以前に指定した名前ではありません。 セットアップ中に「暗黙的に暗黙的に」を`true`に設定した場合は、これを行う必要があります。

特定のブランチ/タグを指定することもできます。`@Library('wagensveld-jenkins@feature/branch') _`または`@Library('wagensveld-jenkins@1.0') _`
