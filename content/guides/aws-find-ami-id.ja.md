---
title: "EC2インスタンスを起動せずにAMI IDを見つけます"
description: "EC2インスタンスを事前に起動しなくてもAWSマーケットプレイスでAMI IDを見つける方法。"
date: "2021-07-15T20:58:32"
category: "guides"
---

1. [AWS マーケットプレイス](https://aws.amazon.com/marketplace/)にアクセスし、目的の画像を検索して選択します。
2. 「Continue to Subscribe」をクリックします（まだログインしていない場合は、ログインが必要になる場合があります）。
3. 「Continue to Configuration」をクリックします。
4. 「Delivery Method」、「Software Version」、「Region」を設定します-これらすべてが AMI ID を変更します。
5. AMI ID は下に向かって表示されます。
