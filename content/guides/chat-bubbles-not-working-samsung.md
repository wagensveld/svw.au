---
title: "Fix Chat Bubbles Not Working On Samsung Phones"
description: "Settings -> Advanced features -> Labs -> Enable Multi window for all apps"
summary: "`Settings -> Advanced features -> Labs -> Enable Multi window for all apps`"
date: "2023-01-29T17:03:24"
category: "guides"
---
> As of 2024 it seems like this no longer works. I've given up on using chat bubbles on Samsung phones.

Since upgrading my Samsung Galaxy to Android 12 / One UI 4 the chat bubbles stopped working, I've just stumbled upon [this comment on Reddit](https://www.reddit.com/r/signal/comments/s8je6s/bubbles_not_appearing_on_one_ui_4_android_12/hw4xvs2/), which has fixed it for Signal.

Signal was the only app where I had this problem, but I'm sure there are others.
