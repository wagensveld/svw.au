---
title: "Transfer a PGP key to another device"
description: "gpg --decrypt myKey.pgp | gpg --import"
summary: "```
gpg --decrypt myKey.pgp | gpg --import
```"
date: "2021-10-05T16:18:24"
category: "guides"
---

> This assumes you already have GPG installed

Let's say you have a pgp key generated on a device.

In my case I use OpenKeychain which lets you back up keys using the GUI, but on another system something like `gpg --armor --export-secret-keys name@domain.com | gpg --armor --symmetric --output myKey.pgp`.

In any case you will have your secret key and a password to go with it. Run:

```
gpg --decrypt myKey.pgp | gpg --import
```

You'll be prompted for a password and you should be set.

## Bonus: Raise trust

After importing a key you may want to raise the trust of your imported key.

Get the key ID: `gpg --list-keys`.

Lets say our key is `49VICCITSVUNYOWVG1VKOMPGW5RSW4E78TMW4YRA`

Edit the key: `gpg --edit-key 49VICCITSVUNYOWVG1VKOMPGW5RSW4E78TMW4YRA`

Run `trust`, select your trust level. Then run `save` to save and exit.
