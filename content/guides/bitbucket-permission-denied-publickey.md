---
title: "Bitbucket Permission denied (publickey)"
description: "Add the following to your ~/.ssh/config file

Host bitbucket.org
 HostName bitbucket.org
 PubkeyAcceptedKeyTypes +ssh-rsa"
summary: "Add the following to your ~/.ssh/config file

Host bitbucket.org
 HostName bitbucket.org
 PubkeyAcceptedKeyTypes +ssh-rsa"
date: "2021-06-29T20:56:34"
category: "guides"
---

I had this issue when setting up my ssh keys for Bitbucket on Fedora 34, even though my keys were fine.

```
ssh -T git@bitbucket.org

git@bitbucket.org: Permission denied (publickey).
```

Eventually I found this [post on Stack Overflow](https://stackoverflow.com/questions/64640596/ssh-permission-denied-publickey-after-upgrade-fedora-33) which had the answer.

Add the following to your `~/.ssh/config` file

```
Host bitbucket.org
 HostName bitbucket.org
 PubkeyAcceptedKeyTypes +ssh-rsa
```

Just like that:

```
ssh -T git@bitbucket.org

logged in as git

You can use git to connect to Bitbucket. Shell access is disabled
```
