---
title: "Rockbox Rsync"
description: "rsync -ah --inplace --progress /Volumes/Samsung_T5/Music/* /Volumes/IPOD/Music"
summary: "```
rsync -ah --inplace --progress /Volumes/Samsung_T5/Music/* /Volumes/IPOD/Music
```
"
date: "2021-10-05T16:18:24"
category: "guides"
---

これは、私がこれまでに Rockbox を搭載した iPod 用に思いついた最高の rsync コマンドです。本当の違いは、メタ変更でファイル全体を再度コピーする必要がない`--inplace`から来ています。（それを見つけてくれた友人のネイサンに感謝します。）
