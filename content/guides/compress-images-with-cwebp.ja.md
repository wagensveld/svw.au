---
title: "cwebpで画像を圧縮する"
description: "Webで使用するために画像を圧縮する簡単な方法。実際、私がこの投稿を持っている唯一の理由は、ツールのことを時々忘れてしまうからです。"
date: "2022-11-30T19:51:32"
category: "guides"
---

1. cwebp をインストールします: `npm install -g cwebp`
2. `cwebp -q 75 IMAGE.png -o IMAGE.webp`

`-q`は品質を決定します。実際、この例では cwebp のデフォルトが 75 であるため、これを含める必要はありません。
