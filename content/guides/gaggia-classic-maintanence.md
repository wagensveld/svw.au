---
title: "Gaggia Classic Maintenance"
description: "While this is written with the Gaggia Classic in mind, this carries over to any espresso machine."
summary: "While this is written with the Gaggia Classic in mind, this carries over to any espresso machine."
date: "2023-02-04T19:43:24"
category: "guides"
---

## Daily

After making a coffee:

- Backflush with water.
- Wipe down and run steam wand.
- Wipe down machine.
- Wash portafilter and milk jug.

## Every Couple of Days

- Empty drip tray.
- Empty knock box.
- Refill water tank.

## Weekly

- Backflush using espresso machine cleaner.
- Clean drip tray.
- Clean knock box.

## Every month

### Clean Shower Screen

1. Remove shower screen from group head.
2. Soak in a warm solution of espresso cleaner and water. Let rest for an hour or so.
3. Reinstall shower screen.

## Every 3 months

### Descaling

1. Fill up the water reservoir to max.
2. Add the descaling solution according to the manufacturers recommended ratio.
   > For reference the water reservoir is 2.1L when filled up to max.
3. Turn the machine on and dispense 500ml of hot water through the steam wand.
   > Open the steam wand valve, and flip the brew and steam switches at the same time.
4. Turn the machine off. Wait 20 minutes.
5. Turn the machine on, dispense 600ml hot through the steam wand.
6. Let the machine rest for a couple of minutes.
7. Dispense 600ml through the group head. Turn the machine off.
   > You should have only a tiny bit of water left in the reservoir. You don't want to empty it completely as air may get stuck in the pump.
8. Remove and clean the reservoir.
9. Add it back to the machine, fill it to the max.
10. Dispense 500ml hot water through the steam wand.
11. Let the machine rest a couple of minutes.
12. Dispense 500ml hot water through the group head.
