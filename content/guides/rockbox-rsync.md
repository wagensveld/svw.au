---
title: "Rockbox Rsync"
description: "rsync -ah --inplace --progress /Volumes/Samsung_T5/Music/* /Volumes/IPOD/Music"
summary: "```
rsync -ah --inplace --progress /Volumes/Samsung_T5/Music/* /Volumes/IPOD/Music
```
"
date: "2021-10-05T16:18:24"
category: "guides"
---

That's the best rsync command I've managed to come up with for my iPod with Rockbox so far. The real difference comes from `--inplace` where meta changes don't require the entire file to be copied again. (Thank you to my friend Nathan for finding that.)
