---
title: "Bitbucket Permission denied (publickey)"
description: "~/.ssh/config ファイルに以下を追加します。

Host bitbucket.org
 HostName bitbucket.org
 PubkeyAcceptedKeyTypes +ssh-rsa
"
summary: "~/.ssh/config ファイルに以下を追加します。

Host bitbucket.org
 HostName bitbucket.org
 PubkeyAcceptedKeyTypes +ssh-rsa
"
date: "2021-06-29T20:56:34"
category: "guides"
---

Key は問題ありませんでしたが、Fedora34 で Bitbucket の SSH Key を設定するときにこの問題が発生しました。

```
ssh -T git@bitbucket.org

git@bitbucket.org: Permission denied (publickey).
```

最終的に答えを持っていた[Stack Overflow でこの投稿](https://stackoverflow.com/questions/64640596/ssh-permission-denied-publickey-after-upgrade-fedora-33)を見つけました。

`~/.ssh/config` ファイルに以下を追加します。

```
Host bitbucket.org
 HostName bitbucket.org
 PubkeyAcceptedKeyTypes +ssh-rsa
```

ちょうどそのように:

```
ssh -T git@bitbucket.org

logged in as git

You can use git to connect to Bitbucket. Shell access is disabled
```
