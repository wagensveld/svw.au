---
title: "Framework Audio Tweaks"
description: "By default the audio on the Framework 13 isn't the best, here are a couple of tweaks which make it not as bad."
summary: "By default the audio on the Framework 13 isn't the best, here are a couple of tweaks which make it not as bad."
date: "2023-12-27T12:29:32"
series: ["linux"]
category: "guides"
---

## Pipewire

First thing's first ensure you have `pipewire` installed:

`pacman -S pipewire`

## Speakers

`easyeffects` can be used to install audio profiles. [Ceiphr has collected community presets for the Framework 13](https://github.com/ceiphr/ee-framework-presets). At the moment I'm using the `philonmetal` preset. It's a definite improvement, but still not fantastic.

Probably stick to headphones if you can.

`pacman -S easyeffects`

## Microphone

By default the microphone is set to 100% volume, this distorts it greatly. Setting the volume to a friendly 30% makes the microphone sound quite pleasant.

### Noise Cancellation

[noise-suppression-for-voice](https://github.com/werman/noise-suppression-for-voice) provides exactly what the name suggests. Instructions are within the repository's `README.md`. But heres the short and sweet of it.

1. Install `noise-suppression-for-voice`: `pacman -S noise-suppression-for-voice`
2. Create a configuration file: `~/.config/pipewire/pipewire.conf.d/99-input-denoising.conf`
3. Add the following contents to the configuration file:

```
context.modules = [
{   name = libpipewire-module-filter-chain
    args = {
        node.description =  "Noise Cancelling source"
        media.name =  "Noise Cancelling source"
        filter.graph = {
            nodes = [
                {
                    type = ladspa
                    name = rnnoise
                    plugin = /usr/lib/ladspa/librnnoise_ladspa.so
                    label = noise_suppressor_mono
                    control = {
                        "VAD Threshold (%)" = 85.0
                        "VAD Grace Period (ms)" = 200
                        "Retroactive VAD Grace (ms)" = 0
                    }
                }
            ]
        }
        capture.props = {
            node.name =  "capture.rnnoise_source"
            node.passive = true
            audio.rate = 48000
        }
        playback.props = {
            node.name =  "rnnoise_source"
            media.class = Audio/Source
            audio.rate = 48000
        }
    }
}
]
```
4. Restart pipewire: `systemctl restart --user pipewire.service`
5. Select `Noise Cancelling source` as your input device.
