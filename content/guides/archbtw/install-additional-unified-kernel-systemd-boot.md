---
title: "Install an additional unified kernel using systemd-boot"
description: "Sometimes it's fun, or reassuring to have additional Linux kernels."
summary: "Sometimes it's fun, or reassuring to have additional Linux kernels."
date: "2023-12-22T23:12:39"
series: ["linux"]
category: "guides"
---

Installing an additional unified kernel is pretty straight forward.

First install the kernel `sudo pacman -S linux-lts`

Edit `/etc/mkinitcpio.d/linux-lts.preset` to look like this:

```
# mkinitcpio preset file for the 'linux-lts' package

ALL_config="/etc/mkinitcpio.conf"
ALL_kver="/boot/vmlinuz-linux-lts"

PRESETS=('default' 'fallback')

#default_config="/etc/mkinitcpio.conf"
#default_image="/boot/initramfs-linux-lts.img"
default_uki="/efi/EFI/Linux/arch-linux-lts.efi"
default_options="--splash /usr/share/systemd/bootctl/splash-arch.bmp"

#fallback_config="/etc/mkinitcpio.conf"
#fallback_image="/boot/initramfs-linux-lts-fallback.img"
fallback_uki="/efi/EFI/Linux/arch-linux-lts-fallback.efi"
fallback_options="-S autodetect"
```

Then regenerate initramfs `sudo mkinitcpio -P`

Lastly sign the EFI files:

```
sudo sbctl sign -s /efi/EFI/linux/arch-linux-lts.efi
sudo sbctl sign -s /efi/EFI/linux/arch-linux-lts-fallback.efi
```

Now if you reboot, and spam the spacebar, you should see linux-lts as a boot option.
