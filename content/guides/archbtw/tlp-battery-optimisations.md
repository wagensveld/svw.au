---
title: "Battery Optimisation with TLP"
description: "A pretty basic configuration for battery optimisation using TLP."
summary: "A pretty basic configuration for battery optimisation using TLP."
date: "2023-12-27T13:03:12"
series: ["linux"]
category: "guides"
---

Battery optimisation isn't great on either the Framework nor on desktop Linux. TLP goes a bit of the way to alleviating that.

## Install

Because I use NetworkManager I want to also install the tlp-rdw package.

`pacman -S tlp tlp-rdw`

## Services

You want to enable the tlp service (and NetworkManager-dispatcher if you're using NetworkManager).

```
systemctl enable tlp.service
systemctl enable NetworkManager-dispatcher.service
```

As well as mask the following:

```
systemctl mask systemd-rfkill.service
systemctl mask systemd-rfkill.socket
```

## Configuration

Edit `/etc/tlp.conf`. By default all of the options are commented out. It's worth going through all of these and deciding what makes most sense for your set up. This is what I have.

```
CPU_DRIVER_OPMODE_ON_AC=active
CPU_DRIVER_OPMODE_ON_BAT=passive
CPU_SCALING_GOVERNOR_ON_AC=performance
CPU_SCALING_GOVERNOR_ON_BAT=powersave
CPU_ENERGY_PERF_POLICY_ON_AC=performance
CPU_ENERGY_PERF_POLICY_ON_BAT=balance_power
CPU_BOOST_ON_AC=1
CPU_BOOST_ON_BAT=0
CPU_HWP_DYN_BOOST_ON_AC=1
CPU_HWP_DYN_BOOST_ON_BAT=0
PLATFORM_PROFILE_ON_AC=performance
PLATFORM_PROFILE_ON_BAT=low-power
PCIE_ASPM_ON_BAT=powersupersave
DEVICES_TO_DISABLE_ON_STARTUP="bluetooth nfc wwan"
DEVICES_TO_ENABLE_ON_STARTUP="wifi"
DEVICES_TO_DISABLE_ON_LAN_CONNECT="wifi wwan"
DEVICES_TO_DISABLE_ON_WIFI_CONNECT="wwan"
DEVICES_TO_DISABLE_ON_WWAN_CONNECT="wifi"
DEVICES_TO_ENABLE_ON_LAN_DISCONNECT="wifi wwan"
```
