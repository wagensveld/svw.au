---
title: "Japanese Input With Fcitx"
description: "Set up Japanese input using Fcitx5 and Mozc."
summary: "Set up Japanese input using Fcitx5 and Mozc."
date: "2024-10-27T08:43:52"
series: ["linux"]
category: "guides"
---

## Fonts

The [Arch Wiki provides some fonts to use for Japanese localisation.](https://wiki.archlinux.org/title/Localization/Japanese)

```
sudo pacman -S adobe-source-han-sans-jp-fonts adobe-source-han-serif-jp-fonts noto-fonts-cjk otf-ipaexfont ttf-hanazono
```

## Fcitx and Mozc

My preferred IMF/IME is Fcitx and Mozc.

Install the following packages:

```
sudo pacman -S fcitx5-im fcitx5-lua fcitx5-mozc
```

And now for some manual configuration. Within the KDE system settings, under keyboard, change:

1. Your keyboard layout to Japanese

2. Keyboard model to Generic | Japanese 106-key

3. Virtual keyboard to "Fcitx 5"

Then under input method:

1. Input method (off) to Keyboard - Japanese

2. Input method on to Mozc

Then configure mozc to use kana input instead of romaji.

Lastly create the following file:

`~/.config/fcitx5/conf/mozc.conf`

```
# Initial Mode
InitialMode=Hiragana
# Vertical candidate list
Vertical=True
# Expand Usage (Requires vertical candidate list)
ExpandMode="On Focus"
# Fix embedded preedit cursor at the beginning of the preedit
PreeditCursorPositionAtBeginning=False
# Hotkey to expand usage
ExpandKey=Control+Alt+H
```
