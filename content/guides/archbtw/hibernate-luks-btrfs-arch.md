---
title: "Hibernation on a LUKS Encrypted btrfs"
description: "Getting hibernation working can be a bit complicated at first but hopefully this quick guide makes it easier."
date: "2023-12-21T23:29:47"
series: ["linux"]
category: "guides"
---

I recently got a [Framework laptop](https://frame.work/au/en); so far I have been really impressed with it.

After years of using 15/16 inch MacBook Pros, it's nice to have a smaller one - which through its openness to repair and upgrades really feel like my own.

One thing which isn't as impressive however, is the battery life. I've been trying to optimise that.

In this guide we're tackling hibernation.

## Context

I used [Walian's guide](https://www.walian.co.uk/arch-install-with-secure-boot-btrfs-tpm2-luks-encryption-unified-kernel-images.html) when setting up Arch Linux on this laptop. It covers a basic Arch install with Secure Boot, btrfs, TPM2 LUKS encryption, and Unified Kernel Images.

## Guide

The guide is made up of information found on the following pages. They have more information and are worth a read.

- [Arch Wiki: Power management/Suspend and hibernate](https://wiki.archlinux.org/title/Power_management/Suspend_and_hibernate#Hibernation)
- [Arch Wiki: Btrfs](https://wiki.archlinux.org/title/Btrfs#Swap_file)

### Create the swap file

A good rule of thumb is to replace the 34g with the amount of ram you have + 2

```
btrfs subvolume create /swap
btrfs filesystem mkswapfile --size 34g --uuid clear /swap/swapfile
swapon /swap/swapfile
```

Add the following line to `/etc/fstab`

```
/swap/swapfile none swap defaults 0 0
```

### Hibernation Size

Create `/etc/tmpfiles.d/hibernation_image_size.conf` with the following contents:

```
#    Path                   Mode UID  GID  Age Argument
w    /sys/power/image_size  -    -    -    -   0
```

### Kernel Parameters

Take note of the following parameters

- resume
- resume_offset

#### resume

Run the following to get the UUID for `resume`

```
findmnt -no UUID -T /swap/swapfile
```

`resume` should look like

```
resume=UUID=xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx
```

#### resume_offset

Run the following to get the offset for `resume_offset`

```
btrfs inspect-internal map-swapfile -r /swap/swapfile
```

The `resume_offset` will look something like this:

```
resume_offset=xxxxxxx
```

#### Adding the Kernel Parameters

Create a file `/etc/cmdline.d/resume.conf` with its contents the values from above:

```
resume=UUID=xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx resume_offset=xxxxxxx
```

#### Moment of truth

It's time to test it out.

Restart your computer.

Log back in and try running `systemctl hibernate`

Lastly - depending on desktop environment, find the settings that handle sleep. Change those values to hibernate.
