---
title: "Pacman and AUR Tips"
description: "Here are a couple of tweaks you can do to improve your pacman and AUR experience."
summary: "Here are a couple of tweaks you can do to improve your pacman and AUR experience."
date: "2023-12-27T14:31:59"
series: ["linux"]
category: "guides"
---

## Pacman

I have a few quality of life configurations set in `/etc/pacman.conf`

```
# Use wget. NOTE: you need to install wget first
XferCommand = /usr/bin/wget --passive-ftp -q --show-progress -O %o %u
Color
CheckSpace
VerbosePkgLists
ParallelDownloads = 5
ILoveCandy

[multilib]
Include = /etc/pacman.d/mirrorlist
```

## AUR

The AUR is really convenient. An easy way to use it is [paru](https://github.com/Morganamilo/paru).

```
sudo pacman -S --needed base-devel
git clone https://aur.archlinux.org/paru.git
cd paru
makepkg -si
```

### MakePKG

Most of the magic happens at the MakePKG stage. So here are some optimisations. All of these configurations appear in `/etc/makepkg.conf`.

#### Optimised binaries

Make sure `CFLAGS` contains `-march=native`, and `RUSTFLAGS` contains `-C target-cpu=native`

#### Parallel Compilation

Set `MAKEFLAGS` to `-j$(nproc)`.

## Hooks

### snap-pac

Snapper is useful if you use btrfs, you can install it via `pacman -S snapper`. Additionally a nice gui is `btrfs-assistant` installed via the AUR `paru  btrfs-assistant`.

snap-pac automatically takes snapshots before and after running pacman installs.

`pacman -S snap-pac`

### needrestart

Checks which daemons need to be restarted after upgrades.

`paru needrestart`

### rebuild-detector

Detect which packages were built using older versions of dependencies and need to be rebuilt to function properly.

`pacman -S rebuild-detector`
