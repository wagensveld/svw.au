---
title: "Frameworkbtw"
description: "Join me on a series of documentation, guides, and bad practices, as I configure my Framework laptop manually. Instead of saving time by writing ansible playbooks, I'm spending time by writing the steps here; and then again when I need to set up another computer."
summary: "Join me on a series of documentation, guides, and bad practices, as I configure my Framework laptop manually. Instead of saving time by writing ansible playbooks, I'm spending time by writing the steps here; and then again when I need to set up another computer."
date: "2023-12-21T23:29:47"
series: ["linux"]
category: "guides"
---

I wrote a [guide the other night on enabling hibernation on a luks encrypted arch install](/guides/archbtw/hibernate-luks-btrfs-arch/). To expand on it I'm going to write a series of guides on how I've set up this computer. Perhaps you'll find something useful. Because this is mostly an exercise in documenting my laptop's setup, quite a few of these entries are dependent on one another (especially the install section). But I'm trying to make it as agnostic as reasonable.

Why not automate this with something like Ansible?

I use Arch on both my laptop and desktop. How I like to use them diverge *just* enough to make automation tricky. At the same time, I like trying one thing on one device and another on the other.

## The Guide

### Install

- [Arch base install with Secure Boot, btrfs, TPM2 LUKS encryption, and Unified Kernel Images](/guides/archbtw/arch-install-secure-boot-btrfs-tpm2-luks-unified-kernel-images/)
- [Install an additional unified kernel using systemd-boot](/guides/archbtw/install-additional-unified-kernel-systemd-boot)
- [Aut Mount Encrypted Drive on Boot](/guides/archbtw/automount-encrypted-drive)
- [Japanese IME in KDE Plasma Wayland](/guides/archbtw/japanese-input-with-fcitx)
<!--- DRAFT: [KDE Plasma (Wayland)](/guides/archbtw/kde-plasma-wayland)-->


### Network

- [NetworkManager Tips](/guides/archbtw/networkmanager-tips)
<!--- Opensnitch
- ufw-->

<!--### Security

- KeePass-->

### Power Optimisation

- [Hibernation on a LUKS Encrypted btrfs](/guides/archbtw/hibernate-luks-btrfs-arch)
- [Battery Optimisation with TLP](/guides/archbtw/tlp-battery-optimisations)

### Tweaks
- [Pacman and AUR Tips](/guides/archbtw/pacman-and-aur)

### Framework Specific

- [Audio Tweaks](/guides/archbtw/framework-audio)
<!--- Display calibration
- Fingerprint Scanner
- Auto brightness
- Bios (E.g. Limiting battery )
- framework-system-git-->
