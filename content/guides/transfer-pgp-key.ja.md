---
title: "PGPキーを別のデバイスに転送する"
description: "gpg --decrypt myKey.pgp | gpg --import"
summary: "```
gpg --decrypt myKey.pgp | gpg --import
```"
date: "2021-10-05T16:18:24"
category: "guides"
---

> これは、すでに GPG がインストールされていることを前提としています

デバイスで生成された pgp キーがあるとします。私の場合、GUI を使用してキーをバックアップできる OpenKeychain を使用していますが、別のシステムでは`gpg --armor --export-secret-keys me@myemail.com | gpg --armor --symmetric --output myKey.pgp`。

いずれにせよ、秘密鍵とそれに付随するパスワードを取得できます。 実行:

```
gpg --decrypt myKey.pgp | gpg --import
```

パスワードの入力を求めるプロンプトが表示され、設定する必要があります。
