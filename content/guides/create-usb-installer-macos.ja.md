---
title: "macOSのインストーラー用にISOをUSBにコピーする"
description: "dd if=image.iso of=/dev/ridentifier bs=1m"
summary: "```
dd if=image.iso of=/dev/ridentifier bs=1m
```"
date: "2022-02-28T21:18:16"
category: "guides"
---

1. ディスクを特定します `diskutil list`

```
diskutil list

...
/dev/disk2 (external, physical):
   #:                       TYPE NAME                    SIZE       IDENTIFIER
   0:     FDisk_partition_scheme                        *15.5 GB    disk2
   1:                  FAT_32 ⁨Untitled⁩                   15.5 GB    disk2s1
```

ボリュームの識別子をメモします。 この場合は`didisk2s1`

2. ボリュームをアンマウントする `sudo umount /dev/<IDENTIFIER>` (`sudo umount /dev/didisk2s1`).
3. ファイルの変換とコピー `sudo dd if=</path/to/image.iso> of=/dev/r<IDENTIFIER> bs=1m` (`sudo dd⋅if=image.iso⋅of=/dev/rdisk2s1⋅bs=1m"`)
4. ボリュームを取り出す `diskutil eject /dev/<IDENTIFIER>` (`diskutil eject /dev/disk2s1`)
