---
title: "Readline Interaction"
description: "gpg --decrypt myKey.pgp | gpg --import"
date: "2021-11-10T14:40:30"
draft: true
category: "drafts"
---

This originally started as trying to find out how to jump to the end of a line in my terminal (It's `C-e` btw). Quickly I ended up reading the [man page for bash](https://www.gnu.org/software/bash/manual/bash.html#Readline-Interaction). That portion covers the ways to edit text while using an interactive shell. Here are some thing's I found useful.

## Notation

For the sake of this post the only modifier keys we need to worry about are `C` for _Control_ and `M` for _Meta_. `C-k` is the equivalent of holding down `Control` and `k`.

The Meta key.

Most keyboards don't come with printed meta keys any more. Often it is the `Alt` key on the left, if that doesn't work, it could be the `Alt` key on the right. If that doesn't work (such as macOS where the `Option/Alt` key is used to modify characters) another option is to press `Esc` release, and then press key.

## Navigation

| Command | Result                     | Mnemonic                         |
| :------ | :------------------------- | :------------------------------- |
| C-a     | Move to start of the line  | _a_ is the start of the alphabet |
| C-e     | Move to end of the line    | The *e*nd of the line            |
| C-b     | Move back one character    | Move *b*ack one character        |
| C-f     | Move forward one character | Move *f*orward one character     |
| M-b     | Move back one word         |                                  |
| M-f     | Move forward one character |                                  |

## Editing

| Command     | Result                                    | Mnemonic |
| :---------- | :---------------------------------------- | :------- |
| C-u or C-\_ | Undo last editing command                 | *u*ndo   |
| C-d         | Delete character under cursor             | *d*elete |
| Del         | Delete character to the left of cursor    |          |
| C-l         | Clear screen preserving only the readline | C*l*ear  |
