---
title: "Stuff I Use"
description: "I don't intend for this to be a post that promotes consumerism. Or that a specific product will automatically fix a problem. These are just things which have made my life a little easier in some way, and perhaps it'll help you figure out what can make your life easier."
summary: "I don't intend for this to be a post that promotes consumerism. Or that a specific product will automatically fix a problem. These are just things which have made my life a little easier in some way, and perhaps it'll help you figure out what can make your life easier."
date: "2024-05-30T15:19:04"
category: "me"

---

While looking for inspiration for this website, I came across [The Setup by Carlos Matallín](https://matall.in/usesthis/), something about it struck a chord with me. Here's my version.

Most of these aren't the best, they're sentimental objects.

## Technology

### Hardware

- **Framework Laptop 13**
<!-- TODO: Create blog post about the framework -->

```
Purchased: Late 2023
```

I love repairable, upgradeable hardware. I wanted to get one at launch; but Intel as the lone option, and not having a JIS layout kept me away.

Until 2023 when I saw they were releasing both of those things. This ended up being the perfect time for me. I had a decent desktop home set up, but wanted to go to the library for dedicated study time, and didn't want a large 15 inch laptop.

Up front it isn't the cheapest laptop, in the long term as I repair and upgrade it perhaps it'll save me money. Ultimately I was in the market for a laptop and ideologically I want a product like this to be successful.

- **TEX Shinobi (Japanese)**

```
Purchased: Mid 2021
```

I've always had a soft spot for ThinkPads. I find the TrackPoint quite convenient. I like mechanical keyboards. And I have somehow gotten accustomed to the JIS layout. I was pleasantly surprised to see a keyboard which ticked those boxes.

- **Kobo Libra 2**

```
Purchased: Early 2024
```

I think this is the ideal size for an eReader, small enough for a large pocket. Large enough to have a decent amount of text.

### Software

- **Typst**

```
Since: Early 2024
```

My Intro to Computer Science lecturer used LaTeX for everything; it became a habit I picked up from him. LaTeX is quite heavy though (about 3GB). Typst isn't a drop in replacement but a nice modern alternative.

## Stationary

- **Leather Binder Notebook**

<!-- TODO: Create blog post about the notebooks -->

```
Purchased: Mid 2024
```

I purchased a few these from AliExpress. Several sellers have them in stock; it looks like "AIGUONIU Official Store" is the main store.

I've found it quite useful to split notebooks into broad categories (such as "Study", "Work", "Projects") and be able to move notes between them freely.

The majority of my notebooks are A5. I have an A7 for everyday notes. And an "A6" as a planner - everything lives there. "A6" is in quotes because in reality it's Filofax Personal sized. Which actually is the ideal size for my jacket pockets. But buyer beware, I don't think actual A6 paper exists on AliExpress.

<!-- ## Kitchen -->
<!---->
<!-- ## Tools -->
