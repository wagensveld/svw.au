---
title: "Git Grep Cheat sheet"
date: "2021-10-07T12:43:23"
aliases:
  - "/git-grep"
draft: true

category: "drafts"
---

## I just want to find a string in my repo

```bash
git grep "find me"
```

## I just want to find a string in certain filetypes

```bash
git grep "find me" -- "*.md" "*.txt"
```

## I want to find a string in another branch

```bash
git grep "find me" other-branch-name
```

## I want to two strings on a line

```bash
git grep -e "want" --and -e "find"
...
content/english/reference/git-grep.md:## I just want to find a string in my repo
content/english/reference/git-grep.md:## I just want to find a string in certain filetypes
content/english/reference/git-grep.md:## I want to find a string in another branch
content/english/reference/grep.md:## I just want to find a string in my current directory
...
```
