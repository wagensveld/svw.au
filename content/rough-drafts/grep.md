---
title: "Grep Cheat sheet"
date: "2021-10-07T11:32:19"
aliases:
  - "/grep"
category: "drafts"
draft: true
---

> Check out the [Git Grep cheat sheet](/reference/git-grep)

## I just want to find a string in my current directory

```bash
grep -R "find me" *
```

## That's too broad I want a file

```bash
grep "find me" myfile.txt
```

## I want my result to be highlighted

```bash
grep -R "find me" myfile.txt --color
```

## I don't care about cases

```bash
grep -i "find me" myfile.txt
```

## I want to know what line it's on

```bash
grep -n "find me" myfile.txt
```

## I want to know what file it's in

```bash
grep -l "find me" *
```

## I want to know how many times it appears

```bash
grep -c "find me" myfile.txt
```

## I want it to match only the string

```bash
grep -w "connect" error.log
# this will match only connect
# without -w this would also match disconnect, connecting, and disconnecting
```

## I want to see 4 lines before and 4 lines after my matching string

```bash
grep "git fetch origin" -B4 -A4 content/english/reference/git.md
...

## I want to reset my local to look just like origin

git fetch origin
git checkout master
git reset --hard origin/master
git clean -d --force
...
```

or

```bash
grep "git fetch origin" -C4 content/english/reference/git.md
```

## I only want to match if my string is at the start of a line

```bash
grep "^find me" myfile.txt
```

## End of a line

```bash
grep "find me$" myfile.txt
```

## Maybe my file has punctuation, I want it to maybe ignore the last character?

```bash
grep "Tom.{0,1}$" space_oddity.txt
...
Ground Control to Major Tom
Ground Control to Major Tom
This is Ground Control to Major Tom
Ground Control to Major Tom
Can you hear me, Major Tom?
Can you hear me, Major Tom?
Can you hear me, Major Tom?
...
```

## I want to match one string or another

```bash
egrep "foo|bar" myfile.txt
```

## I want to search gzip files

```bash
zgrep "find me" myfile.gz
```

## I need to find an IP address

```bash
grep -E "[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}"
```

## I want to tail a file and exclude lines containing either foo, bar, or baz.

```bash
tail -f file.log | grep -Ev '(foo|bar|baz)'
```
