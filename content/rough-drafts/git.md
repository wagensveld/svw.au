---
title: "Git Cheat sheet"
date: "2021-10-07T14:32:52"
aliases:
  - "/git"
draft: true
category: "drafts"
---

> Check out the [Git Grep cheat sheet](/reference/git-grep)

## Tell me about my current branch

```bash
git status
```

## I need to know my git history

```bash
git reflog
...
6b7ca4d (HEAD -> master, origin/master, origin/HEAD) HEAD@{0}: pull: Fast-forward
17f2f45 HEAD@{1}: checkout: moving from recipe/italian-heb-bread to master
351be46 (recipe/italian-heb-bread) HEAD@{2}: commit: Add italian herbs recipe
17f2f45 HEAD@{3}: checkout: moving from master to recipe/italian-heb-bread
17f2f45 HEAD@{4}: pull: Fast-forward
ddb9fc8 (feature/tldr) HEAD@{5}: checkout: moving from feature/tldr to master
...
```

## I need to know my git history but only for a specific file

```bash
git reflog path/to/your/file.txt
```

## I need to know my git history but only for a few lines in a specific file

```bash
git log -L 15,+10:path/to/your/file.txt
```

## I just want to know who to blame

```bash
git blame -L 15,+10 path/to/your/file.txt
```

## I need to see my git history and see the diffs

```bash
git reflog -p
```

## I need to undo a commit

```bash
git revert [HASH]
```

## I need to undo a commit but only for a couple of files

```bash
git checkout [HASH] -- path/to/your/file1.txt path/to/your/file2.txt
```

## I need to change my commit message

```bash
git commit --amend
```

## I accidentally committed to the wrong branch

```bash
# on wrong branch
git reset HEAD~ --soft
git stash

# move to intended branch
git checkout correct-branch
git stash pop
git add .
git commit
```

## I want to rename my branch

```bash
git branch -m new-name
git push origin :old-name new-name
git push origin -u new-name
```

## I want to squash all my commits

```bash
# on your development branch
git reset --soft master
git add .
git commit -m "Here is my squashed commit"
git push --force
```

## I want to remove a file from the repo but keep it local

```bash
git rm --cached myfile.txt
git rm --cached -r mydir
```

## I've already staged my changes but I want to see my diff

```bash
git diff --staged
```

## I want to see the diff between two branches

```bash
git diff master..development-branch
```

## I want to clean up local branches not in origin

```bash
git remote prune origin
```

## I want to reset my local to look just like origin

```bash
git fetch origin
git checkout master
git reset --hard origin/master
git clean -d --force
```

## I find a commit with a certain phrase in its message

```bash
git log --grep "find me"
```

## I find a commit with a certain string in its diff

```bash
git log -p -S "productionPassword"
```

## I want to update a submodule

```bash
git submodule update --remote --merge
```

## I need to merge in changes from master into my dev branch

```bash
git rebase master
```
