---
title: "Good Interview Questions to Ask"
description: "In recent times I have gotten the opportunity to host interviews. It has gotten me thinking - what are some good questions to ask during an interview."
date: "2021-10-24"
draft: true
category: "drafts"
---

I went into this thinking this was going to be solely from the perspective of the interviewer, however the more I write the more I see its application for the interviewee.

I've tried to split this into NUMBER categories: Attitude, and Technical; however there is a lot of crossover.

In general I prefer behavioural style questions, often I've found technical skill isn't the issue, cultural fit is. In addition answering textbook programming questions is a memorisation task, a casual conversation about programming tends to be more revealing.

Most interviews are a weird kind of courting dance of half truths to avoid grim truths.

The 1 to 3 rule, for every question about job responsibilities, ask 3 questions about who they are, their personality, their character etc.

- Most applicants will be able to do the job, what youre actually after is someone who matches the same value system of the company and are able to integrate well with the team.

Really good candidates are also interviewing you

Make an interview feel like a cooperative process rather than an asymmetrical test

IF you have a testing component - whiteboard, technical test, etc. Be sure you actually understand the test.

Use this as a guide, youre interviewing a person, and strictly sticking to a script is worse than not understanding the person youre interviewing.

## Attitude

Where were you 5 years ago?

What controls your decision making your head or your heart?

> What does good code look like?

"It's a great question because it works for all experience levels.

Juniors usually talk about tactics - comments, naming, syntax conventions.

Good Senior/Staff candidates usually talk about high-level goals like readability/maintainability, and then deeper tactics in any direction - OOP, FP, design patterns, testing, performance, security, architecture. It's a great way to sense what they value as a programmer."

> What would you do to improve your current workplace? This question checks what is important for the developer, possible reason why they are leaving, and the thought process of the developer.

> Let's say your manager or team lead or manager proposes a stupid - or even negligent idea. No one else on the team says anything; What do you do?

> Let's say after you have voiced your concerns your boss has decided to go ahead with the idea. What do you do?

> Tell me about a time you messed up. Good for testing someones humility, cirsis managemnet, and ability to problem solve under stress

> "Whats a question you WISH that we asked?"

I usually get the response that its a great question, and allows them to guide you to something they want to expand on and make sure they don't have a strength they wanted to really cover

> FizzBuzz

I do this as well, but it's for another reason. I usually get the "cultural fit" interview questions when I am interviewing senior+ or managers because I am less experienced than most (4 years) and we like to do a broad spectrum.

I ask this because while it is a walk in the park for anyone worth anything if they just refuse to do it because it's cumbersome or personal reasons, I know they are not a good fit.

Hey, we all get dumb tickets sometimes, but when the shit hits the fan I need to know you can help in any way possible even if it's incredibly mundane to you. That is my rationale, as an interviewer.

If as an interviewee I was asked this, I would go "Wow, 5 whole minutes I can't mess up!".

Favourite / least favourite part of their current role - favourite warms them up, meaning theyre more willing to admit what they dislike about their current position

What skills they want to improve on and what theyre doing to improve on them.

Whats the nicest thing an employer has ever done for you?

What does their dream company look like? - E.g. what are their priorities? management style, stress level? free lunches? and then how does that compare to yout current company?

Don't ask "can you", ask "have you"?

## Technical

> I see you worked with X programming language, how do you find it, what do you like about it, what are your frustrations?

## Questions to ask the interviewers

What is the strategy for dealing with technical debt? What is the goal of code reviews? How does the team decide what to work on? – If I accept the position, what will my first year goals look like?

– What is the possible career progression path for my position at this company? – What is the company policy and reality regarding overtime work? How many hours a week do you think are required to do this job as a high performer? – Sometimes the actual job descriptions don't match the posted position thanks to HR getting confused. Can you describe for me what the typical responsibilities will be in this role? – Since this is an in-office position, please describe the office environment and culture. Is it open-plan, cubicle, or private office? – Describe your documentation and onboarding process for new employees.

- Is there a 24/7 on-call support? What is the support policy at your company? What's your policy on working from home? What are expectations for accomplishment within the first 90 days (the probationary period) ?

  What are expectations for accomplishment within the first 90 days (the probationary period) ?

        I want to make certain I know what expectations they have of me in this role, and that our expectations align. I don't want to waste either of our times, and I need to know if they need someone to hit-the-ground running making immediate contributions or if they want a calculated approach to the work I'd be doing.

  Does the company have a corporate strategy in place, is this known by departments, and does each department share the same goals?

        I need to know that the company actually has a plan in place. I also need to know that each department has the same high-level goals - otherwise it's going to be a bunch of departments working in a silo.

  What are the goals of the company currently, and how will my position be used to further those goals?

        I want to verify that the company has measured goals - otherwise it's going to be chaotic to actually get anything done.

        I want to verify that the person I report directly to understands these goals, and that I'm not just going to be working on useless tasks/work

  What is your company's projected revenue and customer growth over the next 12 months?

        I want to see if the hiring manager is privy to the actual financial well-being of the company. I need to try and ensure some level of short-term stability.

  If there's a bonus structure, is it merit-based or company-wide - and if it's merit-based, is it directly tied to the corporate strategy?

        Merit is subjective, and if they have measurable goals in place for their company, I know the work I'll be doing will be directly contributing to my bonus.

Will I be able to set up a virtually identical build to production on my work computer without huge hassles?
