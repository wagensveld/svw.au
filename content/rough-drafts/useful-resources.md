---
title: "Useful Resources"
description: "Documents, books, courses, etc which I have found useful."
summary: "Documents, books, courses, etc which I have found useful."
date: "2022-03-17T06:15:37"
aliases:
  - "/useful"
draft: true
category: "drafts"
---

## Firm Skills

> Malleable Skills may be a better phrase for this.

I couldn't find an existing term for these types of skills. These are skills which are often technical (hard) skills; but are primarily useful in the same way soft skills are. Firm being the best word I could think of between soft and hard.

Coming from a development background: Release Management skills are an example of firm skills. A developer is capable of completing their work without release management experience. However appreciating a release manager's concerns helps the developer consider their own commitments in a way which works better within the release manager's constraints.

### Communication

- [Google Developers: Technical Writing](https://developers.google.com/tech-writing)

### Safety

- [St John Ambulance Australia: Emergency First Aid](https://www.stjohnvic.com.au/media/1932/pfa1d.pdf)

## Other

### Vintage Computing

- [minus zero degrees (-0°)](http://www.minuszerodegrees.net/) Excellent hardware reference for the IBM Personal Computer series.
