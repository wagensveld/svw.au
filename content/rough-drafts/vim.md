---
title: "Vim Cheat Sheet"
date: "2021-11-21T03:12:43"
toc: true
aliases:
  - "/vim"
draft: true
category: "drafts"
---

## Normal Mode

> Tip: In the instances where a capital and lower case option is given, the capital will apply the change before the cursor, while the lower case will apply after.

`:h[elp] keyword` Open help file for specified keyword

### Entering Modes

#### Normal Mode

`<Esc>` or `<Ctr> [` Enter Normal mode

#### Insert Mode

`i` Enter Insert mode before current cursor location

`a` Enter Insert mode after current cursor location

`I` Enter Insert mode at start of line

`A` Enter Insert mode at end of line

`ea` Enter Insert mode at the end of the word under the cursor

`o` or `O` Create new line and enter Insert mode below/above current cursor location

#### Visual Mode

`v` Enter visual mode

`V` Enter line visual mode

`<Ctr> v` Enter visual block mode

#### Replacement Mode

`R` Enter Replacement mode

### Exiting and Saving

`:w[rite]` Save the file, but don't quit

`:w filename` Save the file as filename

`:q[uit]` Quit the file - will fail if there are unsaved changes

`:q!` Quit - disregard unsaved changes

`:wq` Save and quit

`:w !sudo tee %` Save using sudo

`:wpa` Save and quit all tabs

`:e!` Discard all changes to current buffer and reload

### Buffers and Tabs

`:tabNew filename` Open filename in new tab

`:tabNext` or `gt` Go to next tab

`:tabprevious` or `gT` Go to previous tab

`:tabclose` Close tab

`:split filename` Create split below

`:vsplit filename` Create split to the right

`<Ctr> w <hjkl>` Move between splits (E.g. `Ctr w l` would go to the split to the right)

`<Ctr> wT` Move current split into its own tab

`<Number> gt` Go to tab number

`:tabdo command` run `command` on all tabs

### Navigation

`h` Move cursor left

`j` Move cursor down

`k` Move cursor up

> Tip: Adding a number in front of some commands to apply to multiple lines. `4k` moves up 4 lines. `-3dd` Deletes 3 lines up.

`l` Move cursor right

`w` Move cursor to beginning of next word

`b` Move cursor to previous beginning of next word

`e` Move cursor to previous end of next word

`ge` Move cursor to previous end of next word

`W` Move cursor to beginning of next word after punctuation

`B` Move cursor to previous beginning of next word after punctuation

`E` Move cursor to previous end of next word after punctuation

`gE` Move cursor to previous end of next word after punctuation

`0` Move cursor to start of line

`^` Move cursor to first non white space character of the line

`$` Move cursor to end of line

`g_` Move cursor to last non whitespace character of the line

`%` Move cursor to matching bracket when over a bracket.

`gg` or `[[` Go to start of file

`G` or `]]` Go to end of file

`H` Move cursor to top of screen

`M` Move cursor to middle of screen

`L` Move cursor to bottom of screen

`{` go to above blank line

`{` go to below blank line

`]s` Next incorrectly spelt word

`[s` Previously incorrectly spelt word

### Search

`n` Jump to next search result

`N` Jump to previous search result

`/foobar` Search for pattern `foobar`

`?foobar` Search backwards for pattern `foobar`

`/^.` Find non empty lines

`:%s/foo/bar/g` Replaces all instances of foo with bar on all lines

`:%s/foo/bar/gc` Replaces all instances of foo with bar on all lines but ask for confirmation before changing

`:%s/foo/bar/` Replaces first instances of foo with bar on all lines

`:s/foo/bar/g` Replaces foo with bar on current line

`:noh` Remove highlighting

### Modifying Text

`~` Toggle character capitalisation under cursor

`.` Repeat last command

`r` Replace character under cursor

`s` Cut character under cursor and enter insert mode

`x` or `X` Cut character under/before cursor

`xp` or `Xp` Swap character under cursor with the character after/before it

`dd` Cut entire line

`cc` Cut entire line and enter Insert mode

`dgg` Cut everything from your cursor to start of file

`dG` Cut everything from your cursor to end of file

`:%d` Cut entire file

`D` Cut rest of line after cursor

`C` Cut rest of line after cursor and enter Insert mode

`diw` Cut word under cursor

`ciw` Cut word under cursor and enter insert mode

`daw` Cut word under cursor and include space after.

`caw` Cut word under cursor, include space after, and enter insert mode

`"_ Command` Delete word under cursor without cutting. E.g. `"_d` or `"_x`

`J` Join line below current one

`gJ` Join line above current one

`u` or `:u` Undo last change

`<Ctr> r` or `:redo` Redo

### Spellcheck

`zg` Add to spellchecker

`zw` Mark as incorrectly spelt

`z=` Show suggested spellings

### Folds

`:<number>,<number> fold` Create fold containing range of line numbers specified. E.g. `:15,40 fold` covers lines 15 to 40.

`zo` Open fold

`z0` Open all nested folds on that line

`zr` Open all first level folds

`zR` Open all folds in file

`zc` Close fold

`zj` or `zk` Jump between folds

`[z` or `]z` Move between start/end of fold

### Other

`q:` See previously run commands

`gx` Open text the cursor is hovered over in web browser

## Insert Mode

`<Ctr> w` Delete word before curso

`<Ctr> j` Create new line

`<Ctr> t` Indent

`<Ctr> d` Deindent

`<Ctr> o key` Run Normal mode command `key` while in Insert mode

`<Ctr> r0` Paste while in insert mode

## Visual Mode

`v <hjkl>` Select text

> Tip: Use other ways of navigating text. E.g. `w` to jump words

`V` Select line

> Tip: Use `<jk>` to select lines below and above

`>` Shift selected text right

`<` Shift selected text left

`~` Toggle case of selected text

`u` Make selected text lower case

`U` Make selected text upper case

`y` Copy selected text

`d` Cut selected text

## Vimrc/Init.lua

### Use System Clipboard

`set clipboard=unnamedplus` or `vim.o.clipboard = "unnamedplus"`

## Tips

### Multi Line Insert

1. Determine how you want to select the words (e.g using jk navigation, or searching for a word).
2. Enter visual block mode `<Ctr> v`.
3. Press `I`.
4. Type text.
5. Press `<Esc>`

Alternatively:

1. Move your cursor to where you want to edit
2. Go into insert mode `i`, make your changes, press `<Esc>`
3. Go to your next word, press `.` to repeat the last edit.

### Sort Lines

1. Select lines with line visual mode `V`
2. Run `:sort`

### Edit Command Using Vim Syntax

This is almost the same as `q:`, the one exception being you can also edit commands which have been written but not executed yet.

Lets say you need to slightly modify a relatively long command but it's a bit of a hassle to backspace everything and retype it. You want to turn `:%s/Foo Bar/Hello World/g` to `:%s/foo bar/Hello World/g`. While in command mode do `<Ctr> f` to be able to edit it (and previous commands using vim. Press enter to execute the command.

### Marks (Across Files)

1. Use `m<Character>` to set a mark at your cursor. E.g. `mB`
2. Type `'<Character>` to jump to the mark. E.g. `'B`

### Edit Remote Files Over SSH Using Your Configuration

```bash
vim scp://user@myserver[:port]//path/to/file.txt
```
