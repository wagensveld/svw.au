---
title: "Two standing desks of sorts"
description: "Some of them worth it, most of them not."
date: "2022-05-19"
draft: true
category: "drafts"
---

## Standing Desk

### Desk Parts

https://theomnidesk.com.au/collections/ecosystem/products/advanced-cable-management-bar-black

Rectangle Rafael Hardwood Timber Table Tops Walnut - 180 x 80cm https://chairforce.com.au/product/rectangle-rafael-hardwood-timber-table-tops-walnut/

https://www.ebay.com.au/itm/402794005971?var=673227707870
https://www.amazon.com.au/gp/product/B09P646383?psc=1

### Tech Parts

https://www.amazon.com.au/gp/product/B01N6GD9JO?psc=1
https://www.ebay.com.au/itm/175399561866
https://www.ebay.com.au/itm/254990326954
https://www.ebay.com.au/itm/402794005971?var=673227707870

### Computer

https://www.amazon.com.au/gp/product/B071HWSGPN?psc=1
https://www.ebay.com.au/itm/393113161623
https://www.ebay.com.au/itm/325367863248
https://www.centrecom.com.au/samsung-odyssey-g55-lc27g55tqwexxy-27-curved-qhd-va-gaming-monitor?gclid=Cj0KCQjw4omaBhDqARIsADXULuUHEnsSGNbFswevF6iDqRFrAsZTvuVtRCY_lnkSeWcj9jNJVRIrCB4aAgn9EALw_wcB
https://support.hp.com/au-en/document/c05078455

https://tex.com.tw/products/shinobi
https://www.modelfkeyboards.com/product/f77-model-f-keyboard/
https://www.ikea.com/au/en/p/forsa-work-lamp-black-60146778/
https://rode.com/en/accessories/stands-bars/psa1

schiit stack
hd600
cd900st
Audio-Technica Consumer ATR2500X-USB Condenser USB Microphone

## Work Desk

### Desk Parts

Rectangle Finchley Table Top 120 x 70cm Walnut https://chairforce.com.au/product/rectangle-finchley-table-top-120-x-70cm-walnut/

mittback \* 2 https://www.ikea.com/au/en/p/mittback-trestle-birch-50459996/

https://www.woolworths.com.au/shop/productdetails/958886/jackson-4-outlet-surge-protected-powerboard-with-1-metre-lead
