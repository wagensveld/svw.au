---
title: "Writing good documentation"
description: "Documentation may be the most underappreciated aspect of software development. How can we help it?"
date: "2021-10-24"
draft: true
category: "drafts"
---

Often I find myself going through emails or slack messages I have either sent or received to hunt down a confluence page I just can't find. In many ways this site exists because I lose track of information and I need a place to document my thoughts.

## The Problem With Documentation

### Scribbles

code is generally more permanent than intended

- Audiences: novice, competent, expert, internal, external, contributors
- domain (business) knowledge vs technical skill
- users: internal vs external
- outcomes: real world problem, theoretical problem

## How do you test documentation for usability?

## Post types

### Introductions

- Objective:
- Analogy:
- Writing style:

### Blog post

- Objective:
- Analogy:
- Writing style:

### updates

- Objective:
- Analogy:
- Writing style:

### Tutorials

- Objective: to teach the user about the application
- Analogy:
- Writing style:

### How to - maybe including translations (e.g. how to convert something in c to c#)

- Objective: achieve a set out outcome "how to set up a server"
- Analogy:
- Writing style:

### Reference

- architectural diagrams
- code snippets

- Objective:
- Analogy:
- Writing style:

### Overview

- Objective:
- Analogy:
- Writing style:

### Run books

- Objective:
- Analogy:
- Writing style:

### Release notes

- Objective:
- Analogy:
- Writing style:

### Announcements

- Objective:
- Analogy:
- Writing style:

### Case studies

- Objective:
- Analogy:
- Writing style:

### Contribution guides

- Objective:
- Analogy:
- Writing style:

### Cheat sheet

- Objective:
- Analogy:
- Writing style:
